<?php

namespace App\Http\Controllers;

use App\Hospedante;
use App\Models\Caja;
use App\Models\Flujo;
use App\Models\Hotel;
use App\Models\Registro;
use App\Pago;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;
use TCPDF;

class ReporteController extends Controller
{
    public function reporte_pagos(Request $request)
    {
        $datos = array();
        $salidas = array();
        //SI ES POST HARA LA CONSULTA

        $opciones = [
            'TRASPASO DE CAJA CHICA' => 'TRASPASO DE CAJA CHICA',
            'ENTREGA DE CAJA CHICA' => 'ENTREGA DE CAJA CHICA',
            'DEUDA DE TRASPASO' => 'DEUDA DE TRASPASO',
            'DEUDA DE ENTREGA' => 'DEUDA DE ENTREGA',
            'DEUDA CANCELADA' => 'DEUDA CANCELADA',
            'OTROS' => 'OTROS',
        ];
        /*        $opciones_aux = $opciones;
        unset($opciones_aux['OTROS']);
        dd($opciones_aux);*/
        if ($request->method() == "POST") {
//            dd($request->toArray());

            $array_detalle = $array_detalle_not = [];
            if (!empty($request->opciones)) {
                $ver_otros = array_search('OTROS', $request->opciones);
//                dd($ver_otros);
                if ($ver_otros === false) {
                    $array_detalle = $request->opciones;
                } else {
                    $opciones_aux = $opciones;
                    unset($opciones_aux['OTROS']);

                    $recate_op = $request->opciones;
                    unset($recate_op['OTROS']);
                    $array_detalle_not = array_diff($opciones_aux, $recate_op);
                }
            }
//dd($array_detalle_not);
            $datos = Flujo::whereHas('caja', function ($query) use ($request) {
                if (!empty($request->hotel_id)) {
                    $query->where('hotel_id', '=', $request->hotel_id);
                }
                return $query;
            })
                ->where(function ($query) use ($request, $array_detalle, $array_detalle_not) {
                    $f_ini = $this->conv_fecha($request->fecha_ini);
                    $f_fin = $this->conv_fecha($request->fecha_fin);
                    $query->where(DB::raw('DATE(created_at)'), '>=', $f_ini);
                    $query->where(DB::raw('DATE(created_at)'), '<=', $f_fin);
                    $query->where('ingreso', '<>', 0);
                    if (!empty($array_detalle)) {
                        $query->whereIn('detalle', $array_detalle);
                    }
                    if (!empty($array_detalle_not)) {
                        $query->whereNotIn('detalle', $array_detalle_not);
                    }

                    return $query;
                })
//                ->whereIn('detalle',[])
            //                ->whereNotIn('detalle',['dsad'])
                ->orderBy('created_at', 'desc')
                ->get()
            ;

//            dd($datos);

            $salidas = Flujo::whereHas('caja', function ($query) use ($request) {
                if (!empty($request->hotel_id)) {
                    $query->where('hotel_id', '=', $request->hotel_id);
                }
                return $query;
            })
                ->where(function ($query) use ($request, $array_detalle, $array_detalle_not) {
                    $f_ini = $this->conv_fecha($request->fecha_ini);
                    $f_fin = $this->conv_fecha($request->fecha_fin);
                    $query->where(DB::raw('DATE(created_at)'), '>=', $f_ini);
                    $query->where(DB::raw('DATE(created_at)'), '<=', $f_fin);
                    $query->where('salida', '<>', 0);
                    if (!empty($array_detalle)) {
                        $query->whereIn('detalle', $array_detalle);
                    }
                    if (!empty($array_detalle_not)) {
                        $query->whereNotIn('detalle', $array_detalle_not);
                    }
                    return $query;
                })
//                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

//            dd($salidas->toSql());
        }
        //----------------------------------------------------
        //REVISA LAS FECHA PARA PONER DEFAULT FECHA HOY
        if (empty($request->fecha_ini)) {
            $fecha_ini = date('d/m/Y');
        } else {
            $fecha_ini = $request->fecha_ini;
        }
        if (empty($request->fecha_fin)) {
            $fecha_fin = date('d/m/Y');
        } else {
            $fecha_fin = $request->fecha_fin;
        }
        if (empty($request->hotel_id)) {
            $hotel_id = '';
        } else {
            $hotel_id = $request->hotel_id;
        }
        //-------------------------------------------------
        //dd($fecha_ini);
        $hoteles = Hotel::pluck('nombre', 'id')->all();

        $cajas = Caja::where(function ($query) use ($request) {
            if (!empty($request->hotel_id)) {
                $query->where('hotel_id', '=', $request->hotel_id);
            }
        })->get();

        return view('reportes.reporte_pagos')->with(compact('datos', 'hoteles', 'fecha_ini', 'fecha_fin', 'hotel_id', 'salidas', 'cajas', 'opciones'));
    }

    public function conv_fecha($fecha)
    {
        $arrayf = explode("/", $fecha);
        return $arrayf[2] . "-" . $arrayf[1] . "-" . $arrayf[0];
    }

    public function pasajeros_reporte(Request $request)
    {

        $nmeses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $permanentes = array();
        $ingresos = array();
        $salidas = array();
        $datos_f = array();
        $hotel = array();
        if ($request->method() == "POST" && !empty($request->fecha_ini) && !empty($request->fecha_fin)) {
            //dd($request->tipo_r);

            $dfecha_inicial = explode("/", $request->fecha_ini);
            $dfecha_final = explode("/", $request->fecha_fin);

            $hotel = Hotel::find($request->hotel_id);

            $datos_f['dia_ini'] = $dfecha_inicial[0];
            $datos_f['dia_fin'] = $dfecha_final[0];
            $datos_f['mes'] = $nmeses[intval($dfecha_inicial[1]) - 1];
            $datos_f['ano'] = substr($dfecha_inicial[2], 2);
            $permanentes = Hospedante::where('hospedantes.estado', 'Ocupando')
                ->join('clientes', 'clientes.id', '=', 'hospedantes.cliente_id')
                ->whereNull('clientes.deleted_at')
                ->whereHas('registro', function ($query) use ($request) {
                    $query->whereHas('habitacione', function ($query) use ($request) {
                        $query->whereHas('rpiso', function ($query) use ($request) {
                            $query->where('hotel_id', $request->hotel_id);
                        });
                    });
                })
                ->where(function ($query) use ($request) {
                    $f_ini = $this->conv_fecha($request->fecha_ini);
                    $query->where(DB::raw('DATE(fecha_ingreso)'), '<', $f_ini);
                    return $query;
                })->get();
            $ingresos = Hospedante::where('hospedantes.estado', 'Ocupando')
                ->join('clientes', 'clientes.id', '=', 'hospedantes.cliente_id')
                ->whereNull('clientes.deleted_at')
                ->whereHas('registro', function ($query) use ($request) {
                    $query->whereHas('habitacione', function ($query) use ($request) {
                        $query->whereHas('rpiso', function ($query) use ($request) {
                            $query->where('hotel_id', $request->hotel_id);
                        });
                    });
                })
                ->where(function ($query) use ($request) {
                    $f_ini = $this->conv_fecha($request->fecha_ini);
                    $f_fin = $this->conv_fecha($request->fecha_fin);
                    $query->where(DB::raw('DATE(fecha_ingreso)'), '>=', $f_ini);
                    $query->where(DB::raw('DATE(fecha_ingreso)'), '<=', $f_fin);
                    return $query;
                })->get();
            $salidas = Hospedante::where('hospedantes.estado', 'Salida')
                ->join('clientes', 'clientes.id', '=', 'hospedantes.cliente_id')
                ->whereNull('clientes.deleted_at')
                ->whereHas('registro', function ($query) use ($request) {
                    $query->whereHas('habitacione', function ($query) use ($request) {
                        $query->whereHas('rpiso', function ($query) use ($request) {
                            $query->where('hotel_id', $request->hotel_id);
                        });
                    });
                })

                ->where(function ($query) use ($request) {
                    $f_ini = $this->conv_fecha($request->fecha_ini);
                    $f_fin = $this->conv_fecha($request->fecha_fin);
                    $query->where(DB::raw('DATE(fecha_salida)'), '>=', $f_ini);
                    $query->where(DB::raw('DATE(fecha_salida)'), '<=', $f_fin);
                    return $query;
                })->get();

//            dd($salidas[3]->toArray());exit;
            if ($request->tipo_r == 'pdf') {
                $pageLayout = array(216, 321); //  or array($height, $width)
                $pdf = new TCPDF('P', 'mm', $pageLayout, true, 'UTF-8', false);
                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                $pdf->SetHeaderMargin(0);
                $pdf->SetFooterMargin(0);
                $pdf->SetAutoPageBreak(false, 0);
                $pdf->AddPage();

                $pdf->SetFillColor(255, 255, 255);
                $h_cel = 4.5;
                $y_pag = 74;
                $contador_r = 0;

                $pdf->SetFont('', '', 10);
                $pdf->Text(25, 42, $hotel->nombre);
                $pdf->Text(130, 42, $hotel->direccion);
                $pdf->Text(35, 52, $hotel->telefonos);
                $pdf->Text(100, 52, $datos_f['dia_ini']);
                $pdf->Text(135, 52, $datos_f['dia_fin']);
                $pdf->Text(160, 52, $datos_f['mes']);
                $pdf->Text(200, 52, $datos_f['ano']);
                $pdf->Text(60, 304, Auth::user()->name);
                //------------------------------ PERMANENTES --------------------------------
                $pdf->SetFont('', '', 15);
                $contador_r++;
                $pdf->SetFont('', 'B', 15);
                $pdf->MultiCell(74, $h_cel, "PERMANENTES", 0, 'C', 1, 1, 30, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                $y_pag = $y_pag + $h_cel;
                $pdf->SetFont('', '', 15);
                foreach ($permanentes as $per) {
                    $contador_r++;
                    if ($contador_r == 46) {
                        $pdf->AddPage();
                        $pdf->SetFont('', '', 10);
                        $pdf->Text(25, 42, $hotel->nombre);
                        $pdf->Text(130, 42, $hotel->direccion);
                        $pdf->Text(35, 52, $hotel->telefonos);
                        $pdf->Text(100, 52, $datos_f['dia_ini']);
                        $pdf->Text(135, 52, $datos_f['dia_fin']);
                        $pdf->Text(160, 52, $datos_f['mes']);
                        $pdf->Text(200, 52, $datos_f['ano']);
                        $pdf->Text(60, 304, Auth::user()->name);
                        $pdf->SetFont('', '', 15);
                        $y_pag = 74;
                    }

                    $pdf->MultiCell(6, $h_cel, $per->dia_ingreso, 0, 'C', 1, 1, 12, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(6, $h_cel, $per->mes_ingreso, 0, 'C', 1, 1, 18, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(6, $h_cel, $per->ano_ingreso, 0, 'C', 1, 1, 24, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(74, $h_cel, $per->cliente->nombre, 0, 'Lc', 1, 1, 30, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(10.5, $h_cel, $per->registro->habitacione->nombre, 0, 'C', 1, 1, 104, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(21.5, $h_cel, $per->cliente->nacionalidad, 0, 'C', 1, 1, 114.5, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(21, $h_cel, $per->cliente->procedencia, 0, 'C', 1, 1, 136, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(16, $h_cel, $per->cliente->profesion, 0, 'C', 1, 1, 157, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(8.5, $h_cel, $per->cliente->edad2, 0, 'C', 1, 1, 173, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(26, $h_cel, $per->cliente->identidad, 0, 'C', 1, 1, 181.5, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $y_pag = $y_pag + $h_cel;
                }

                //---------------------------------------------------------------------------
                //------------------------------ SALIDAS --------------------------------
                $pdf->SetFont('', '', 15);
                $contador_r++;
                if ($contador_r == 46) {
                    $pdf->AddPage();
                    $pdf->SetFont('', '', 10);
                    $pdf->Text(25, 42, $hotel->nombre);
                    $pdf->Text(130, 42, $hotel->direccion);
                    $pdf->Text(35, 52, $hotel->telefonos);
                    $pdf->Text(100, 52, $datos_f['dia_ini']);
                    $pdf->Text(135, 52, $datos_f['dia_fin']);
                    $pdf->Text(160, 52, $datos_f['mes']);
                    $pdf->Text(200, 52, $datos_f['ano']);
                    $pdf->Text(60, 304, Auth::user()->name);
                    $pdf->SetFont('', '', 15);
                    $y_pag = 74;
                }
                $pdf->SetFont('', 'B', 15);
                $pdf->MultiCell(74, $h_cel, "SALIDAS", 0, 'C', 1, 1, 30, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                $y_pag = $y_pag + $h_cel;
                $pdf->SetFont('', '', 15);
                foreach ($salidas as $per) {
                    $contador_r++;
                    if ($contador_r == 46) {
                        $pdf->AddPage();
                        $pdf->SetFont('', '', 10);
                        $pdf->Text(25, 42, $hotel->nombre);
                        $pdf->Text(130, 42, $hotel->direccion);
                        $pdf->Text(35, 52, $hotel->telefonos);
                        $pdf->Text(100, 52, $datos_f['dia_ini']);
                        $pdf->Text(135, 52, $datos_f['dia_fin']);
                        $pdf->Text(160, 52, $datos_f['mes']);
                        $pdf->Text(200, 52, $datos_f['ano']);
                        $pdf->Text(60, 304, Auth::user()->name);
                        $pdf->SetFont('', '', 15);
                        $y_pag = 74;
                    }

                    $pdf->MultiCell(6, $h_cel, $per->dia_ingreso, 0, 'C', 1, 1, 12, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(6, $h_cel, $per->mes_ingreso, 0, 'C', 1, 1, 18, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(6, $h_cel, $per->ano_ingreso, 0, 'C', 1, 1, 24, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(74, $h_cel, $per->cliente->nombre, 0, 'Lc', 1, 1, 30, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(10.5, $h_cel, $per->registro->habitacione->nombre, 0, 'C', 1, 1, 104, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(21.5, $h_cel, $per->cliente->nacionalidad, 0, 'C', 1, 1, 114.5, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(21, $h_cel, $per->cliente->procedencia, 0, 'C', 1, 1, 136, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(16, $h_cel, $per->cliente->profesion, 0, 'C', 1, 1, 157, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(8.5, $h_cel, $per->cliente->edad2, 0, 'C', 1, 1, 173, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(26, $h_cel, $per->cliente->identidad, 0, 'C', 1, 1, 181.5, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $y_pag = $y_pag + $h_cel;
                }

                //---------------------------------------------------------------------------
                //------------------------------ INGRESOS --------------------------------
                $pdf->SetFont('', '', 15);
                $contador_r++;
                if ($contador_r == 46) {
                    $pdf->AddPage();
                    $pdf->SetFont('', '', 10);
                    $pdf->Text(25, 42, $hotel->nombre);
                    $pdf->Text(130, 42, $hotel->direccion);
                    $pdf->Text(35, 52, $hotel->telefonos);
                    $pdf->Text(100, 52, $datos_f['dia_ini']);
                    $pdf->Text(135, 52, $datos_f['dia_fin']);
                    $pdf->Text(160, 52, $datos_f['mes']);
                    $pdf->Text(200, 52, $datos_f['ano']);
                    $pdf->Text(60, 304, Auth::user()->name);
                    $pdf->SetFont('', '', 15);
                    $y_pag = 74;
                }
                $pdf->SetFont('', 'B', 15);
                $pdf->MultiCell(74, $h_cel, "INGRESOS", 0, 'C', 1, 1, 30, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                $y_pag = $y_pag + $h_cel;
                $pdf->SetFont('', '', 15);
                foreach ($ingresos as $per) {
                    $contador_r++;
                    if ($contador_r == 46) {
                        $pdf->AddPage();
                        $pdf->SetFont('', '', 10);
                        $pdf->Text(25, 42, $hotel->nombre);
                        $pdf->Text(130, 42, $hotel->direccion);
                        $pdf->Text(35, 52, $hotel->telefonos);
                        $pdf->Text(100, 52, $datos_f['dia_ini']);
                        $pdf->Text(135, 52, $datos_f['dia_fin']);
                        $pdf->Text(160, 52, $datos_f['mes']);
                        $pdf->Text(200, 52, $datos_f['ano']);
                        $pdf->Text(60, 304, Auth::user()->name);
                        $pdf->SetFont('', '', 15);
                        $y_pag = 74;
                    }

                    $pdf->MultiCell(6, $h_cel, $per->dia_ingreso, 0, 'C', 1, 1, 12, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(6, $h_cel, $per->mes_ingreso, 0, 'C', 1, 1, 18, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(6, $h_cel, $per->ano_ingreso, 0, 'C', 1, 1, 24, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(74, $h_cel, $per->cliente->nombre, 0, 'Lc', 1, 1, 30, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(10.5, $h_cel, $per->registro->habitacione->nombre, 0, 'C', 1, 1, 104, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(21.5, $h_cel, $per->cliente->nacionalidad, 0, 'C', 1, 1, 114.5, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(21, $h_cel, $per->cliente->procedencia, 0, 'C', 1, 1, 136, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(16, $h_cel, $per->cliente->profesion, 0, 'C', 1, 1, 157, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(8.5, $h_cel, $per->cliente->edad2, 0, 'C', 1, 1, 173, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $pdf->MultiCell(26, $h_cel, $per->cliente->identidad, 0, 'C', 1, 1, 181.5, $y_pag, true, 0, false, true, $h_cel, 'M', true);
                    $y_pag = $y_pag + $h_cel;
                }

                //---------------------------------------------------------------------------

                $filename = date('dmYHsi') . '.pdf';
                $pdf->output($filename, 'D');
                return Response::download($filename);
            }
        }

        //REVISA LAS FECHA PARA PONER DEFAULT FECHA HOY
        if (empty($request->fecha_ini)) {
            $fecha_ini = date('d/m/Y');
        } else {
            $fecha_ini = $request->fecha_ini;
        }
        if (empty($request->fecha_fin)) {
            $fecha_fin = date('d/m/Y');
        } else {
            $fecha_fin = $request->fecha_fin;
        }
        //-------------------------------------------------

        $hoteles = Hotel::pluck('nombre', 'id')->all();
        return view('reportes.pasajeros_reporte')->with(compact('fecha_ini', 'fecha_fin', 'permanentes', 'ingresos', 'salidas', 'datos_f', 'hoteles', 'hotel'));
        //dd("dsadsa");
    }

    //Reporte de pagos de registros
    public function repo_pago_regis(Request $request)
    {
        //REVISA LAS FECHA PARA PONER DEFAULT FECHA HOY
        //-------------------------------------------------
        $pagos = array();
        $hoteles = Hotel::pluck('nombre', 'id')->all();
        if ($request->method() == "POST" && !empty($request->fecha_ini) && !empty($request->fecha_fin)) {
            //dd($request->all());
            $f_ini = $this->conv_fecha($request->fecha_ini);
            $f_fin = $this->conv_fecha($request->fecha_fin);
            $pagos = Pago::where(DB::raw('DATE(fecha)'), '>=', $f_ini)
                ->where(DB::raw('DATE(fecha)'), '<=', $f_fin)
                ->where(function ($query) use ($request) {
                    if (!empty($request->estado)) {
                        $query->where('estado', $request->estado);
                    }
                })
                ->whereHas('registro', function ($query) use ($request) {
                    $query->whereHas('habitacione', function ($query) use ($request) {
                        $query->whereHas('rpiso', function ($query) use ($request) {
                            if (!empty($request->hotel_id)) {
                                $query->where('hotel_id', $request->hotel_id);
                            }
                        });
                    });
                })->get();
            //dd($pagos);
        }
        if (empty($request->fecha_ini)) {
            $fecha_ini = date('d/m/Y');
        } else {
            $fecha_ini = $request->fecha_ini;
        }
        if (empty($request->fecha_fin)) {
            $fecha_fin = date('d/m/Y');
        } else {
            $fecha_fin = $request->fecha_fin;
        }

        $hotel_id = '';
        if (!empty($request->hotel_id)) {
            $hotel_id = $request->hotel_id;
        }
        $estado = '';
        if (!empty($request->estado)) {
            $estado = $request->estado;
        }
        return view('reportes.repo_pago_regis')->with(compact('fecha_ini', 'fecha_fin', 'hoteles', 'pagos', 'hotel_id', 'estado'));
    }

    public function reporte_registros(Request $request)
    {

        $registros = array();

        if ($request->method() == "POST" && !empty($request->fecha_ini) && !empty($request->fecha_fin)) {
            //dd($request->all());

            if ($request->tipo_fecha == 'Fecha de creacion') {
                $tfecha = "created_at";
            } elseif ($request->tipo_fecha == 'Fecha de ingreso') {
                $tfecha = "fecha_ingreso";
            } else {
                $tfecha = "fech_ini_reserva";
            }
            $f_ini = $this->conv_fecha($request->fecha_ini);
            $f_fin = $this->conv_fecha($request->fecha_fin);

            $registros = Registro::whereHas('habitacione', function ($query) use ($request) {
                $query->whereHas('rpiso', function ($query) use ($request) {
                    if (!empty($request->hotel_id)) {
                        $query->where('hotel_id', $request->hotel_id);
                    }
                });
            })
                ->where(DB::raw("DATE($tfecha)"), '>=', $f_ini)
                ->where(DB::raw("DATE($tfecha)"), '<=', $f_fin)
                ->where(function ($query) use ($request) {
                    if (!empty($request->estado)) {
                        $query->where('estado', $request->estado);
                    }
                })->get();
        }

        if (empty($request->fecha_ini)) {
            $fecha_ini = date('d/m/Y');
        } else {
            $fecha_ini = $request->fecha_ini;
        }
        if (empty($request->fecha_fin)) {
            $fecha_fin = date('d/m/Y');
        } else {
            $fecha_fin = $request->fecha_fin;
        }
        $hotel_id = '';
        if (!empty($request->hotel_id)) {
            $hotel_id = $request->hotel_id;
        }
        $estado = '';
        if (!empty($request->estado)) {
            $estado = $request->estado;
        }
        $tipo_fecha_f = 'Fecha de creacion';
        if (!empty($request->tipo_fecha)) {
            $tipo_fecha_f = $request->tipo_fecha;
        }
        $hoteles = Hotel::pluck('nombre', 'id')->all();
        return view('reportes.reporte_registros')->with(compact('fecha_ini', 'fecha_fin', 'hoteles', 'hotel_id', 'estado', 'registros', 'tipo_fecha_f'));
    }

    public function repo_pago_retirados(Request $request)
    {
        $pagos = array();
        $hoteles = Hotel::pluck('nombre', 'id')->all();
        if ($request->method() == "POST" && !empty($request->fecha_ini) && !empty($request->fecha_fin)) {
            $pagos = Pago::where('retiroflujo_id', '<>', null)
                ->whereHas('retiro', function ($query) use ($request) {
                    $f_ini = $this->conv_fecha($request->fecha_ini);
                    $f_fin = $this->conv_fecha($request->fecha_fin);
                    $query->where(DB::raw('DATE(created_at)'), '>=', $f_ini);
                    $query->where(DB::raw('DATE(created_at)'), '<=', $f_fin);
                })
                ->whereHas('registro', function ($query) use ($request) {
                    $query->whereHas('habitacione', function ($query) use ($request) {
                        $query->whereHas('rpiso', function ($query) use ($request) {
                            if (!empty($request->hotel_id)) {
                                $query->where('hotel_id', $request->hotel_id);
                            }
                        });
                    });
                })->get();
            //dd($pagos);
        }
        if (empty($request->fecha_ini)) {
            $fecha_ini = date('d/m/Y');
        } else {
            $fecha_ini = $request->fecha_ini;
        }
        if (empty($request->fecha_fin)) {
            $fecha_fin = date('d/m/Y');
        } else {
            $fecha_fin = $request->fecha_fin;
        }
        $hotel_id = '';
        if (!empty($request->hotel_id)) {
            $hotel_id = $request->hotel_id;
        }
        return view('reportes.repo_pago_retirados')->with(compact('fecha_ini', 'fecha_fin', 'hoteles', 'pagos', 'hotel_id'));
    }

    public function reportedia(Request $request)
    {
        $idHotel = Auth::user()->hotel_id;

        $fecha_hoy = date("Y-m-d 07:00:00");

        $date = new DateTime($fecha_hoy);

        $date->modify('-1 day');
        $fecha_ayer = $date->format('Y-m-d H:i:s');

        $num_ingreso = Registro::whereHas('habitacione', function ($query) use ($idHotel) {
            $query->whereHas('rpiso', function ($query) use ($idHotel) {
                $query->where('hotel_id', $idHotel);
            });
        })
            ->where('registros.fecha_ingreso', '>=', $fecha_ayer)
            ->where('registros.fecha_ingreso', '<=', $fecha_hoy)->count();

        $num_salida = Registro::whereHas('habitacione', function ($query) use ($idHotel) {
            $query->whereHas('rpiso', function ($query) use ($idHotel) {
                $query->where('hotel_id', $idHotel);
            });
        })
            ->where('registros.fecha_salida', '>=', $fecha_ayer)
            ->where('registros.fecha_salida', '<=', $fecha_hoy)->count();

        $num_ahora = Registro::whereHas('habitacione', function ($query) use ($idHotel) {
            $query->whereHas('rpiso', function ($query) use ($idHotel) {
                $query->where('hotel_id', $idHotel);
            });
        })->where('estado', 'Ocupando')->count();

        $d_ingresos = Flujo::select(DB::raw('SUM(ingreso) as total_ingresos'))
            ->whereHas('caja', function ($query) use ($request) {
                if (!empty($request->hotel_id)) {
                    $query->where('hotel_id', '=', $request->hotel_id);
                }
                return $query;
            })
            ->where('created_at', '>=', $fecha_ayer)
            ->where('created_at', '<=', $fecha_hoy)
            ->where('ingreso', '<>', 0)->get();
        $d_salidas = Flujo::select(DB::raw('SUM(salida) as total_salidas'))
            ->whereHas('caja', function ($query) use ($request) {
                if (!empty($request->hotel_id)) {
                    $query->where('hotel_id', '=', $request->hotel_id);
                }
                return $query;
            })
            ->where('created_at', '>=', $fecha_ayer)
            ->where('created_at', '<=', $fecha_hoy)
            ->where('salida', '<>', 0)->get();
        $ingresos = 0;
        if (!empty($d_ingresos[0]->total_ingresos)) {
            $ingresos = $d_ingresos[0]->total_ingresos;
        }
        $salidas = 0;
        if (!empty($d_salidas[0]->total_salidas)) {
            $salidas = $d_salidas[0]->total_salidas;
        }

        return response()->json([
            'numero_ingresos' => $num_ingreso,
            'numero_salida' => $num_salida,
            'numero_ahora' => $num_ahora,
            'gastos' => $salidas,
            'ingresos' => $ingresos,
            'state' => 'CA',
        ]);
    }

    public function reservas()
    {
        return view('reportes.reservas');
    }

    public function ajaxreservas(Request $request)
    {
        $idHotel = Auth::user()->hotel_id;
        $c_reservas = DB::table('registros')
            ->select(DB::raw('CONCAT(habitaciones.nombre," - ",pisos.nombre," - ",clientes.nombre) as titulo'), 'registros.fech_ini_reserva', 'registros.fech_fin_reserva')
            ->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
            ->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
            ->join('clientes', 'clientes.id', '=', 'registros.cliente_id')
            ->where('pisos.hotel_id', $idHotel)
            ->where('registros.estado', 'Reservado')
            ->where(function ($query) use ($request) {
                $query->where('registros.fech_ini_reserva', '>=', $request->start)
                    ->where('registros.fech_fin_reserva', '<=', $request->end)
                    ->orWhere('registros.fech_fin_reserva', '>=', $request->start)
                    ->where('registros.fech_fin_reserva', '<=', $request->end)
                    ->orWhere('registros.fech_ini_reserva', '>=', $request->start)
                    ->where('registros.fech_ini_reserva', '<=', $request->end)
                    ->orWhere('registros.fech_ini_reserva', '<=', $request->start)
                    ->where('registros.fech_fin_reserva', '>=', $request->end)
                ;
            })
            ->whereNull('registros.deleted_at')
            ->get();
        // dd($c_reservas);

        $array_reserva = [];

        foreach ($c_reservas as $key => $reserva) {
            $array_reserva[$key]['title'] = $reserva->titulo;
            $array_reserva[$key]['start'] = $reserva->fech_ini_reserva;
            if (!empty($reserva->fech_fin_reserva)) {
                $array_reserva[$key]['end'] = $reserva->fech_fin_reserva;
            }
            $array_reserva[$key]['backgroundColor'] = "#f56954";
            $array_reserva[$key]['borderColor'] = "#f56954";
        }

        return response()->json($array_reserva);
    }

}
