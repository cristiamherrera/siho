<?php

namespace App\Http\Controllers;

use App\Insumo;
use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;
use App\Paquete;
use App\Ingrediente;
use App\Hospedante;
use App\Entrega;
use App\Totale;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaqueteController extends Controller
{
    //
    public function index(){
        $paquetes = Paquete::where('deleted_at',null)->orderBy('updated_at','desc')->get();
        return view('paquetes.index')->with(compact('paquetes'));
    }

    public function paquete($idPaquete = null){
        if (isset($idPaquete)) {
            $paquete = Paquete::find($idPaquete);
        }
        return view('paquetes.paquete')->with(compact('paquete'));
    }

    public function guarda_paquete(Request $request, $idPaquete = null)
    {
        if (isset($idPaquete)) {
            $paquete = Paquete::find($idPaquete);
            $paquete->nombre = $request->nombre;
            $paquete->estado = $request->estado;
            $paquete->descripcion = $request->descripcion;
            $paquete->save();
        } else {
            $paquete = new Paquete;
            $paquete->nombre = $request->nombre;
            $paquete->estado = $request->estado;
            $paquete->descripcion = $request->descripcion;
            $paquete->save();
        }
        Flash::success('Se ha registrado correctamente el paquete!!!');

        return redirect()->back();
    }


    public function eliminar_paquete($idPaquete){
        $paquete = Paquete::find($idPaquete);
        $paquete->deleted_at = date("Y-m-d H:i:s");
        $paquete->save();
        Flash::success('Se ha eliminado correctamente el paquete!!!');
        return redirect()->back();
    }


    public function ingrediente($idPaquete,$idIngrediente = null){
        if (isset($idIngrediente)) {
            $ingrediente = Ingrediente::find($idIngrediente);
        }
        $insumos = Insumo::where('deleted_at',null)->get()->pluck('nombre', 'id')->all();
        return view('paquetes.ingrediente')->with(compact('ingrediente','idPaquete','insumos'));
    }

    public function guarda_ingrediente(Request $request, $idIngrediente = null)
    {
        if (isset($idIngrediente)) {
            $ingrediente = Ingrediente::find($idIngrediente);
            $ingrediente->paquete_id = $request->paquete_id;
            $ingrediente->insumo_id = $request->insumo_id;
            $ingrediente->cantidad = $request->cantidad;
            $ingrediente->descripcion = $request->descripcion;
            $ingrediente->save();
        } else {
            $ingrediente = new Ingrediente;
            $ingrediente->paquete_id = $request->paquete_id;
            $ingrediente->insumo_id = $request->insumo_id;
            $ingrediente->cantidad = $request->cantidad;
            $ingrediente->descripcion = $request->descripcion;
            $ingrediente->save();
        }
        //dd($request->insumo_id);

        $paquete = Paquete::find($request->paquete_id);
        $paquete->save();

        Flash::success('Se ha registrado correctamente el ingrediente!!!');

        return redirect()->back();
    }

    public function eliminar_ingrediente($idIngrediente)
    {
        $ingrediente = Ingrediente::find($idIngrediente);
        $ingrediente->delete();
        Flash::success('Se ha eliminado correctamente el ingrediente');
        return redirect()->back();
    }

    public function entregas_insumos($fecha_d = null){


        if(empty($fecha_d)){
            $fecha_d = date('Y-m-d');
        }
        $idHotel = Auth::user()->hotel_id;

        $hospedantes = Hospedante::whereHas('registro', function ($query) use ($idHotel) {
            $query->whereHas('habitacione', function ($query) use ($idHotel) {
                $query->whereHas('rpiso', function ($query) use ($idHotel) {
                    $query->where('hotel_id', $idHotel);
                });
            });
        })
        ->where(function ($query) use ($fecha_d){
            $query->where('estado','Ocupando');
            $query->where(DB::raw('DATE(fecha_ingreso)'), '<=', $fecha_d);
        })->orWhere(function ($query) use ($fecha_d){
            $query->where('estado','Salida');
            $query->where(DB::raw('DATE(fecha_salida)'), '>=', $fecha_d);
            $query->where(DB::raw('DATE(fecha_ingreso)'), '<=', $fecha_d);
        })->get();
/*        dd($hospedantes);
        exit;*/

        return view('paquetes.entregas_insumos')->with(compact('hospedantes','fecha_d','idHotel'));
    }

    public function registra_entregas_pa(Request $request){
        $clientes_l = $request->clientes;
        //dd($request->all());
        $paquetes = Paquete::where('deleted_at',null)->get()->pluck('nombre','id')->all();

        return view('paquetes.registra_entregas_pa')->with(compact('paquetes','clientes_l'));
    }

    public function guarda_entrega_insumo(Request $request){
        //dd($request->all());
        $idUser = Auth::user()->id;
        $idHotel = Auth::user()->hotel_id;
        $ingredientes = Ingrediente::where('paquete_id',$request->paquete_id)->get();
        $numero_paquetes = count($request->clientes);
        foreach ($ingredientes as $ingrediente){
            if($ingrediente->insumo->getTotal($idHotel) < ($numero_paquetes*$ingrediente->cantidad)){
                Flash::error("La cantidad de insumos no es suficiente verifique antes de hacer entregas!!!");
                return redirect()->back();
            }
        }

        foreach ($ingredientes as $ingrediente){
            $total_d_hot = Totale::where('insumo_id', $ingrediente->insumo_id)->where('hotel_id', $idHotel)->get()->first();
            $total_d_hot->cantidad = $total_d_hot->cantidad - ($numero_paquetes*$ingrediente->cantidad);
            $total_d_hot->save();
        }

        foreach ($request->clientes as $idCliente => $cliente){
            $entrega = new Entrega;
            $entrega->cliente_id = $idCliente;
            $entrega->paquete_id = $request->paquete_id;
            $entrega->user_id = $idUser;
            $entrega->observacion = $request->observacion;
            $entrega->hotel_id = $idHotel;
            $entrega->save();
        }
        Flash::success('Se ha registrado correctamente la(s) entrega(s)!!!');
        return redirect()->back();
    }

    public function eliminar_entrega_in($idEntrega){
        $entrega = Entrega::find($idEntrega);

        foreach ($entrega->paquete->ingredientes as $ingrediente){
            $total_d_hot = Totale::where('insumo_id', $ingrediente->insumo_id)->where('hotel_id', $entrega->hotel_id)->get()->first();
            $total_d_hot->cantidad = $total_d_hot->cantidad + $ingrediente->cantidad;
            $total_d_hot->save();
        }

        $entrega->deleted_at = date('Y-m-d H:i:s');
        $entrega->save();

        Flash::success('Se ha eliminado correctamente la entrega!!');
        return redirect()->back();
    }
}
