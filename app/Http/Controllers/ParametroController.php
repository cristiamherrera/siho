<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Flash;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Parametro;

class ParametroController extends Controller
{
    public function index(){
        $parametros = Parametro::orderBy('id','desc')->limit(20)->get();
        return view('parametros.index')->with(compact('parametros'));
    }

    public function parametro($idParametro = null)
    {

        if (isset($idParametro)) {
            $parametro = Parametro::find($idParametro);
        }
        return view('parametros.parametro')->with(compact('parametro'));
    }


    public function guarda_parametro(Request $request, $idParametro = null)
    {
        if (isset($idParametro)) {

            $parametro = Parametro::find($idParametro);
            $parametro->nit = $request->nit;
            $parametro->numero_autorizacion = $request->numero_autorizacion;
            $parametro->llave = $request->llave;
            $parametro->fechalimite = $request->fechalimite;
            $parametro->numero_ref = $request->numero_ref;
            $parametro->save();
        } else {
            $parametro = new Parametro;
            $parametro->nit = $request->nit;
            $parametro->numero_autorizacion = $request->numero_autorizacion;
            $parametro->llave = $request->llave;
            $parametro->fechalimite = $request->fechalimite;
            $parametro->numero_ref = $request->numero_ref;
            $parametro->save();
        }
        Flash::success('El registro del parametro se ha realizado correctamente!!');
        return redirect()->back();
    }

    public function eliminarparametro($idParametro)
    {
        $parametro = Parametro::find($idParametro);
        $parametro->delete();
        Flash::success('Se ha eliminado correctamente el parametro');
        return redirect(route('parametros'));
    }
}