<?php

namespace App\Http\Controllers;

use App\Models\Registro;
use DB;
use Log;
use App\Models\Habitaciones;
use App\Models\Pisos;
use App\Http\Requests;
use App\Http\Requests\CreateHotelRequest;
use App\Http\Requests\UpdateHotelRequest;
use App\Repositories\HotelRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use PhpParser\Node\Expr\New_;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\Categoria;

use App\User;
use App\Opcione;

class HotelController extends InfyOmBaseController
{
    /** @var  HotelRepository */
    private $hotelRepository;

    public function __construct(HotelRepository $hotelRepo)
    {
        $this->middleware('auth');
        $this->hotelRepository = $hotelRepo;
    }

    /**
     * Display a listing of the Hotel.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->hotelRepository->pushCriteria(new RequestCriteria($request));
        $hotels = $this->hotelRepository->all();
        //$habitaciones = $this->hotelRepository->find(2)->habitaciones;
        //$habita = \App\Models\Hotel::find(2);
        //$ha = \App\Models\Hotel::find(2)->habitaciones;
        //$ha = $this->Hotel->all();
        //dd($ha);
        return view('hotels.index')
            ->with('hotels', $hotels);
    }

    /**
     * @return HotelRepository
     */
    public function muestraHabitaciones($idHotel)
    {
        //dd($idHotel);
        /*$ha = DB::table('habitaciones')
            ->leftJoin('hotels','hotels.id','=','habitaciones.hotel_id')
            ->select('hotels.nombre', 'habitaciones.piso')
            ->where('hotels.id',$idHotel)
            ->get();*/
        //dd($ha);
        //$habitaciones = \App\Models\Hotel::find($idHotel)->rhabitaciones;
        /*foreach ($habitaciones as $h){
            echo($h);
        }*/
        //dd($habitaciones);
        //return view('hotels.muestraHabitaciones', compact('habitaciones'));
    }

    /**
     * Show the form for creating a new Hotel.
     *
     * @return Response
     */
    public function create()
    {
        return view('hotels.create');
    }

    /**
     * Store a newly created Hotel in storage.
     *
     * @param CreateHotelRequest $request
     *
     * @return Response
     */
    public function store(CreateHotelRequest $request)
    {
        $input = $request->all();
        //dd($input);

        $pisos = $request->pisos;
        $habitaciones = $request->input('habitaciones');
        $camas = $request->camas;

        $hotel = $this->hotelRepository->create($input);

        if ($hotel) {
            $idHotel = $hotel->id;
            $idCategoria = null;
            if(!empty($request->categoria)){
                $categoria = new Categoria;
                $categoria->nombre = $request->categoria;
                $categoria->hotel_id = $idHotel;
                $categoria->save();
                $idCategoria = $categoria->id;
            }
            //echo 'hotel: '.$idHotel;
            //dd($idHotel);
            for ($i = 1; $i <= $pisos; $i++) {
                $p = new Pisos();
                $p->hotel_id = $idHotel;
                $p->nombre = 'Piso ' . $i;
                $p->estado = 'Activo';
                $p->save();
                $idPiso = $p->id;
                //echo 'Piso: ' . $idPiso;
                for ($c = 1; $c <= $habitaciones; $c++) {
                    echo 'Piso: ' . $i . 'Habitacion: ' . $c . 'camas por habitacion: ' . $camas . '<p>';
                    $h = new Habitaciones();
                    $h->piso_id = $idPiso;
                    //$h->hotel_id = $idHotel;
                    $h->camas = $camas;
                    $h->categoria_id = $idCategoria;
                    $h->nombre = $i.'0'.$c;
                    $h->estado = 'Habilitado';
                    $h->save();
                }
            }
            Flash::success('Se guardo correctamente.');
        } else {
            Flash::error('Error al guradar.');
        }

        /*Log::info($pisos);
        echo '<pre>';
        print_r($input);
        echo '</pre>';
        \Debugbar::info($input);
        echo $habitaciones;
        dd($pisos);

        \Debugbar::info($pisos);*/

        /*$hotel = $this->hotelRepository->create($input);1

        Flash::success('Hotel saved successfully.');*/

        return redirect(route('hotels.index'));
    }

    /**
     * Display the specified Hotel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('hotels.index'));
        }

        return view('hotels.show')->with('hotel', $hotel);
    }

    /**
     * Show the form for editing the specified Hotel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('hotels.index'));
        }

        return view('hotels.edit')->with('hotel', $hotel);
    }

    /**
     * Update the specified Hotel in storage.
     *
     * @param  int $id
     * @param UpdateHotelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHotelRequest $request)
    {
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('hotels.index'));
        }

        $hotel = $this->hotelRepository->update($request->all(), $id);

        Flash::success('Hotel updated successfully.');

        return redirect(route('hotels.index'));
    }

    /**
     * Remove the specified Hotel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hotel = $this->hotelRepository->findWithoutFail($id);

        if (empty($hotel)) {
            Flash::error('Hotel not found');

            return redirect(route('hotels.index'));
        }

        $this->hotelRepository->delete($id);

        Flash::success('Hotel deleted successfully.');

        return redirect(route('hotels.index'));
    }

    public function pisos()
    {

    }

    public function terminos_condiciones(Request $request){

        if($request->method() == "POST"){
            //dd($request->descripcion);
            if(!empty($request->id)){
                $opcione = Opcione::find($request->id);
            }else{
                $opcione = new Opcione();
            }
            $opcione->descripcion = $request->descripcion;
            $opcione->tipo = 'Terminos';
            $opcione->save();
            Flash::success('Se ha guardado correctamente los terminos y condiciones!!');
            return redirect()->back();
        }else{
            $opcione = Opcione::where('tipo','Terminos')->get()->first();
            return view('hotels.terminos_condiciones')->with(compact('opcione'));
        }
    }

    public function tiempo_cancelar_reg(Request $request){

        if($request->method() == "POST"){
            //dd($request->descripcion);
            if(!empty($request->id)){
                $opcione = Opcione::find($request->id);
            }else{
                $opcione = new Opcione();
            }
            $opcione->descripcion = $request->descripcion;
            $opcione->tipo = 'Tiempo cancelacion registro';
            $opcione->save();
            Flash::success('Se ha guardado correctamente los el tiempo de cancelacion del registro!!');
            return redirect()->back();
        }else{
            $opcione = Opcione::where('tipo','Tiempo cancelacion registro')->get()->first();
            return view('hotels.tiempo_cancelar_reg')->with(compact('opcione'));
        }
    }

    public function config_logo(Request $request){

        if($request->method() == "POST"){
            //dd($request->descripcion);
            //dd($request->file('archivo')->getClientOriginalExtension());

            if (!empty($request->file('archivo'))) {
                //$nombre_original = $request->file('archivo')->getClientOriginalName();
                $ext = $request->file('archivo')->getClientOriginalExtension();
                $nombre_uui = uniqid('', true);
                if ($request->file('archivo')->move('adjuntos', "$nombre_uui.$ext")) {

                    if(!empty($request->id)){
                        $opcione = Opcione::find($request->id);
                    }else{
                        $opcione = new Opcione();
                    }
                    $opcione->descripcion = "$nombre_uui.$ext";
                    $opcione->tipo = 'Logo';
                    $opcione->save();

                    Flash::success('Se ha guardado correctamente los el logo!!');
                    return redirect()->back();
                }
            }else{
                dd("entro aki");
            }
            Flash::error('No se ha podido guardar el logo!!');
            return redirect()->back();
        }else{
            $opcione = Opcione::where('tipo','Logo')->get()->first();
            return view('hotels.config_logo')->with(compact('opcione'));
        }


    }


    public function config_hora_lim(Request $request){

        if($request->method() == "POST"){

            if(!empty($request->id)){
                $opcione = Opcione::find($request->id);
            }else{
                $opcione = new Opcione();
            }
            $opcione->descripcion = $request->descripcion;
            $opcione->tipo = 'Hora limite de salida';
            $opcione->save();
            Flash::success('Se ha guardado correctamente la configuracion de la hora limite de configuracion!!');
            return redirect()->back();
        }else{
            $opcione = Opcione::where('tipo','Hora limite de salida')->get()->first();
            /*if(empty($opcione)){
                $opcione = new Opcione();
            }*/
            return view('hotels.config_hora_lim')->with(compact('opcione'));
        }
    }



    public function config_hora_camb_fecha(Request $request){

        if($request->method() == "POST"){

            if(!empty($request->id)){
                $opcione = Opcione::find($request->id);
            }else{
                $opcione = new Opcione();
            }
            $opcione->descripcion = $request->descripcion;
            $opcione->tipo = 'Hora cambio de fecha';
            $opcione->save();
            Flash::success('Se ha guardado correctamente la configuracion de la hora de cambio de fecha!!');
            return redirect()->back();
        }else{
            $opcione = Opcione::where('tipo','Hora cambio de fecha')->get()->first();
            return view('hotels.config_hora_camb_fecha')->with(compact('opcione'));
        }
    }
}
