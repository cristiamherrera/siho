<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Insumo;
use App\Models\Pisos;
use App\Models\Hotel;
use App\Movimiento;
use App\Totale;
use App\User;
use Flash;
use Illuminate\Support\Facades\Auth;

class InsumoController extends Controller
{
    //
    public function index($idHotel = null)
    {
        $hoteles = Hotel::get();
        $idHotel_aux = Auth::user()->hotel_id;
        if(empty($idHotel) && $idHotel_aux != null){
            $idHotel = $idHotel_aux;
        }
        $hotel = Hotel::find($idHotel);
        //join('totales', 'id', '=', 'totales.insumo_id')->
        $insumos = Insumo::where('insumos.deleted_at', null)->orderBy('id', 'desc')->get();
        //dd(Auth::user()->hotel_id);
        return view('insumos.index')->with(compact('insumos', 'hoteles', 'idHotel', 'hotel'));
    }

    public function insumo($idInsumo = null)
    {
        if (isset($idInsumo)) {
            $insumo = Insumo::find($idInsumo);
        }
        $categorias = Insumo::select('categoria')->groupBy('categoria')->get()->pluck('categoria');
        // print_r($categorias);die();
        return view('insumos.insumo')->with(compact('insumo', 'categorias'));
    }

    public function guarda_insumo(Request $request, $idInsumo = null)
    {
        if (isset($idInsumo)) {
            $insumo = Insumo::find($idInsumo);
            $insumo->nombre = $request->nombre;
            $insumo->medida = 'Unidades';
            $insumo->cantidad = 1;
            $insumo->descripcion = $request->descripcion;
            $insumo->categoria = $request->categoria;
            $insumo->save();
        } else {
            $insumo = new Insumo;
            $insumo->nombre = $request->nombre;
            $insumo->medida = 'Unidades';
            $insumo->cantidad = 1;
            $insumo->descripcion = $request->descripcion;
            $insumo->categoria = $request->categoria;
            $insumo->save();
        }
        Flash::success('Se ha registrado correctamente el insumo!!!');

        return redirect()->back();
    }

    public function eliminar_insumo($idInsumo)
    {
        $insumo = Insumo::find($idInsumo);
        return redirect()->back();
        $insumo->deleted_at = date("Y-m-d H:i:s");
        $insumo->save();
        Flash::success('Se ha eliminado correctamente el insumo!!!');
    }

    public function ingreso_insumo($idInsumo, $idHotel = null)
    {
        //print_r(Auth::user());
        //print_r(Auth::user()->hotel_id);
        $id_hotel = Auth::user()->hotel_id;
        $insumo = Insumo::find($idInsumo);
        $hoteles = Hotel::get()->pluck('nombre', 'id')->all();
        // $operarios = User::find()
        $operarios = User::where('hotel_id',$idHotel)->orderBy('name')->get()->pluck('name','id');
        $medidas = [];
        $medidas['Unidades de Insumo'] = 'Unidades de Insumo';

        $medidas[$insumo->medida] = $insumo->medida;
        return view('insumos.ingreso_insumo')->with(compact('hoteles', 'insumo', 'medidas', 'idHotel', 'operarios'));
    }

    public function guarda_ingre_insumo(Request $request)
    {
        //dd($request->all());
        $movimiento = new Movimiento;
        $insumo = Insumo::find($request->insumo_id);

        if ($request->medida == 'Unidades de Insumo') {
            $cantidad = $insumo->cantidad * $request->cantidad;
        } else {
            $cantidad = $request->cantidad;
        }

        if ($request->tipo == 'Ingreso') {
            if (!empty($request->aux_hotel_id)) {
                $movimiento->hotel_id = $request->aux_hotel_id;
                $dd_idhotel = $request->aux_hotel_id;
                $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', $request->aux_hotel_id)->get()->first();
            } else {
                $dd_idhotel = null;
                $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', null)->get()->first();
            }

            if (!isset($total_d_cen)) {
                $total_d_cen = new Totale;
                $total_d_cen->insumo_id = $request->insumo_id;
                $total_d_cen->cantidad = $cantidad;
                $total_d_cen->hotel_id = $dd_idhotel;
                $total_d_cen->save();
            } else {
                $total_d_cen->insumo_id = $request->insumo_id;
                $total_d_cen->cantidad = $total_d_cen->cantidad + $cantidad;
                $total_d_cen->hotel_id = $dd_idhotel;
                $total_d_cen->save();
            }
        } else {
            $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', null)->get()->first();
            $movimiento->hotel_id = $request->hotel_id;
            if (isset($total_d_cen) && $total_d_cen->cantidad >= $cantidad) {
                $total_d_hot = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', $request->hotel_id)->get()->first();
                if (!isset($total_d_hot)) {
                    $total_d_hot = new Totale;
                    $total_d_hot->hotel_id = $request->hotel_id;
                    $total_d_hot->insumo_id = $request->insumo_id;
                }
                $total_d_hot->cantidad = $total_d_hot->cantidad + $cantidad;
                $total_d_hot->save();

                $total_d_cen->cantidad = $total_d_cen->cantidad - $cantidad;
                $total_d_cen->save();
            } else {
                Flash::error("La cantidad en central no es suficiente!!!");
                return redirect()->back();
            }
        }

        $movimiento->tipo = $request->tipo;
        $movimiento->insumo_id = $request->insumo_id;
        $movimiento->medida = 'Unidades';
        $movimiento->asignado_id = $request->asignado_id;
        $movimiento->cantidad = $request->cantidad;
        $movimiento->estado = "Ingreso";
        $movimiento->user_id = Auth::user()->id;
        $movimiento->save();
        Flash::success('Se ha registrado el ingreso correctamente!!!');
        return redirect()->back();
    }


    public function movimientos_in($idInsumo, $idHotel = null)
    {
        $hoteles = Hotel::get();
        $hotel = Hotel::find($idHotel);
        $insumo = Insumo::find($idInsumo);

        if (!empty($idHotel)) {
            $movimientos = Movimiento::where('insumo_id', $idInsumo)->where('hotel_id', $idHotel)->orderBy('id', 'desc')->get();
        } else {
            $movimientos = Movimiento::where('insumo_id', $idInsumo)
                ->where(function ($query) {
                    $query->where('tipo', 'Ingreso');
                    $query->where('hotel_id', 0);
                })->orWhere(function ($query) {
                    $query->where('tipo', 'Ingreso a Hotel');
                })->orWhere(function ($query) {
                    $query->where('tipo', 'Salida');
                    $query->where('hotel_id', 0);
                })->orderBy('id', 'desc')->get();
            //dd($movimientos->toSql());
        }

        return view('insumos.movimientos_in')->with(compact('hoteles', 'hotel', 'insumo', 'movimientos'));
    }

    public function eliminar_movimiento($idMovimiento)
    {
        $movimiento = Movimiento::find($idMovimiento);

        if ($movimiento->medida == 'Unidades de Insumo') {
            $cantidad = $movimiento->insumo->cantidad * $movimiento->cantidad;
        } else {
            $cantidad = $movimiento->cantidad;
        }
        if ($movimiento->tipo == "Ingreso a Hotel") {
            $total_d_hot = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', $movimiento->hotel_id)->get()->first();
            if ($total_d_hot->cantidad >= $cantidad) {
                $total_d_hot->cantidad = $total_d_hot->cantidad - $cantidad;
                $total_d_hot->save();
                $total_d_cen = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', null)->get()->first();
                $total_d_cen->cantidad = $total_d_cen->cantidad + $cantidad;
                $total_d_cen->save();
            } else {
                Flash::error("La cantidad en hotel no es suficiente!!!");
                return redirect()->back();
            }
        } elseif ($movimiento->tipo == "Ingreso" && $movimiento->hotel_id == 0) {
            $total_d_cen = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', null)->get()->first();
            if ($total_d_cen->cantidad >= $cantidad) {
                $total_d_cen->cantidad = $total_d_cen->cantidad - $cantidad;
                $total_d_cen->save();
            } else {
                Flash::error("La cantidad en central no es suficiente!!!");
                return redirect()->back();
            }
        } elseif ($movimiento->tipo == "Ingreso" && $movimiento->hotel_id != 0) {
            $total_d_hot = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', $movimiento->hotel_id)->get()->first();
            if ($total_d_hot->cantidad >= 0) {
                $total_d_hot->cantidad = $total_d_hot->cantidad - $cantidad;
                $total_d_hot->save();
            } else {
                Flash::error("La cantidad en hotel no es suficiente!!!");
                return redirect()->back();
            }
        }elseif ($movimiento->tipo == "Salida" && $movimiento->hotel_id == 0){
            $total_d_cen = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', null)->get()->first();
            $total_d_cen->cantidad = $total_d_cen->cantidad + $cantidad;
            $total_d_cen->save();
        }elseif($movimiento->tipo == "Salida" && $movimiento->hotel_id != 0){
            $total_d_hot = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', $movimiento->hotel_id)->get()->first();
            $total_d_hot->cantidad = $total_d_hot->cantidad + $cantidad;
            $total_d_hot->save();
        }elseif($movimiento->tipo == "Salida a Central" && $movimiento->hotel_id != 0){
            $total_d_cen = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', null)->get()->first();
            if($total_d_cen->cantidad >= $cantidad){
                $total_d_cen->cantidad = $total_d_cen->cantidad - $cantidad;
                $total_d_cen->save();
                $total_d_hot = Totale::where('insumo_id', $movimiento->insumo_id)->where('hotel_id', $movimiento->hotel_id)->get()->first();
                $total_d_hot->cantidad = $total_d_hot->cantidad + $cantidad;
                $total_d_hot->save();
            }else {
                Flash::error("La cantidad no es suficiente!!!");
                return redirect()->back();
            }
        }
        $movimiento->deleted_at = date('Y-m-d H:i');
        $movimiento->save();
        Flash::success("Se ha cancelado correctamente el movimiento!!!");
        return redirect()->back();
    }


    public function salida_insumo($idInsumo, $idHotel = null)
    {
        //dd($idHotel);
        // $idHotel= Auth::user()->hotel_id;
        $insumo = Insumo::find($idInsumo);
        $total = Totale::where('hotel_id', $idHotel)->where('insumo_id', $idInsumo)->get()->first();
        //$hoteles = Hotel::get()->pluck('nombre', 'id')->all();
        $medidas = [];
        $medidas['Unidades de Insumo'] = 'Unidades de Insumo';
        if(!empty($idHotel)){
            $operarios = User::where('hotel_id',$idHotel)->orderBy('name')->get()->pluck('name','id');
            $pisos = Pisos::where('hotel_id', $idHotel)->orderBy('nombre')->get()->pluck('nombre', 'id');
        }else{
            $operarios = '';
        }
        $medidas[$insumo->medida] = $insumo->medida;
        return view('insumos.salida_insumo')->with(compact('insumo', 'medidas', 'idHotel', 'operarios', 'pisos', 'total'));
    }


    public function guarda_salida_insumo(Request $request)
    {
        //dd($request->all());
        $movimiento = new Movimiento;
        $insumo = Insumo::find($request->insumo_id);

        if ($request->medida == 'Unidades de Insumo') {
            $cantidad = $insumo->cantidad * $request->cantidad;
        } else {
            $cantidad = $request->cantidad;
        }


        if ($request->tipo == 'Salida') {
            if (!empty($request->hotel_id)) {
                $movimiento->hotel_id = $request->hotel_id;
                $dd_idhotel = $request->hotel_id;
                $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', $request->hotel_id)->get()->first();
            } else {
                $dd_idhotel = null;
                $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', null)->get()->first();
            }

            if (isset($total_d_cen)){
                if($total_d_cen->cantidad >= $cantidad){
                    $total_d_cen->insumo_id = $request->insumo_id;
                    $total_d_cen->cantidad = $total_d_cen->cantidad - $cantidad;
                    $total_d_cen->hotel_id = $dd_idhotel;
                    $total_d_cen->save();
                }else{
                    Flash::error("La cantidad  no es suficiente!!!");
                    return redirect()->back();
                }
            } else {
                Flash::error("La cantidad  no es suficiente!!!");
                return redirect()->back();
            }
        } else {
            $total_d_hot = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', $request->hotel_id)->get()->first();

//            $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', null)->get()->first();
            $movimiento->hotel_id = $request->hotel_id;
            if (isset($total_d_hot) && $total_d_hot->cantidad >= $cantidad) {
                $total_d_cen = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', null)->get()->first();

//                $total_d_hot = Totale::where('insumo_id', $request->insumo_id)->where('hotel_id', $request->hotel_id)->get()->first();
                if (!isset($total_d_cen)) {
                    $total_d_cen = new Totale;
                    $total_d_cen->hotel_id = $request->hotel_id;
                    $total_d_cen->insumo_id = $request->insumo_id;
                }
                $total_d_cen->cantidad = $total_d_cen->cantidad + $cantidad;
                $total_d_cen->save();

                $total_d_hot->cantidad = $total_d_hot->cantidad - $cantidad;
                $total_d_hot->save();
            } else {
                Flash::error("La cantidad en hotel no es suficiente!!!");
                return redirect()->back();
            }
        }
        $a = (empty($request->operarios))? '0' : $request->operarios;
        $p = (empty($request->pisos))? '0' : $request->pisos;
        $movimiento->tipo = $request->tipo;
        $movimiento->insumo_id = $request->insumo_id;
        $movimiento->medida = "Unidades";
        $movimiento->asignado_id = $a;
        $movimiento->piso_id = $p;
        $movimiento->cantidad = $request->cantidad;
        $movimiento->estado = "Salida";
        $movimiento->user_id = Auth::user()->id;
        $movimiento->save();
        Flash::success('Se ha registrado la salida correctamente!!!');
        return redirect()->back();
    }

}
