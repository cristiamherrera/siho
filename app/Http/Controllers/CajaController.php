<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests\CreateCajaRequest;
use App\Http\Requests\UpdateCajaRequest;
use App\Models\Caja;
use App\Models\Flujo;
use App\Pago;
use App\Repositories\CajaRepository;
use App\User;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;
use Yajra\Datatables\Datatables;

class CajaController extends InfyOmBaseController
{
    /** @var  CajaRepository */
    private $cajaRepository;

    public function __construct(CajaRepository $cajaRepo)
    {
        $this->cajaRepository = $cajaRepo;
    }

    /**
     * Display a listing of the Caja.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {

        $idHotel = Auth::user()->hotel_id;
        $cajas = Caja::all()->where('hotel_id', $idHotel);
        return view('cajas.index')
            ->with('cajas', $cajas);
    }

    /**
     * Show the form for creating a new Caja.
     *
     * @return Response
     */
    public function create()
    {
        return view('cajas.create');
    }

    /**
     * Store a newly created Caja in storage.
     *
     * @param CreateCajaRequest $request
     *
     * @return Response
     */
    public function store(CreateCajaRequest $request)
    {
        $input = $request->all();

        $caja = $this->cajaRepository->create($input);

        Flash::success('Caja saved successfully.');

        return redirect(route('cajas.index'));
    }

    /**
     * Display the specified Caja.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja not found');

            return redirect(route('cajas.index'));
        }

        return view('cajas.show')->with('caja', $caja);
    }

    /**
     * Show the form for editing the specified Caja.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja not found');

            return redirect(route('cajas.index'));
        }

        return view('cajas.edit')->with('caja', $caja);
    }

    /**
     * Update the specified Caja in storage.
     *
     * @param  int $id
     * @param UpdateCajaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCajaRequest $request)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja not found');

            return redirect(route('cajas.index'));
        }

        $caja = $this->cajaRepository->update($request->all(), $id);

        Flash::success('Caja updated successfully.');

        return redirect(route('cajas.index'));
    }

    /**
     * Remove the specified Caja from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja not found');

            return redirect(route('cajas.index'));
        }

        $this->cajaRepository->delete($id);

        Flash::success('Caja deleted successfully.');

        return redirect(route('cajas.index'));
    }

    public function flujos($idCaja)
    {
        $idHotel_aut = Auth::user()->hotel_id;
        $caja = Caja::where('id', $idCaja)->where('hotel_id', $idHotel_aut)->first();
        $flujos = Flujo::where('caja_id', $idCaja)
            ->whereHas('caja', function ($query) use ($idHotel_aut) {
                $query->where('hotel_id', $idHotel_aut);
            })->orderBy('id', 'desc')->limit(100)->get();
        return view('cajas.flujos')->with(compact('caja', 'flujos'));
    }

    public function ingreso($idCaja)
    {
        return view('cajas.ingreso')->with(compact('idCaja'));
    }

    public function egreso($idCaja)
    {
        return view('cajas.egreso')->with(compact('idCaja'));
    }

    public function eliminaflujo($idFlujo)
    {
        $flujo = Flujo::find($idFlujo);
        $ultima_entrega = Flujo::whereIn('detalle', ['ENTREGA DE CAJA CHICA', 'TRASPASO DE CAJA CHICA'])->where('id', '>', $idFlujo)->first();
//        dd($ultima_entrega);
        return view('cajas.eliminaflujo')->with(compact('idFlujo', 'flujo', 'ultima_entrega'));
    }

    public function guarda_ingreso(Request $request)
    {

        $flujo = new Flujo;
        $flujo->detalle = $request->detalle;
        $flujo->ingreso = $request->ingreso;
        $flujo->observacion = $request->observacion;
        $flujo->salida = $request->salida;
        $flujo->caja_id = $request->caja_id;
        $flujo->user_id = $request->user_id;
        $flujo->save();

        $total = $this->get_total($request->caja_id);
        $this->set_total($request->caja_id, ($total + $request->ingreso));

        Flash::success('El ingreso se ha registrado correctamente!!');
        return redirect()->back();

    }

    public function guarda_egreso(Request $request)
    {

        $total = $this->get_total($request->caja_id);

        if ($total >= $request->salida) {
            $flujo = new Flujo;
            $flujo->detalle = $request->detalle;
            $flujo->ingreso = $request->ingreso;
            $flujo->observacion = $request->observacion;
            $flujo->salida = $request->salida;
            $flujo->caja_id = $request->caja_id;
            $flujo->user_id = $request->user_id;
            $flujo->save();

            $this->set_total($request->caja_id, ($total - $request->salida));
            Flash::success('El egreso se ha registrado correctamente!!');
        } else {
            Flash::error('No se pudo registral el egreso por q solo hay ' . $total . ' en caja!!');
        }

        return redirect()->back();

    }

    public function get_total($idCaja)
    {
        $caja = Caja::find($idCaja);
        return $caja->total;
    }

    public function set_total($idCaja, $total = 0.00)
    {
        $caja = Caja::find($idCaja);
        $caja->total = $total;
        $caja->save();
        return true;
    }

    public function eliminar_flujo(Request $request, $idFlujo)
    {
        //dd($idFlujo);
        //dd($request->all());

        $flujo = Flujo::find($idFlujo);
        $total = $this->get_total($flujo->caja_id);
        $idUsuario = Auth::user()->id;

        if ($flujo->detalle == 'ENTREGA DE CAJA CHICA') {
            $flujo->deleted_at = date('Y-m-d H:m:i');
            $flujo->observacion = $request->observacion;
            $flujo->save();

            $salida_deuda = 0;
            $deuda_f = Flujo::where('flujoid', $idFlujo)->where('detalle', 'DEUDA DE ENTREGA')->first();
            if (!empty($deuda_f)) {
                $deuda_f->deleted_at = date('Y-m-d H:m:i');
                $deuda_f->observacion = $request->observacion;
                $deuda_f->save();
                $salida_deuda = $deuda_f->salida;
            }
            $m_flujos = Flujo::whereNull('deleted_at')->where('flujoid', $idFlujo)->get();
            foreach ($m_flujos as $m_flujo) {
                $m_flujo->flujoid = null;
                $m_flujo->save();
            }
            $this->set_total($flujo->caja_id, ($total + $flujo->salida + $salida_deuda));
        } /*elseif ($flujo->detalle == 'DEUDA DE ENTREGA') {
        $flujo->deleted_at = date('Y-m-d H:m:i');
        $flujo->observacion = $request->observacion;
        $flujo->save();

        $entrega_f = Flujo::where('id', $flujo->flujoid)->where('detalle', 'ENTREGA DE CAJA CHICA')->first();
        $entrega_f->deleted_at = date('Y-m-d H:m:i');
        $entrega_f->observacion = $request->observacion;
        $entrega_f->save();
        $salida_entrega = $entrega_f->salida;
        $m_flujos = Flujo::whereNull('deleted_at')->where('flujoid', $flujo->flujoid)->get();
        foreach ($m_flujos as $m_flujo) {
        $m_flujo->flujoid = null;
        $m_flujo->save();
        }
        $this->set_total($flujo->caja_id, ($total + $flujo->salida + $salida_entrega));
        }*/elseif ($flujo->detalle == 'TRASPASO DE CAJA CHICA' && $flujo->ingreso > 0) {
            $flujo->deleted_at = date('Y-m-d H:m:i');
            $flujo->observacion = $request->observacion;
            $flujo->save();

            $flujo_2 = Flujo::where('flujoid', $flujo->id)->where('salida', '>', 0)->where('detalle', 'TRASPASO DE CAJA CHICA')->first();
            $flujo_2->deleted_at = date('Y-m-d H:m:i');
            $flujo_2->observacion = $request->observacion;
            $flujo_2->save();

            $salida_deuda = 0;
            $deuda_f = Flujo::where('flujoid', $flujo->id)->where('detalle', 'DEUDA DE TRASPASO')->first();
            if (!empty($deuda_f)) {
                $deuda_f->deleted_at = date('Y-m-d H:m:i');
                $deuda_f->observacion = $request->observacion;
                $deuda_f->save();
                $salida_deuda = $deuda_f->salida;
            }

            $m_flujos = Flujo::whereNull('deleted_at')->where('flujoid', $flujo->id)->get();
            foreach ($m_flujos as $m_flujo) {
                $m_flujo->flujoid = null;
                $m_flujo->save();
            }
            $this->set_total($flujo->caja_id, ($total + $salida_deuda));
        } elseif ($flujo->detalle == 'TRASPASO DE CAJA CHICA' && $flujo->salida > 0) {
            $flujo->deleted_at = date('Y-m-d H:m:i');
            $flujo->observacion = $request->observacion;
            $flujo->save();

            $flujo_2 = Flujo::where('id', $flujo->flujoid)->where('ingreso', '>', 0)->where('detalle', 'TRASPASO DE CAJA CHICA')->first();
            $flujo_2->deleted_at = date('Y-m-d H:m:i');
            $flujo_2->observacion = $request->observacion;
            $flujo_2->save();

            $salida_deuda = 0;
            $deuda_f = Flujo::where('flujoid', $flujo->flujoid)->where('detalle', 'DEUDA DE TRASPASO')->first();
            if (!empty($deuda_f)) {
                $deuda_f->deleted_at = date('Y-m-d H:m:i');
                $deuda_f->observacion = $request->observacion;
                $deuda_f->save();
                $salida_deuda = $deuda_f->salida;
            }

            $m_flujos = Flujo::whereNull('deleted_at')->where('flujoid', $flujo->flujoid)->get();
            foreach ($m_flujos as $m_flujo) {
                $m_flujo->flujoid = null;
                $m_flujo->save();
            }
            $this->set_total($flujo->caja_id, ($total + $salida_deuda));
        } elseif (!empty($request->opcion)) {
            if ($request->opcion == 'Cancelar deuda') {
                $flujo_deu = new Flujo;
                $flujo_deu->detalle = 'DEUDA CANCELADA';
                $flujo_deu->ingreso = $flujo->salida;
                $flujo_deu->observacion = '';
                $flujo_deu->salida = 0;
                $flujo_deu->caja_id = $flujo->caja_id;
                $flujo_deu->user_id = $idUsuario;
                $flujo_deu->save();

                $idFlujo_deuda = $flujo_deu->id;

                $flujo->deleted_at = date('Y-m-d H:m:i');
                $flujo->flujoid = $idFlujo_deuda;
                $flujo->save();

                $this->set_total($flujo->caja_id, ($total + $flujo->salida));
            } else {
                $flujo->deleted_at = date('Y-m-d H:m:i');
                $flujo->observacion = '';
                $flujo->save();
            }
        } else {
            if ($flujo->ingreso != 0) {
                if ($total >= $flujo->ingreso) {
                    $this->set_total($flujo->caja_id, ($total - $flujo->ingreso));
                } else {
                    Flash::error('No se ha podido eliminar el flujo por que el total es solo ' . $total);
                    return redirect()->back();
                }
            } else {
                $this->set_total($flujo->caja_id, ($total + $flujo->salida));
            }

            if ($flujo->detalle == 'DEUDA CANCELADA') {
                $deud_ff = Flujo::where('flujoid', $flujo->id)->first();
                $deud_ff->deleted_at = null;
                $deud_ff->flujoid = null;
                $deud_ff->observacion = '';
                $deud_ff->save();
            }

            $flujo->deleted_at = date('Y-m-d H:m:i');
            $flujo->observacion = $request->observacion;
            $flujo->save();

            $pagos = Pago::where('flujo_id', $idFlujo)->get();
            foreach ($pagos as $pago) {
                $pago->estado = 'Deuda';
                $pago->flujo_id = null;
                $pago->save();
            }
        }

        Flash::success('Se ha eliminado correctamente el flujo!!');
        return redirect()->back();
    }

    public function soloingresos($idCaja)
    {
        $caja = Caja::find($idCaja);
        $pagos_sr = Pago::whereHas('flujo', function ($query) use ($idCaja) {
            $query->where('caja_id', $idCaja);
        })
            ->whereHas('registro', function ($query) use ($idCaja) {
                $query->where('deleted_at', null);
            })
            ->orderBy('fecha', 'desc')
            ->where('retiroflujo_id', null)
            ->where('estado', 'Pagado')->get();
        $idHotel = Auth::user()->hotel_id;
        $idUsuario = Auth::user()->id;
        $usuarios = User::where('hotel_id', $idHotel)->where('id', '<>', $idUsuario)->get()->pluck('name', 'id')->all();
        return view('cajas.soloingresos')->with(compact('idCaja', 'caja', 'pagos_sr', 'usuarios'));
    }

    public function soloingresosdia($idCaja)
    {
        $caja = Caja::find($idCaja);

        $pagos_sr = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
//            ->leftJoin('pagos', 'pagos.flujo_id', '=', 'flujos.id')
        //            ->where('flujos.ingreso','>',0)
            ->where('flujos.caja_id', $idCaja)
            ->where('flujos.deleted_at', null)
            ->where('flujos.flujoid', null)
            ->groupBy('lafecha')
            ->orderBy('lafecha', 'asc')
            ->get();

//        dd($pagos_sr->toArray());
        $idHotel = Auth::user()->hotel_id;
        $idUsuario = Auth::user()->id;
        $usuarios = User::where('hotel_id', $idHotel)->where('id', '<>', $idUsuario)->get()->pluck('name', 'id')->all();

        $ultimo_retio = Flujo::select('created_at')
            ->where('flujos.caja_id', $idCaja)
            ->where('flujos.deleted_at', null)
            ->where('detalle', 'ENTREGA DE CAJA CHICA')
            ->orderBy('created_at', 'desc')
            ->first();

        if (!empty($ultimo_retio->created_at)) {
            $flujos_det = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha,users.name AS usuario, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
                ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
                ->where('flujos.caja_id', $idCaja)
                ->where('flujos.deleted_at', null)
                ->where('flujos.created_at', '>', $ultimo_retio->created_at)
                ->havingRaw('(SUM(flujos.ingreso)-SUM(flujos.salida)) <> 0')
                ->groupBy('lafecha', 'usuario')
                ->orderBy('lafecha', 'asc')
                ->get();
        } else {
            $flujos_det = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha,users.name AS usuario, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
                ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
                ->where('flujos.caja_id', $idCaja)
                ->where('flujos.deleted_at', null)
                ->havingRaw('(SUM(flujos.ingreso)-SUM(flujos.salida)) <> 0')
                ->groupBy('lafecha', 'usuario')
                ->orderBy('lafecha', 'asc')
                ->get();
        }

        /*$total = 0;
        foreach ($flujos_det as $da){
        $total+=$da->monto_total;
        }
        print_r($total);
        dd($flujos_det->toArray());*/

        return view('cajas.soloingresosdia')->with(compact('idCaja', 'caja', 'pagos_sr', 'usuarios', 'flujos_det'));
    }

    public function retirarpagos(Request $request)
    {
        $idUser = Auth::user()->id;
        if (!empty($request->pagos)) {
            $total = $this->get_total($request->caja_id);
            if ($total >= $request->salida) {
                $flujo = new Flujo;
                $flujo->detalle = "RETIRO DE PAGOS-INGRESOS";
                $flujo->ingreso = 0;
                // $flujo->observacion = $request->observacion;
                $flujo->observacion = '';
                $flujo->salida = $request->salida;
                $flujo->caja_id = $request->caja_id;
                $flujo->user_id = $idUser;
                $flujo->ruser_id = $request->ruser_id;
                $flujo->save();
                $idFlujo = $flujo->id;
                foreach ($request->pagos as $idPago => $rpago) {
                    $pago = Pago::find($idPago);
                    $pago->retiroflujo_id = $idFlujo;
                    $pago->save();
                }
                $this->set_total($request->caja_id, ($total - $request->salida));
                Flash::success('El Retiro y egreso se ha registrado correctamente!!');
            } else {
                Flash::error('No se pudo registral el egreso por q solo hay ' . $total . ' en caja!!');
            }
        } else {
            Flash::error('No ha seleccionado ningun pago!!');
        }
        return redirect()->back();
    }

    public function retirarpagosdia(Request $request)
    {

        $idUser = Auth::user()->id;
        $role = Auth::user()->rol;
        if (!empty($request->pagos)) {
            $idCaja = $request->caja_id;
            $total = $this->get_total($request->caja_id);
            if ($total >= $request->salida) {

                if ($role == 'Operario') {
                    $flujo_i = new Flujo;
                    $flujo_i->detalle = 'TRASPASO DE CAJA CHICA';
                    $flujo_i->ingreso = $request->salida_real;
                    $flujo_i->observacion = '';
                    $flujo_i->salida = 0;
                    $flujo_i->caja_id = $request->caja_id;
                    $flujo_i->user_id = $idUser;
                    $flujo_i->ruser_id = $request->ruser_id;
                    $flujo_i->save();
                    $flujo_i->flujoid = 0;
                    $idFlujo = $flujo_i->id;

                    $flujo = new Flujo;
                    $flujo->detalle = 'TRASPASO DE CAJA CHICA';
                    $flujo->ingreso = 0;
                    $flujo->observacion = '';
                    $flujo->salida = $request->salida_real;
                    $flujo->caja_id = $request->caja_id;
                    $flujo->user_id = $idUser;
                    $flujo->ruser_id = $request->ruser_id;
                    $flujo->flujoid = $idFlujo;
                    $flujo->save();

                    if (!empty($request->salida_deuda)) {
                        $flujo_d = new Flujo;
                        $flujo_d->detalle = 'DEUDA DE TRASPASO';
                        $flujo_d->ingreso = 0;
                        $flujo_d->observacion = '';
                        $flujo_d->salida = $request->salida_deuda;
                        $flujo_d->caja_id = $request->caja_id;
                        $flujo_d->user_id = $request->ruser_id;
                        $flujo_d->flujoid = $idFlujo;
                        $flujo_d->save();
                    }

                } else {
                    $flujo = new Flujo;
                    $flujo->detalle = 'ENTREGA DE CAJA CHICA';
                    $flujo->ingreso = 0;
                    $flujo->observacion = '';
                    $flujo->salida = $request->salida_real;
                    $flujo->caja_id = $request->caja_id;
                    $flujo->user_id = $idUser;
                    $flujo->ruser_id = $request->ruser_id;
                    $flujo->flujoid = 0;
                    $flujo->save();
                    $idFlujo = $flujo->id;

                    if (!empty($request->salida_deuda)) {
                        $flujo_d = new Flujo;
                        $flujo_d->detalle = "DEUDA DE ENTREGA";
                        $flujo_d->ingreso = 0;
                        $flujo_d->observacion = '';
                        $flujo_d->salida = $request->salida_deuda;
                        $flujo_d->caja_id = $request->caja_id;
                        $flujo_d->user_id = $request->ruser_id;
                        $flujo_d->flujoid = $idFlujo;
                        $flujo_d->save();
                    }
                }

                foreach ($request->pagos as $fecha => $rpago) {
                    $d_flujos = Flujo::where('flujos.deleted_at', null)
                        ->where(DB::raw('DATE(flujos.created_at)'), $fecha)
                        ->where('flujos.flujoid', null)
                        ->where('flujos.caja_id', $idCaja)->get();

                    foreach ($d_flujos as $d_flujo) {
                        $d_flujo->flujoid = $idFlujo;
                        $d_flujo->save();
                    }
                }

                if ($role == 'Operario') {
                    $flujo_i->flujoid = null;
                    $flujo_i->save();
                    $this->set_total($request->caja_id, ($total - $request->salida_deuda));
                } else {
                    $this->set_total($request->caja_id, ($total - $request->salida));
                }

                Flash::success('La entrega se ha registrado correctamente!!');
            } else {
                Flash::error('No se pudo registrar la entrega por que solo hay ' . $total . ' en caja!!');
            }
        } else {
            Flash::error('No hay ningun flujo que entregar!!');
        }
        return redirect()->back();
    }

    public function ultimosretiros($idCaja)
    {
        $idHotel_aut = Auth::user()->hotel_id;
        $caja = Caja::where('id', $idCaja)->where('hotel_id', $idHotel_aut)->first();
        //$caja = Caja::find($idCaja);

        $pagos_cr = Pago::whereHas('flujo', function ($query) use ($idCaja) {
            $query->where('caja_id', $idCaja);
        })->where('retiroflujo_id', '<>', null)->orderBy('retiroflujo_id', 'desc')->take(60)->get();
        return view('cajas.ultimosretiros')->with(compact('idCaja', 'caja', 'pagos_cr'));
    }

    public function recibo_retiro($idFlujo)
    {
        $flujo = Flujo::find($idFlujo);

        $ultimo_retio = Flujo::select('created_at')
            ->where('flujos.caja_id', $flujo->caja_id)
            ->where('flujos.deleted_at', null)
            ->where('flujos.created_at', '<', $flujo->created_at)
            ->where('detalle', 'ENTREGA DE CAJA CHICA')
            ->orderBy('created_at', 'desc')
            ->first();

        $deuda = Flujo::where('flujoid', $idFlujo)->whereIn('detalle', ['DEUDA DE ENTREGA', 'DEUDA DE TRASPASO'])->first();

        if (!empty($ultimo_retio->created_at)) {
            $flujos_det = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha,users.name AS usuario, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
                ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
                ->where('flujos.caja_id', $flujo->caja_id)
                ->where('flujos.deleted_at', null)
                ->where('flujos.created_at', '<', $flujo->created_at)
                ->where('flujos.created_at', '>', $ultimo_retio->created_at)
                ->havingRaw('(SUM(flujos.ingreso)-SUM(flujos.salida)) <> 0')
                ->groupBy('lafecha', 'usuario')
                ->orderBy('lafecha', 'asc')
                ->get();
        } else {
            $flujos_det = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha,users.name AS usuario, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
                ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
                ->where('flujos.caja_id', $flujo->caja_id)
                ->where('flujos.deleted_at', null)
                ->where('flujos.created_at', '<', $flujo->created_at)
                ->havingRaw('(SUM(flujos.ingreso)-SUM(flujos.salida)) <> 0')
                ->groupBy('lafecha', 'usuario')
                ->orderBy('lafecha', 'asc')
                ->get();
        }

        return view('cajas.recibo_retiro')->with(compact('flujo', 'flujos_det', 'deuda'));
    }

    public function recibo_retiroxdia($idFlujo)
    {
        $flujo = Flujo::find($idFlujo);

        $ultimo_retio = Flujo::select('created_at')
            ->where('flujos.caja_id', $flujo->caja_id)
            ->where('flujos.deleted_at', null)
            ->where('flujos.created_at', '<', $flujo->created_at)
            ->where('detalle', 'ENTREGA DE CAJA CHICA')
            ->orderBy('created_at', 'desc')
            ->first();

        if (!empty($ultimo_retio->created_at)) {
            $flujos_det = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
                ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
                ->where('flujos.caja_id', $flujo->caja_id)
                ->where('flujos.deleted_at', null)
                ->where('flujos.created_at', '<', $flujo->created_at)
                ->where('flujos.created_at', '>', $ultimo_retio->created_at)
                ->orWhere(function ($query) use ($idFlujo) {
                    $query->whereIn('detalle',['DEUDA DE ENTREGA','DEUDA DE TRASPASO'])
                        ->where('flujoid',$idFlujo);
                })
                ->havingRaw('(SUM(flujos.ingreso)-SUM(flujos.salida)) <> 0')
                ->groupBy('lafecha')
                ->orderBy('lafecha', 'asc')
                ->get();
        } else {
            $flujos_det = Flujo::select(DB::raw('DATE(flujos.created_at) AS lafecha, (SUM(flujos.ingreso)-SUM(flujos.salida)) as monto_total'))
                ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
                ->where('flujos.caja_id', $flujo->caja_id)
                ->where('flujos.deleted_at', null)
                ->where('flujos.created_at', '<', $flujo->created_at)
                ->orWhere(function ($query) use ($idFlujo) {
                    $query->whereIn('detalle',['DEUDA DE ENTREGA','DEUDA DE TRASPASO'])
                        ->where('flujoid',$idFlujo);
                })
                ->havingRaw('(SUM(flujos.ingreso)-SUM(flujos.salida)) <> 0')
                ->groupBy('lafecha')
                ->orderBy('lafecha', 'asc')
                ->get();
        }

        return view('cajas.recibo_retiroxdia')->with(compact('flujo', 'flujos_det'));
    }

    public function datatableflujos($idCaja, Request $request)
    {
        $idHotel = Auth::user()->hotel_id;
        $idUsuario = Auth::user()->id;
        $rol = Auth::user()->rol;

/*        $sql_1 = <<<EOE
SELECT SUM(pagos.monto_total) as monto_total
FROM pagos
WHERE pagos.registro_id = registros.id AND pagos.estado IN ('Deuda','Deuda Extra')
GROUP BY registros.grupo_id
EOE;

$s_deuda = '<button type="button" class="btn btn-block btn-warning btn-sm">DEBE</button>';

$registros = Registro::query()
->select('grupos.nombre as nombre_grupo',
DB::raw("CONCAT(habitaciones.nombre, ' - ', pisos.nombre) as nombre_habitacion"),
'registros.*',
'clientes.nombre as nombre_cliente',
DB::raw("IF(IFNULL(($sql_1),0) > 0,'$s_deuda',' ') as robservacion,IFNULL(($sql_1),0) as tdeudas"),
'users.name as nombre_usuario'
)
->orderBy('id', 'desc')
->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
->leftJoin('clientes', 'clientes.id', '=', 'registros.cliente_id')
->leftJoin('grupos', 'grupos.id', '=', 'registros.grupo_id')
->leftJoin('users', 'users.id', '=', 'registros.user_id')
->where('pisos.hotel_id', $idHotel);
 */

//        $btn_detalle = '<a href="javascript:" onclick="cargarmodal()" class="btn btn-primary btn-xs" title="Detalle Flujo"><i class="fa fa-list"></i></a>';

        $btn_factura = '<a href="javascript:" onclick="onfacturar(' . "',flujos.id,'" . ')" class="btn btn-info btn-xs" title="Hacer Factura"><i class="fa fa-file"></i></a>';
        $btn_ver_factura = '<a href="javascript:" onclick="onverfactura(' . "',flujos.factura_id,'" . ')" class="btn btn-warning btn-xs" title="Ver Factura"><i class="fa fa-eye"></i></a>';
        $btn_recibo_1 = '<a href="javascript:" onclick="onreciboxdia(' . "',flujos.id,'" . ')" class="btn btn-success btn-xs" title="IMPRIMIR ENTREGA POR DIAS"><i class="fa fa-print"></i></a>';
        $btn_recibo_2 = '<a href="javascript:" onclick="onrecibo(' . "',flujos.id,'" . ')" class="btn btn-warning btn-xs" title="IMPRIMIR ENTREGA A DETALLE"><i class="fa fa-print"></i></a>';
        $btn_recibo = $btn_recibo_1 . " " . $btn_recibo_2;
        $btn_eliminar = '<a href="javascript:" onclick="oneliminar(' . "',flujos.id,'" . ')" class="btn btn-danger btn-xs" title="Eliminar Flujo"><i class="fa fa-remove"></i></a>';

        if ($rol == 'Operario') {
            $acc_role = <<<EOE
IF(flujos.user_id = $idUsuario,CONCAT(IF(flujos.detalle = 'TRASPASO DE CAJA CHICA' AND flujos.ingreso > 0,CONCAT(' $btn_recibo '),''),' $btn_eliminar'),'')
EOE;
        } else {
            $acc_role = <<<EOE
CONCAT(IF(flujos.detalle = 'ENTREGA DE CAJA CHICA' OR (flujos.detalle = 'TRASPASO DE CAJA CHICA' AND flujos.ingreso > 0),CONCAT(' $btn_recibo '),''),' $btn_eliminar')
EOE;
        }

        $acciones = <<<EOE
'<a href="javascript:" onclick="ondetalle(',flujos.id,')" class="btn btn-primary btn-xs" title="Detalle Flujo"><i class="fa fa-list"></i></a>',
IF(flujos.ingreso <> 0,IF(ISNULL(flujos.factura_id),CONCAT(' $btn_factura '),CONCAT('$btn_factura $btn_ver_factura')),''),$acc_role

EOE;

        $flujos = Flujo::where('caja_id', $idCaja)
            ->select('flujos.id', 'flujos.created_at', 'users.name as usuario', 'flujos.detalle', 'flujos.ingreso', 'flujos.salida'
                , DB::raw("CONCAT($acciones) as acciones"))
            ->leftJoin('users', 'users.id', '=', 'flujos.user_id')
            ->orderBy('id', 'desc')
            ->get();

        $datatables = Datatables::of($flujos);
        /*if($keyword = $request->all()['columns'][1]['search']['value']){
        $datatables->filterColumn('nombre_habitacion', 'whereRaw', "CONCAT(habitaciones.nombre, ' - ', pisos.nombre) like ?", ["%$keyword%"]);
        }
        if($keyword = $request->all()['columns'][5]['search']['value']){
        $datatables->filterColumn('robservacion', 'whereRaw', "IF(IFNULL(($sql_1),0) > 0,'$s_deuda',' ') like ?", ["%$keyword%"]);
        }
        if($keyword = $request->all()['columns'][7]['search']['value']){
        $datatables->filterColumn('tdeudas', 'whereRaw', "IFNULL(($sql_1),0) like ?", ["%$keyword%"]);
        }
        if ($keyword = $request->all()['search']['value']) {

        $datatables->filterColumn('nombre_habitacion', 'whereRaw', "CONCAT(habitaciones.nombre, ' - ', pisos.nombre) like ?", ["%$keyword%"]);
        $datatables->filterColumn('robservacion', 'whereRaw', "IF(IFNULL(($sql_1),0) > 0,'$s_deuda',' ') like ?", ["%$keyword%"]);
        $datatables->filterColumn('tdeudas', 'whereRaw', "IFNULL(($sql_1),0) like ?", ["%$keyword%"]);
        }*/
        return $datatables->make(true);

    }
}
