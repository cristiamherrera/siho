<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTutorialesRequest;
use App\Http\Requests\UpdateTutorialesRequest;

use App\Repositories\TutorialesRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Tutoriales;
use App\Adjunto;


class TutorialesController extends InfyOmBaseController
{
    /** @var  ActividadRepository */
     /**
     * Display a listing of the Actividad.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {
        //$this->actividadRepository->pushCriteria(new RequestCriteria($request));
        //$actividads = $this->actividadRepository->all();
        // $idHotel = Auth::user()->hotel_id;
        // $actividads = Actividad::where('hotel_id',$idHotel)->orderBy('id','desc')->get();
        // //dd($actividads);
        // return view('actividads.index')
        //     ->with('actividads', $actividads);

        return view('tutoriales.index');
    }
    public function operariotutoriales()
    {
        //$this->actividadRepository->pushCriteria(new RequestCriteria($request));
        //$actividads = $this->actividadRepository->all();
        // $idHotel = Auth::user()->hotel_id;
        // $actividads = Actividad::where('hotel_id',$idHotel)->orderBy('id','desc')->get();
        // //dd($actividads);
        // return view('actividads.index')
        //     ->with('actividads', $actividads);

        return view('tutoriales.operariotutoriales');
    }
     /**
     * Show the form for creating a new Clientes.
     *
     * @return Response
     */
}