<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use Illuminate\Http\Request;
use Flash;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Auth;
use App\Models\Flujo;
use App\Movimiento;
use Illuminate\Support\Facades\DB;
use App\Pago;

class UserController extends Controller
{

    public function index()
    {

        $usuarios = User::all();
        return view('users.index')->with(compact('usuarios'));
    }

    public function usuario($idUsuario = null)
    {

        if (isset($idUsuario)) {
            $usuario = User::find($idUsuario);
        }
        $hoteles = Hotel::get()->pluck('nombre', 'id')->all();
        //dd($hoteles);
        return view('users.usuario')->with(compact('usuario', 'hoteles'));
    }


    public function guarda_usuario(Request $request, $idUsuario = null)
    {
//dd($request);exit;
        if (isset($idUsuario)) {
            $v_usuario = User::where('email', $request->email)->where('id', '<>', $idUsuario)->get()->first();
            if (isset($v_usuario->email)) {
                Flash::error('El nombre de usuario ya existe!!');
                return redirect()->back();
            }
            $usuario = User::find($idUsuario);
            $usuario->name = $request->name;
            $usuario->rol = $request->rol;
            $usuario->email = $request->email;
            $usuario->hotel_id = $request->hotel_id;
            if (isset($request->password2) && !empty($request->password2)) {
                $usuario->password = bcrypt($request->password2);
            }
            $usuario->save();
        } else {
            $v_usuario = User::where('email', $request->email)->get()->first();
            if (isset($v_usuario->email)) {
                Flash::error('El nombre de usuario ya existe!!');
                return redirect()->back();
            }
            $usuario = new User;
            $usuario->name = $request->name;
            $usuario->rol = $request->rol;
            $usuario->email = $request->email;
            $usuario->hotel_id = $request->hotel_id;
            if (isset($request->password2) && !empty($request->password2)) {
                $usuario->password = bcrypt($request->password2);
            }
            $usuario->save();
        }
        Flash::success('El registro del usuario se ha realizado correctamente!!');
        return redirect()->back();
        //return redirect(route('usuarios'));
    }

    public function eliminar($idUsuario)
    {
        $usuario = User::find($idUsuario);
        $usuario->delete();
        Flash::success('Se ha eliminado correctamente el usuario');
        return redirect(route('usuarios'));
    }

    public function direcciona()
    {
        //dd(Auth::user());
        $usuario = Auth::user();
        ///dd(isset($usuario));
        if (isset($usuario)) {
            if ($usuario->rol == 'Administrador') {
                return redirect(route('panelcontrol'));
            } elseif ($usuario->rol == 'Operario') {
                return redirect(route('vhabitaciones'));
            } elseif ($usuario->rol == 'Super Administrador') {
                if ($usuario->hotel_id == 0) {
                    return redirect(url('/usuarios'));
                } else {
                    return redirect(route('registros'));
                }
            } else {
                return view('auth.login');
            }
        } else {
            return view('auth.login');
        }
    }

    public function cambiarhotel($idHotel)
    {
        $usuario = User::find(Auth::user()->id);
        $usuario->hotel_id = $idHotel;
        $usuario->save();
        return redirect(route('direcciona'));
    }

    public function ayuda()
    {
        return view('users.ayuda');
    }

    public function panelcontrol(Request $request)
    {

//        dd($request);
//
        $idHotel = Auth::user()->hotel_id;

        //---------- PARA LOS 7 DIAS -------------------------------
        $fecha_fin_ult7 = date("d/m/Y");
        if (!empty($request->fecha_ult7)) {
            $fecha_fin_ult7 = $request->fecha_ult7;
        }
        $ult7_datos = Flujo::select(DB::raw('DATE(created_at) as fecha, SUM(ingreso) as t_ingreso, SUM(salida) as t_salida'))
            ->whereHas('caja', function ($query) use ($idHotel) {
                if (!empty($idHotel)) {
                    $query->where('hotel_id', '=', $idHotel);
                }
                return $query;
            })
            ->whereNotIn('flujos.detalle', ['ENTREGA DE CAJA CHICA','TRASPASO DE CAJA CHICA'])
            ->where(function ($query) use ($fecha_fin_ult7) {
                $f_fin = $this->conv_fecha($fecha_fin_ult7);
                $f_ini = date('Y-m-d', strtotime($f_fin . ' - 7 days'));
                $query->where(DB::raw('DATE(created_at)'), '>=', $f_ini);
                $query->where(DB::raw('DATE(created_at)'), '<=', $f_fin);
                $query->where('deleted_at', null);
                return $query;
            })
            ->groupBy('fecha')
            ->orderBy('fecha', 'asc')
            ->get();

        $array_ult7 = $ult7_datos->toArray();
        $fechas_7dias = array();
        $f_fin = $this->conv_fecha($fecha_fin_ult7);
        $cont = 0;
        for ($i=6; $i>=0; $i--) {
            $fecha = date('Y-m-d', strtotime($f_fin . " - $i days"));
            $fecha_nu = date('D d', strtotime($f_fin . " - $i days"));
            $fechas_7dias[$cont] = array();
            $fechas_7dias[$cont]['fecha'] = $fecha_nu;
            $found_key = array_search($fecha, array_column($array_ult7, 'fecha'));
            if ($found_key != false) {
                $fechas_7dias[$cont]['t_ingreso'] = $array_ult7[$found_key]['t_ingreso'];
                $fechas_7dias[$cont]['t_salida'] = $array_ult7[$found_key]['t_salida'];
            } else {
                $fechas_7dias[$cont]['t_ingreso'] = 0;
                $fechas_7dias[$cont]['t_salida'] = 0;
            }
            $cont++;
        }
        //----------------------------------------------------------------

        //---------- PARA LOS MESES  -------------------------------
        $fecha_ini_mes = date('d/m/Y', strtotime(date('Y-m-d') . ' - 3 month'));
        $fecha_fin_mes = date("d/m/Y");
        if (!empty($request->fecha_ini_mes)) {
            $fecha_ini_mes = $request->fecha_ini_mes;
        }
        if (!empty($request->fecha_fin_mes)) {
            $fecha_fin_mes = $request->fecha_fin_mes;
        }

        $mes_datos = Flujo::select(DB::raw('DATE(created_at) as fecha, YEAR(created_at) as gestion, MONTH(created_at) as mes,DAY(created_at) as dia, SUM(ingreso) as t_ingreso, SUM(salida) as t_salida'))
            ->whereHas('caja', function ($query) use ($idHotel) {
                if (!empty($idHotel)) {
                    $query->where('hotel_id', '=', $idHotel);
                }
                return $query;
            })
            ->whereNotIn('flujos.detalle', ['ENTREGA DE CAJA CHICA','TRASPASO DE CAJA CHICA'])
            ->where(function ($query) use ($fecha_ini_mes, $fecha_fin_mes) {
                $f_ini = $this->conv_fecha($fecha_ini_mes);
                $f_fin = $this->conv_fecha($fecha_fin_mes);
                $query->where(DB::raw('DATE(created_at)'), '>=', $f_ini);
                $query->where(DB::raw('DATE(created_at)'), '<=', $f_fin);
                $query->where('deleted_at', null);
                return $query;
            })
            ->groupBy('fecha', 'gestion', 'mes', 'dia')
            ->orderBy('fecha', 'asc')
            ->get();

        // dd($mes_datos->toArray());
        //----------------------------------------------------------------

        //---------- PARA LOS PISOS  -------------------------------

        $pisos_datos = Pago::select('pisos.nombre', DB::raw('SUM(flujos.ingreso) as t_ingreso'))
            ->join('registros', 'registros.id', '=', 'pagos.registro_id')
            ->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
            ->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
            ->join('flujos', 'flujos.id', '=', 'pagos.flujo_id')
            ->where('pisos.hotel_id', $idHotel)
            ->where(DB::raw('MONTH(flujos.created_at)'), date('n'))
            ->where(DB::raw('YEAR(flujos.created_at)'), date('Y'))
            ->groupBy('pisos.id')
            ->orderBy('pisos.id', 'asc')
            ->get();
//        dd($pisos_datos->toArray());
        //----------------------------------------------------------------

        //---------- POR OPERARIO -------------------------------

        $usuarios_datos = Pago::select('users.name', DB::raw('SUM(flujos.ingreso) as t_ingreso'))
            ->join('registros', 'registros.id', '=', 'pagos.registro_id')
            ->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
            ->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
            ->join('flujos', 'flujos.id', '=', 'pagos.flujo_id')
            ->join('users', 'users.id', '=', 'flujos.user_id')
            ->where('pisos.hotel_id', $idHotel)
            ->where(DB::raw('MONTH(flujos.created_at)'), date('n'))
            ->where(DB::raw('YEAR(flujos.created_at)'), date('Y'))
            ->groupBy('users.id')
            ->orderBy('users.name', 'asc')
            ->get();
        // dd($pisos_datos->toArray());
        //----------------------------------------------------------------

        // PARA EL INVENTARIO
        $fecha_ini_mes = date('d/m/Y', strtotime(date('Y-m-d') . ' - 3 month'));
        $fecha_fin_mes = date("d/m/Y");
        $f_ini = $this->conv_fecha($fecha_ini_mes);
        $f_fin = $this->conv_fecha($fecha_fin_mes);
        $datos_insumos = Movimiento::select('insumos.nombre', DB::raw('SUM(movimientos.cantidad) as total'))
            ->join('insumos', 'insumos.id', '=', 'movimientos.insumo_id')
            ->groupBy('movimientos.insumo_id')
            ->where('movimientos.hotel_id', $idHotel)
            ->where(DB::raw('DATE(movimientos.created_at)'), '>=', $f_ini)
            ->where(DB::raw('DATE(movimientos.created_at)'), '<=', $f_fin)
            ->where('movimientos.estado', "Salida")
            ->orderBy('total', 'desc')
            ->limit(10)
            ->get();
        //dd($datos_insumos->toSql());
        // dd($datos_insumos->toArray());
        // FIN PARA INVENTARIO


        return view('users.panelcontrol')->with(compact('fecha_fin_ult7', 'fechas_7dias', 'mes_datos', 'fecha_fin_mes', 'fecha_ini_mes', 'pisos_datos', 'usuarios_datos', 'datos_insumos'));
    }

    public function conv_fecha($fecha)
    {
        $arrayf = explode("/", $fecha);
        return $arrayf[2] . "-" . $arrayf[1] . "-" . $arrayf[0];
    }
}
