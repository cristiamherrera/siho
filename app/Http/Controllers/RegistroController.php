<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\Hospedante;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Requests\CreateRegistroRequest;
use App\Http\Requests\UpdateRegistroRequest;
use App\Models\Caja;
use App\Models\Clientes;
use App\Models\Flujo;
use App\Models\Habitaciones;
use App\Models\Precioshabitaciones;
use App\Models\Registro;
use App\Opcione;
use App\Pago;
use App\Repositories\RegistroRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;
use Yajra\Datatables\Datatables;

class RegistroController extends InfyOmBaseController
{
    /** @var  RegistroRepository */
    private $registroRepository;

    public function __construct(RegistroRepository $registroRepo)
    {
        $this->registroRepository = $registroRepo;
    }

    /**
     * Display a listing of the Registro.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {

        //dd($idHotel);
        //$this->registroRepository->pushCriteria(new RequestCriteria($request));
        //$registros = $this->registroRepository->findWhere(['habitacione_id','=',1])->all();

//        $registros = Registro::all()->where('habitacione.rpiso.hotel_id', $idHotel);
        //        $registros = Registro::orderBy('id', 'desc')->get()->where('habitacione.rpiso.hotel_id', $idHotel);
        //dd($registros);

        $this->generadeudatotales();

        return view('registros.index');
    }

    public function generadeudatotales()
    {
        $idHotel = Auth::user()->hotel_id;
        $fecha_actual = date('Y-m-d');
        $opcione = Opcione::where('tipo', 'Hora limite de salida')->get()->first();
        if (intval(date('H')) < intval($opcione->descripcion)) {
            $fecha_actual = date('Y-m-d', strtotime($fecha_actual . ' -1 day'));
        }
        $registros = Registro::select('registros.*', DB::raw('DATE(registros.fecha_ingreso) as fecha_ingreso33'))
            ->where('registros.estado', 'Ocupando')
            ->leftJoin('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
            ->leftJoin('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
            ->where('pisos.hotel_id', $idHotel)->get();
//        dd($registros->toArray());
        foreach ($registros as $registro) {
//            dd($registro->fecha_ingreso3);
            $fechas = $this->createDateRangeArray($registro->fecha_ingreso33, $fecha_actual);
//            dd($fechas);
            foreach ($fechas as $fecha) {
                $si_pago = Pago::where('registro_id', $registro->id)->where('fecha', $fecha)->first();
                if (!isset($si_pago)) {
                    $pago = new Pago;
                    $pago->registro_id = $registro->id;
                    $pago->precio = $registro->precio;
                    $pago->monto_total = $registro->precio;
                    $pago->fecha = $fecha;
                    $pago->estado = 'Deuda';
                    $pago->save();

                }
            }
        }

        //Flash::success('Se ha actualizado correctamente los pagos pendientes!!');
        return true;
    }

    public function createDateRangeArray($strDateFrom, $strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    /**
     * Show the form for creating a new Registro.
     *
     * @return Response
     */
    public function create()
    {
        return view('registros.create');
    }

    /**
     * Store a newly created Registro in storage.
     *
     * @param CreateRegistroRequest $request
     *
     * @return Response
     */
    public function store(CreateRegistroRequest $request)
    {
        $input = $request->all();

        $registro = $this->registroRepository->create($input);

        Flash::success('Registro saved successfully.');

        return redirect(route('registros.index'));
    }

    /**
     * Display the specified Registro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $registro = $this->registroRepository->findWithoutFail($id);

        if (empty($registro)) {
            Flash::error('Registro not found');

            return redirect(route('registros.index'));
        }

        return view('registros.show')->with('registro', $registro);
    }

    /**
     * Show the form for editing the specified Registro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $registro = $this->registroRepository->findWithoutFail($id);

        if (empty($registro)) {
            Flash::error('Registro not found');

            return redirect(route('registros.index'));
        }

        return view('registros.edit')->with('registro', $registro);
    }

    /**
     * Update the specified Registro in storage.
     *
     * @param  int $id
     * @param UpdateRegistroRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegistroRequest $request)
    {
        $registro = $this->registroRepository->findWithoutFail($id);

        if (empty($registro)) {
            Flash::error('Registro not found');

            return redirect(route('registros.index'));
        }

        $registro = $this->registroRepository->update($request->all(), $id);

        Flash::success('Registro updated successfully.');

        return redirect(route('registros.index'));
    }

    /**
     * Remove the specified Registro from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $registro = $this->registroRepository->findWithoutFail($id);
        //----------- Elimina el flujo de pago ---------------------
        if (!empty($registro->flujo_id)) {

            $flujo = Flujo::find($registro->flujo_id);
            $total = $this->get_total($flujo->caja_id);

            if ($flujo->ingreso != 0) {
                if ($total >= $flujo->ingreso) {
                    $this->set_total($flujo->caja_id, ($total - $flujo->ingreso));
                } else {
                    //dd($request->all());
                    Flash::error('No se ha podido eliminar el pago por que el total es solo ' . $total);
                    return redirect()->back();
                }
            } else {
                $this->set_total($flujo->caja_id, ($total + $flujo->salida));
            }
            $flujo->deleted_at = date('Y-m-d H:m:i');
            $flujo->observacion = 'Eliminado desde Registro';
            $flujo->save();

            $datos_reg['flujo_id'] = null;
        }
        //--------------------------------------------------------

        if (empty($registro)) {
            Flash::error('El registro no fue encontrado');
            return redirect(route('registros.index'));
        }

        if (!empty($registro->num_reg)) {
            //$registros = Registro::
            //$registros = Registro::
            $registros = Registro::where('num_reg', $registro->num_reg)->get()->all();
            foreach ($registros as $registro) {
                $this->registroRepository->delete($registro->id);
            }
        } else {
            $this->registroRepository->delete($registro->id);
        }

        Flash::success('Registro deleted successfully.');
        return redirect()->back();
        //return redirect(route('registros.index'));
    }

    public function nuevo($tipo, $idCliente = null, $idHabitacion = null, $idRegistro = null)
    {

        //dd($idRegistro);
        //return view('registros.create');
        $idHotel = Auth::user()->hotel_id;
        $hospedantes = array();
        if (isset($idRegistro)) {
            //$registro = $this->registroRepository->findWithoutFail($idRegistro);
            $registro = Registro::where('id', $idRegistro)->whereHas('habitacione', function ($query) use ($idHotel) {
                $query->whereHas('rpiso', function ($query) use ($idHotel) {
                    $query->where('hotel_id', $idHotel);
                });
            })->first();
            if (empty($registro)) {
                Flash::error('Usted no esta autorizado. Se podria informar al administrador!!');
                return redirect()->back();
            }
            //dd($registro);
            $hospedantes = Hospedante::WhereIn('estado', ['Reservado', 'Ocupando'])->where('registro_id', $idRegistro)->get();
        }

        $registros = Habitaciones::find($idHabitacion)->registrosactivos;
        $precios = Precioshabitaciones::where('habitacione_id', $idHabitacion)->get()->pluck('precio', 'precio')->all();
        //dd($precios);
        $cliente = array();
        $grupo = array();
        $sel_grupo_i = null;
        if ($tipo == 'Cliente' && empty($idRegistro)) {
            $cliente = Clientes::find($idCliente);
        } elseif ($tipo == 'Grupo') {
            $grupo = Grupo::find($idCliente);

            $sel_grupo_i = $grupo->id;
        }
        //$habitacion = Habitaciones::find($idHabitacion);
        $habitacion = Habitaciones::where('id', $idHabitacion)->whereHas('rpiso', function ($query) use ($idHotel) {
            $query->where('hotel_id', $idHotel);
        })->first();

        $grupos = DB::table('registros')
            ->whereIn('registros.estado', ['Ocupando', 'Reservado'])
            ->join('grupos', 'grupos.id', '=', 'registros.grupo_id')
            ->where('hotel_id', $idHotel)
            ->orderBy('grupos.nombre')
            ->pluck('grupos.nombre', 'grupos.id');

        $equipaje = DB::table('registros')
            ->groupBy('equipaje')
            ->pluck('equipaje', 'equipaje');

        $ocupado = Registro::where('habitacione_id', $idHabitacion)->where('estado', 'Ocupando')->first();

        return view('registros.nuevo')->with(compact('precios', 'habitacion', 'cliente', 'registro', 'registros', 'grupos', 'hospedantes', 'ocupado', 'grupo', 'sel_grupo_i', 'equipaje'));
    }

    public function quitarhuesped($idHuesped)
    {
        Hospedante::destroy($idHuesped);
        Flash::success('Se ha eliminado al huesped del registro correctamente!!');
        return redirect()->back();
        //return redirect(route('registros.index'));
    }

    public function msalidahuesped($idHuesped)
    {
        $hospedante = Hospedante::find($idHuesped);
        $hospedante->estado = 'Salida';
        $hospedante->fecha_salida = date('Y-m-d H:i:s');
        $hospedante->save();
        Flash::success('Se ha marcado la salida del huesped correctamente!!');
        return redirect()->back();
        //return redirect(route('registros.index'));
    }

    public function guarda_registro(Request $request, $idRegistro = null)
    {

//        dd($request->all());
        $datos_reg = $request->all();

        /*if (isset($datos_reg['fecha_ingreso']) && !empty($datos_reg['fecha_ingreso'])) {
        $datos_reg['fecha_ingreso'] = Carbon::createFromFormat('d/m/Y', $datos_reg['fecha_ingreso'])->toDateTimeString();
        }
        if (isset($datos_reg['fecha_salida']) && !empty($datos_reg['fecha_salida'])) {
        $datos_reg['fecha_salida'] = Carbon::createFromFormat('d/m/Y', $datos_reg['fecha_salida'])->toDateTimeString();
         */

        //-------------- Registrar al grupo-------------------
        if (isset($request->nuevogrupo) && !empty($request->nuevogrupo)) {
            $grupo = new Grupo;
            if (!empty($request->cliente_id)) {
                $grupo->responsable_id = $request->cliente_id;
            }
            $grupo->nombre = $request->nuevogrupo;
            $grupo->hotel_id = Auth::user()->hotel_id;
            $grupo->save();

            $idGrupo = $grupo->id;
            $datos_reg['grupo_id'] = $idGrupo;
        }
        //---------------------------------------------------

        //----------- Guarda el registro -----------------------
        if (isset($request->estado) && $request->estado == 'Ocupando') {
            $datos_reg['fecha_ingreso'] = date('Y-m-d H:i:s');

            $fecha_pago = date('Y-m-d');
            $opcione = Opcione::where('tipo', 'Hora cambio de fecha')->get()->first();
            if (date('H:i') >= '00:00' && intval(date('H')) < intval($opcione->descripcion)) {
                $fecha_pago = date('Y-m-d', strtotime($fecha_pago . ' -1 day'));
            }
        } else {
            $datos_reg['fecha_ingreso'] = null;
        }
        if (empty($datos_reg['fech_ini_reserva'])) {
            unset($datos_reg['fech_ini_reserva']);
        }
        if (empty($datos_reg['fech_fin_reserva'])) {
            unset($datos_reg['fech_fin_reserva']);
        }

        if (isset($idRegistro)) {
            $registro = $this->registroRepository->findWithoutFail($idRegistro);
            $registro = $this->registroRepository->update($datos_reg, $idRegistro);
        } else {
            $registro = $this->registroRepository->create($datos_reg);
            $idRegistro = $registro->id;
        }
        //---------------------------------------------------------
        //------------- Registra el primer pago y huespedes registrados los ocupa -------------------
        $fecha_hu_in = null;
        if (isset($request->estado) && $request->estado == 'Ocupando') {

            $enc_pago_deuda = Pago::where('registro_id', $idRegistro)
                ->where('estado', 'Deuda')
                ->where('fecha', $fecha_pago)->orderBy('created_at', 'desc')->get()->first();
            $enc_pago_pagado = Pago::where('registro_id', $idRegistro)
                ->where('estado', 'Pagado')
                ->where('fecha', $fecha_pago)->orderBy('created_at', 'desc')->get()->first();
            if (empty($enc_pago_deuda->fecha) && empty($enc_pago_pagado->fecha)) {
                $pago = new Pago;
                $pago->registro_id = $idRegistro;
                $pago->precio = $request->precio;
                $pago->monto_total = $request->precio;
                $pago->fecha = $fecha_pago;
                $pago->estado = 'Deuda';
                $pago->save();
                $fecha_hu_in = $datos_reg['fecha_ingreso'];
            } else {
                if (!empty($enc_pago_deuda['Pago']['estado'])) {
                    if (!empty($request->cambpreciact)) {
                        $enc_pago_deuda->precio = $request->precio;
                        $enc_pago_deuda->monto_total = $request->precio;
                        $enc_pago_deuda->save();
                    }
                } else {
                    if (!empty($enc_pago_pagado->monto_total) && $request->precio > $enc_pago_pagado->monto_total && empty($enc_pago_deuda->fecha)) {
                        $precio_so = $request->precio - $enc_pago_pagado->monto_total;
                        $pago = new Pago;
                        $pago->registro_id = $idRegistro;
                        $pago->precio = $precio_so;
                        $pago->monto_total = $precio_so;
                        $pago->fecha = $fecha_pago;
                        $pago->estado = 'Deuda';
                        $pago->save();
                    }
                }

            }
            //----------------HUESPEDES --------------------------------//
            $a_hospedantes = Hospedante::where('registro_id', $idRegistro)->get();
            foreach ($a_hospedantes as $a_hospedante) {
                $a_hospedante->estado = 'Ocupando';
                $a_hospedante->fecha_ingreso = $datos_reg['fecha_ingreso'];
                $a_hospedante->save();
            }
            //-----------------------------------------------------//
        }
        //---------------------------------------------------
        // ------------ Registra Huespedes ----------------
        //dd($request->estado);
        if (isset($request->huespedes) && isset($request->estado)) {
            foreach ($request->huespedes as $huesped) {
                $hospedante = new Hospedante;
                $hospedante->cliente_id = $huesped['cliente_id'];
                $hospedante->registro_id = $idRegistro;
                $hospedante->estado = $request->estado;
                $hospedante->fecha_ingreso = $datos_reg['fecha_ingreso'];
                $hospedante->save();
            }
        }
        //--------------------------------------------
        Flash::success('El registro de habitacion se ha realizado correctamente!!');
        //return redirect()->back();
        return redirect(route('registrosgrupos', [$datos_reg['grupo_id']]));
    }

    public function guarda_transferencia(Request $request)
    {

        if (empty($request->precio) || empty($request->habitacione_id)) {
            Flash::error('Ocurrio algun error no se pudo hacer la transferencia!!');
            return redirect()->back();
        }
        $idRegistro_ant = $request->registro_id;
        $registro = Registro::find($idRegistro_ant);

        //-------------- CREA EL NUEVO REGISTRO ------------------
        $datos_reg['habitacione_id'] = $request->habitacione_id;
        $datos_reg['estado'] = 'Ocupando';
        $datos_reg['fecha_ingreso'] = date('Y-m-d H:i:s');
        $datos_reg['precio'] = $request->precio;
        $datos_reg['user_id'] = Auth::user()->id;
        $datos_reg['grupo_id'] = $registro->grupo_id;
        $datos_reg['equipaje'] = $registro->equipaje;
        $registro_pp = $this->registroRepository->create($datos_reg);
        $idRegistro = $registro_pp->id;
        //------------- TERMINA EL NUEVO REGISTRO ---------------
        //------------- REGISTRA PAGO --------------------------
        $fecha_pago = date('Y-m-d');
        $enc_pago_deuda = Pago::where('registro_id', $idRegistro_ant)
            ->where('estado', 'Deuda')
            ->where('fecha', $fecha_pago)->orderBy('created_at', 'desc')->get()->first();
        $enc_pago_pagado = Pago::where('registro_id', $idRegistro_ant)
            ->where('estado', 'Pagado')
            ->where('fecha', $fecha_pago)->orderBy('created_at', 'desc')->get()->first();
        if (empty($enc_pago_deuda->fecha) && empty($enc_pago_pagado->fecha)) {
            $pago = new Pago;
            $pago->registro_id = $idRegistro;
            $pago->precio = $request->precio;
            $pago->monto_total = $request->precio;
            $pago->fecha = $fecha_pago;
            $pago->estado = 'Deuda';
            $pago->save();
            $fecha_hu_in = $datos_reg['fecha_ingreso'];
        } else {
            if (!empty($enc_pago_deuda['Pago']['estado']) && empty($enc_pago_pagado['Pago']['estado'])) {
                $enc_pago_deuda->registro_id = $idRegistro;
                $enc_pago_deuda->precio = $request->precio;
                $enc_pago_deuda->monto_total = $request->precio;
                $enc_pago_deuda->save();
            } else {
                if (!empty($enc_pago_pagado->monto_total) && $request->precio > $enc_pago_pagado->monto_total && empty($enc_pago_deuda->fecha)) {
                    $precio_so = $request->precio - $enc_pago_pagado->monto_total;
                    $pago = new Pago;
                    $pago->registro_id = $idRegistro;
                    $pago->precio = $precio_so;
                    $pago->monto_total = $precio_so;
                    $pago->fecha = $fecha_pago;
                    $pago->estado = 'Deuda';
                    $pago->save();
                    $enc_pago_pagado->registro_id = $idRegistro;
                    $enc_pago_pagado->save();
                } elseif (!empty($enc_pago_pagado->monto_total) && $request->precio > $enc_pago_pagado->monto_total && !empty($enc_pago_deuda->fecha)) {
                    $precio_so = $request->precio - $enc_pago_pagado->monto_total;

                    $enc_pago_deuda->registro_id = $idRegistro;
                    $enc_pago_deuda->precio = $precio_so;
                    $enc_pago_deuda->monto_total = $precio_so;
                    $enc_pago_deuda->save();

                    $enc_pago_pagado->registro_id = $idRegistro;
                    $enc_pago_pagado->save();
                } elseif (!empty($enc_pago_pagado->monto_total) && empty($enc_pago_deuda->fecha)) {
                    $enc_pago_pagado->registro_id = $idRegistro;
                    $enc_pago_pagado->save();
                }elseif (!empty($enc_pago_pagado->monto_total) && !empty($enc_pago_deuda->fecha)) {
                    $enc_pago_deuda->registro_id = $idRegistro;
                    $enc_pago_deuda->save();
                    $enc_pago_pagado->registro_id = $idRegistro;
                    $enc_pago_pagado->save();
                }
            }

        }
        //-------------------------------------------------------

        //------------- REGISTRA HOSPEDANTES --------------------
        foreach ($registro->hospedantes as $hospedante) {
            $rhospedante = new Hospedante();
            $rhospedante->cliente_id = $hospedante->cliente_id;
            $rhospedante->registro_id = $idRegistro;
            $rhospedante->estado = $hospedante->estado;
            $rhospedante->fecha_ingreso = date('Y-m-d H:i:s');
            $rhospedante->save();
        }
        //------------- TERMINA EL REGISTRO DE HOSPEDANTES -----------

        //------------- MARCA LA SALIDA DE LOS OTROS ---------------
        $registro->estado = 'Salida';
        $registro->fecha_salida = date('Y-m-d H:i:s');
        $registro->save();
        foreach ($registro->hospedantes as $hospedante) {
            $hospedante->estado = 'Salida';
            $hospedante->fecha_salida = date('Y-m-d H:i:s');
            $hospedante->save();
        }
        //-----------------------------------------------------------
        Flash::success('Se ha realizado la transferencia con exito!!');

        return redirect()->back();
    }

    public function guarda_registros(Request $request, $num_reg = null)
    {

        $direccionar = route('registros.index');
        $habitaciones = $request->habitaciones;
        $datos_reg = $request->all();
        unset($datos_reg['habitaciones']);

        if (isset($datos_reg['fecha_ingreso']) && !empty($datos_reg['fecha_ingreso'])) {
            $datos_reg['fecha_ingreso'] = Carbon::createFromFormat('d/m/Y', $datos_reg['fecha_ingreso'])->toDateTimeString();
        }
        if (isset($datos_reg['fecha_salida']) && !empty($datos_reg['fecha_salida'])) {
            $datos_reg['fecha_salida'] = Carbon::createFromFormat('d/m/Y', $datos_reg['fecha_salida'])->toDateTimeString();
        }
        //----------- Elimina el flujo de pago ---------------------
        if (isset($request->repago) && !empty($datos_reg['flujo_id'])) {

            $flujo = Flujo::find($datos_reg['flujo_id']);
            $total = $this->get_total($flujo->caja_id);

            if ($flujo->ingreso != 0) {
                if ($total >= $flujo->ingreso) {
                    $this->set_total($flujo->caja_id, ($total - $flujo->ingreso));
                } else {
                    //dd($request->all());
                    Flash::error('No se ha guardado porque no se ha podido eliminar el pago por que el total es solo ' . $total);
                    return redirect()->back();
                }
            } else {
                $this->set_total($flujo->caja_id, ($total + $flujo->salida));
            }
            $flujo->deleted_at = date('Y-m-d H:m:i');
            $flujo->observacion = 'Eliminado desde Registro';
            $flujo->save();

            $datos_reg['flujo_id'] = null;
        }
        //--------------------------------------------------------
        //----------- Crea el flujo de pago para el registro ---------
        if (isset($request->pagar) && empty($datos_reg['flujo_id'])) {
            //$datos_reg['monto_total'];
            $flujo = new Flujo;
            $flujo->detalle = 'Pago de Registro';
            $flujo->ingreso = $datos_reg['monto_total'];
            $flujo->observacion = '';
            $flujo->salida = 0;
            $flujo->caja_id = $request->caja_id;
            $flujo->user_id = $request->user_id;
            $flujo->save();
            $total = $this->get_total($request->caja_id);
            $this->set_total($request->caja_id, ($total + $datos_reg['monto_total']));
            $datos_reg['flujo_id'] = $flujo->id;
            $direccionar = route('flujos', [$request->caja_id]);
        }
        //-------------------------------------------------------
        //----------- Guarda el registro -----------------------

        if (isset($request->ocupar)) {
            $datos_reg['estado'] = 'Ocupando';
        }

        if (isset($num_reg)) {
            foreach ($habitaciones as $idHabitacion => $habitacion) {
                //$datos_reg['habitacione_id'] = $idHabitacion;
                $datos_reg['precio'] = $habitacion['precio'];
                $datos_reg['monto_total'] = $habitacion['monto_total'];
                if (isset($habitacion['registro_id'])) {
                    $registro = $this->registroRepository->update($datos_reg, $habitacion['registro_id']);
                } else {
                    $datos_reg['num_reg'] = $num_reg;
                    $registro = $this->registroRepository->create($datos_reg);
                }
            }
        } else {

            $numero_reg = $this->get_num_reg();
            $datos_reg['num_reg'] = $numero_reg;
            foreach ($habitaciones as $idHabitacion => $habitacion) {
                $datos_reg['habitacione_id'] = $idHabitacion;
                $datos_reg['precio'] = $habitacion['precio'];
                $datos_reg['monto_total'] = $habitacion['monto_total'];
                $registro = $this->registroRepository->create($datos_reg);

            }
        }
        //---------------------------------------------------------
        //Desocupa la habitacion liberando del registro

        if (isset($request->ocupado) && isset($num_reg)) {

            $registros = Registro::all()->where('num_reg', $num_reg);
            foreach ($registros as $registro) {
                $registro->estado = 'Desocupado';
                $registro->save();
            }

            //$datos_reg['estado'] = 'Desocupado';
            //$registro = $this->registroRepository->update($datos_reg, $idRegistro);
        }
        //----------------------------------------------------------
        Flash::success('El registro de habitacion se ha realizado correctamente!!');
        //return redirect()->back();
        return redirect($direccionar);
    }

    public function nuevos(Request $request, $idCliente = null, $num_reg = null)
    {
        $habitaciones = $request->habitaciones;
        //dd($num_reg);

        $idHotel = Auth::user()->hotel_id;
        if (isset($num_reg)) {
            $registros = Registro::all()->where('num_reg', $num_reg);

            //dd($h_ocupado);
            $habitaciones2 = $habitaciones;
            $habitaciones = array();
            foreach ($registros as $registro) {
                $habitaciones[$registro->habitacione_id]['registro'] = $registro;
                $habitaciones[$registro->habitacione_id]['precios'] = Precioshabitaciones::where('habitacione_id', $registro->habitacione_id)->get()->pluck('precio', 'precio')->all();
                $habitaciones[$registro->habitacione_id]['habitacion'] = Habitaciones::find($registro->habitacione_id);
            }

            if (isset($habitaciones2)) {
                foreach ($habitaciones2 as $idHabitacion => $habi) {
                    if (!isset($habitaciones[$idHabitacion])) {
                        $habitaciones[$idHabitacion]['precios'] = Precioshabitaciones::where('habitacione_id', $idHabitacion)->get()->pluck('precio', 'precio')->all();
                        $habitaciones[$idHabitacion]['habitacion'] = Habitaciones::find($idHabitacion);
                    }
                }
            }

        } else {
            foreach ($habitaciones as $idHabitacion => $ha) {
                $habitaciones[$idHabitacion]['precios'] = Precioshabitaciones::where('habitacione_id', $idHabitacion)->get()->pluck('precio', 'precio')->all();
                $habitaciones[$idHabitacion]['habitacion'] = Habitaciones::find($idHabitacion);
            }
        }

        $cliente = Clientes::find($idCliente);
        $cajas = Caja::where('hotel_id', $idHotel)->get()->pluck('nombre', 'id')->all();
        return view('registros.nuevos')->with(compact('cliente', 'registro', 'cajas', 'habitaciones'));
    }

    public function get_total($idCaja)
    {
        $caja = Caja::find($idCaja);
        return $caja->total;
    }

    public function set_total($idCaja, $total = 0.00)
    {
        $caja = Caja::find($idCaja);
        $caja->total = $total;
        $caja->save();
        return true;
    }

    public function get_num_reg()
    {

        $registro = DB::table('registros')
            ->orderBy('num_reg', 'desc')
            ->first();
        if (isset($registro->num_reg)) {
            return $registro->num_reg + 1;
        } else {
            return 1;
        }
    }

    public function calendario()
    {
        $ano = date('Y');
        $mes = intval(date('m'));
        $idHotel = Auth::user()->hotel_id;
        $habitaciones = Habitaciones::all()->where('rpiso.hotel_id', $idHotel);
        $numero_dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
        return view('registros.calendario')->with(compact('habitaciones', 'idhotel', 'numero_dias'));
    }

    public function registros_cliente($idCliente = null)
    {
        //$idHotel = Auth::user()->hotel_id;guarda_registros
        $cliente = Clientes::find($idCliente);

        $hosp_registros = Hospedante::where('cliente_id', $idCliente)->orderBy('updated_at', 'desc')->get();
        //dd($hosp_registros);
        return view('registros.registros_cliente')->with(compact('cliente', 'hosp_registros'));
    }

    public function registrar_pago(Request $request)
    {
        //dd($request->cliente_id);
        $idCliente = $request->cliente_id;
        $monto_total = $request->monto_total;
        $registros = Registro::where('cliente_id', $idCliente)->where('flujo_id', 0)->get();

        $flujo = new Flujo;
        $flujo->detalle = 'Pago de Registro';
        $flujo->ingreso = $monto_total;
        $flujo->observacion = '';
        $flujo->salida = 0;
        $flujo->caja_id = $request->caja_id;
        $flujo->user_id = $request->user_id;
        $flujo->save();
        $total = $this->get_total($request->caja_id);
        $this->set_total($request->caja_id, ($total + $monto_total));
        $idFlujo = $flujo->id;

        foreach ($registros as $registro) {
            $registro->flujo_id = $idFlujo;
            $registro->save();
        }
        $direccionar = route('flujos', [$request->caja_id]);
        Flash::success('El pago de los pendientes se ha registrado correctamente!!');
        //return redirect()->back();
        return redirect($direccionar);
    }

    public function imp_registro($idRegistro)
    {
        $registro = Registro::find($idRegistro);
        $terminos = Opcione::where('tipo', 'Terminos')->get()->first();
        $logo = Opcione::where('tipo', 'Logo')->get()->first();
        return view('registros.imp_registro')->with(compact('registro', 'terminos', 'logo'));
    }

    public function datatableregistros(CreateRegistroRequest $request)
    {
        $idHotel = Auth::user()->hotel_id;

        $sql_1 = <<<EOE
        SELECT SUM(pagos.monto_total) as monto_total
        FROM pagos
        WHERE pagos.registro_id = registros.id AND pagos.estado IN ('Deuda','Deuda Extra')
        GROUP BY registros.grupo_id
EOE;

        $s_deuda = '<button type="button" class="btn btn-block btn-warning btn-sm">DEBE</button>';

        $registros = Registro::query()
            ->select('grupos.nombre as nombre_grupo',
                DB::raw("CONCAT(habitaciones.nombre, ' - ', pisos.nombre) as nombre_habitacion"),
                'registros.*',
                'clientes.nombre as nombre_cliente',
                DB::raw("IF(IFNULL(($sql_1),0) > 0,'$s_deuda',' ') as robservacion,IFNULL(($sql_1),0) as tdeudas"),
                'users.name as nombre_usuario'
            )
            ->orderBy('id', 'desc')
            ->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
            ->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
            ->leftJoin('clientes', 'clientes.id', '=', 'registros.cliente_id')
            ->leftJoin('grupos', 'grupos.id', '=', 'registros.grupo_id')
            ->leftJoin('users', 'users.id', '=', 'registros.user_id')
            ->where('pisos.hotel_id', $idHotel);

        $datatables = Datatables::of($registros);

        if ($keyword = $request->all()['columns'][1]['search']['value']) {
            $datatables->filterColumn('nombre_habitacion', 'whereRaw', "CONCAT(habitaciones.nombre, ' - ', pisos.nombre) like ?", ["%$keyword%"]);
        }

        if ($keyword = $request->all()['columns'][5]['search']['value']) {
            $datatables->filterColumn('robservacion', 'whereRaw', "IF(IFNULL(($sql_1),0) > 0,'$s_deuda',' ') like ?", ["%$keyword%"]);
        }

        if ($keyword = $request->all()['columns'][7]['search']['value']) {
            $datatables->filterColumn('tdeudas', 'whereRaw', "IFNULL(($sql_1),0) like ?", ["%$keyword%"]);
        }

        if ($keyword = $request->all()['search']['value']) {

            $datatables->filterColumn('nombre_habitacion', 'whereRaw', "CONCAT(habitaciones.nombre, ' - ', pisos.nombre) like ?", ["%$keyword%"]);
            $datatables->filterColumn('robservacion', 'whereRaw', "IF(IFNULL(($sql_1),0) > 0,'$s_deuda',' ') like ?", ["%$keyword%"]);
            $datatables->filterColumn('tdeudas', 'whereRaw', "IFNULL(($sql_1),0) like ?", ["%$keyword%"]);
        }

        return $datatables->make(true);
    }

}
