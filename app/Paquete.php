<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    //
    public function ingredientes(){
        return $this->hasMany('\App\Ingrediente','paquete_id');
    }
}
