<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    //
    public function hotel()
    {
        return $this->belongsTo('\App\Models\Hotel', 'hotel_id');
    }
    public function insumo()
    {
        return $this->belongsTo('\App\Insumo', 'insumo_id');
    }
    public function asignado()
    {
        return $this->belongsTo('\App\User', 'asignado_id');
    }
    public function piso()
    {
        return $this->belongsTo('\App\Models\Pisos', 'piso_id');
    }
}
