<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    //
    public function insumo(){
        return $this->belongsTo('\App\Insumo');
    }
}
