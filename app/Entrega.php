<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Entrega extends Model
{
    //
    public function paquete(){
        return $this->belongsTo('\App\Paquete', 'paquete_id');
    }
    public function getHoracreadoAttribute()
    {
        $value = $this->created_at;

        //dd($this->habitacione_id);
        if (!empty($value) && "0000-00-00 00:00:00" != $value) {
            //dd($value);
            $fecha = Carbon::parse($value);
            return $fecha->format('H:i:s');
        } else {
            return null;
        }
    }
}
