<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Insumo extends Model
{
    //
    /*public function totalc()
    {
        return $this->hasOne('App\Totale','insumo_id')->where('hotel_id',null);
    }
    public function totalh()
    {
        return $this->hasOne('App\Totale','insumo_id')->where('hotel_id',0);
    }*/

    public function getTotal($valor = null){
        $relac =  $this->hasOne('App\Totale','insumo_id')->where('hotel_id',$valor)->get()->first();
        //dd($relac->cantidad);
        if(isset($relac->cantidad)){
            return $relac->cantidad;
        }else{
            return 0;
        }
    }

}

