<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambioEstadoDeClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->string('estado')->nullable()->change();
            $table->string('procedencia')->nullable()->change();
            $table->string('profesion')->nullable()->change();
            $table->string('pasaporte')->nullable()->change();
            $table->string('ci')->nullable()->change();
            $table->string('celular')->nullable()->change();
            $table->string('referencia')->nullable()->change();
            $table->string('direccion')->nullable()->change();
            $table->text('observaciones')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            //
        });
    }
}
