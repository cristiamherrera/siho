@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
      <div class="box-header with-border">
        <h3 class="box-title">Pasos iniciales</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        {{ Html::image('img/SIHO.png', 'Inicio',  array('class'=>'img-responsive pad')) }}
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-6">
    <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
      <div class="box-header with-border">
        <h3 class="box-title">Expandable</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        The body of the box
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@endsection
