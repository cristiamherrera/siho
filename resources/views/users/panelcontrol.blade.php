@extends('layouts.app')

<link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte del dia</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Ahora: <span id="ss-ocupados"></span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Salidas: <span id="ss-salidas"></span></td>
                                    <td>Entradas: <span id="ss-entradas"></span></td>
                                </tr>
                                <tr>
                                    <td>Gastos: <span id="ss-gastos"></span></td>
                                    <td>Ingresos: <span id="ss-ingresos"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
            <div class="box-header with-border">
                <h3 class="box-title">Resumen Mensual</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open(['id' => 'formdames']) !!}
                <div class="row">
                    <div class="col-md-2">
                        Ultima fecha:
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('fecha_ini_mes', $fecha_ini_mes, ['class' => 'form-control calendario','placeholder' => '','required','id' => 'cfechadmini']) !!}
                    </div>

                    <div class="col-md-2">
                        Ultima fecha:
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('fecha_fin_mes', $fecha_fin_mes, ['class' => 'form-control calendario','placeholder' => '','required','id' => 'cfechadmfin']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
                <div id="chart_div" style="width: 100%; height: 300px;"></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
            <div class="box-header with-border">
                <h3 class="box-title">Movimientos ultimos 7 dias</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::open(['id' => 'form7dias']) !!}
                <div class="row">
                    <div class="col-md-2">
                        Ultima fecha:
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('fecha_ult7', $fecha_fin_ult7, ['class' => 'form-control calendario','placeholder' => '','required','id' => 'cfecha7dias']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
                <div id="columnchart_material" style="width: 100%; height: 300px;"></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
            <div class="box-header with-border">
                <h3 class="box-title">Salidas de insumos 3 Meses atras</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="piechart_3d" style="width: 100%; height: 350px;"></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
            <div class="box-header with-border">
                <h3 class="box-title">Movimientos por piso</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="columnchart2_material" style="width: 100%; height: 300px;"></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info box-solid" data-vivaldi-spatnav-clickable="1">
            <div class="box-header with-border">
                <h3 class="box-title">Resumen Mensual</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="donutchart" style="width: 100%; height: 300px;"></div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endsection
@push('scriptsextras')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages': ['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Dia', 'Ingresos', 'Gastos'],
            <?php foreach ($fechas_7dias as $dias):?>
            ['<?php echo $dias['fecha'] ?>', <?php echo $dias['t_ingreso'] ?>, <?php echo $dias['t_salida'] ?>],
            <?php endforeach;?>
            ]);


        var options = {
            legend: {position: 'none'},
            vAxis: {format: 'decimal'},
                /*chart: {
                    title: 'Resumen Semanal',
                    subtitle: 'Ingresos, Gastos, de los ultimos 7 dias',
                },*/
            };

            var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['bar']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data2 = google.visualization.arrayToDataTable([
                ['Piso', 'Ingresos'],
                <?php foreach($pisos_datos as $dato): ?>
                ['<?php echo $dato['nombre'] ?>', <?php echo $dato['t_ingreso'] ?>],
                <?php endforeach; ?>
                ]);

            var options = {
                legend: {position: 'none'},
                vAxis: {format: 'decimal'},
                colors: ['#FFBF00']
                /*chart: {
                    title: 'Resumen Semanal',
                    subtitle: 'Ingresos, Gastos, de los ultimos 7 dias',
                },*/
            };

            var chart2 = new google.charts.Bar(document.getElementById('columnchart2_material'));

            chart2.draw(data2, google.charts.Bar.convertOptions(options));
        }
    </script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Mes', 'Ingresos', 'Gastos'],
                /*['Ene',  560,      480],
                ['Feb',  350,      350],
                ['Mar',  500,      200],
                ['Jun',  600,      360],
                ['Jul',  500,      230],
                ['Ago',  700,      300],
                ['Sep',  1170,      460],
                ['Oct',  660,       1120],
                ['Nov',  1030,      540],
                ['Dic',  1030,      540],*/

                <?php
                foreach ($mes_datos as $dato) {
                    echo "[new Date( " . $dato['gestion'] . "," . ($dato['mes'] - 1) . "," . $dato['dia'] . ")," . $dato['t_ingreso'] . "," . $dato['t_salida'] . "],";
                }
                ?>
                /*[new Date(2015, 4, 4), 1, 7],  [new Date(2015, 4, 5), 3, 7],  [new Date(2015, 4, 6), 4, 7],
                [new Date(2015, 4, 7), 3, 7],  [new Date(2015, 4, 8), 4, 7],  [new Date(2015, 4, 9), 2, 7],
                [new Date(2015, 4, 10), 5, 7], [new Date(2015, 4, 11), 8, 7], [new Date(2015, 4, 12), 6, 7],
                [new Date(2015, 4, 13), 3, 7], [new Date(2015, 4, 14), 3, 7], [new Date(2015, 4, 15), 5, 7],
                [new Date(2015, 4, 16), 7, 7], [new Date(2015, 4, 17), 6, 7], [new Date(2015, 4, 18), 6, 7],
                [new Date(2015, 4, 19), 3, 7], [new Date(2015, 4, 20), 1, 7], [new Date(2015, 4, 21), 2, 7],
                [new Date(2015, 4, 22), 4, 7], [new Date(2015, 4, 23), 6, 7], [new Date(2015, 4, 24), 5, 7],
                [new Date(2015, 4, 25), 9, 7], [new Date(2015, 4, 26), 4, 7], [new Date(2015, 4, 27), 9, 7],
                [new Date(2015, 4, 28), 8, 7], [new Date(2015, 4, 29), 6, 7], [new Date(2015, 4, 30), 4, 7],
                [new Date(2015, 4, 31), 6, 7], [new Date(2015, 1, 1), 7, 7],  [new Date(2015, 1, 2), 9, 10]*/
                ]);

            var options = {
                chartArea: {left: 10, top: 0, width: '100%', height: '80%'},
                legend: {position: 'none'},
                title: 'Resumen',
                hAxis: {title: 'Mes', titleTextStyle: {color: '#333'}, format: 'dd MMM'},
                vAxis: {minValue: 0}
            };

            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Operarios', 'Ingreso'],
                <?php foreach($usuarios_datos as $dato): ?>
                ['<?php echo $dato['name'] ?>', <?php echo $dato['t_ingreso'] ?>],
                <?php endforeach; ?>
                ]);

            var options = {
                title: 'Ingresos por operario',
                pieHole: 0.4,
                chartArea: {left: 20, top: 50, width: '100%', height: '90%'}
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          <?php foreach ($datos_insumos as $di): ?>
          ['<?php echo $di['nombre'] ?>', <?php echo $di['total'] ?>],
          <?php endforeach ?>

          ]);

        var options = {
          is3D: true,
          chartArea: {left: 20, top: 50, width: '100%', height: '90%'}
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
      chart.draw(data, options);
  }
</script>


<script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>


<script>
    $('.calendario').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }


    $('#cfecha7dias').on('change', function () {
        $('#form7dias').submit();
    });


    $('#formdames input').on('change', function () {
        $('#formdames').submit();
    });



        //PARA EL REPORTE DE UN DIA HASTA LAS 7AM
        $.ajax({
            type: 'GET',
            url: '{!! route("reportedia") !!}',
            success: function(data){
                $('#ss-ingresos').html(data.ingresos);
                $('#ss-entradas').html(data.numero_ingresos);
                $('#ss-salidas').html(data.numero_salida);
                $('#ss-gastos').html(data.gastos);
                $('#ss-ocupados').html(data.numero_ahora);
            }
        });

    </script>

    @endpush
