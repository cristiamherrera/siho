@if(isset($opcione))
    {!! Form::model($opcione, ['route' => ['terminos_condiciones'], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['terminos_condiciones']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Terminos & Condiciones</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control','placeholder' => 'Ingrese los terminos y condiciones que desea para los hoteles','rows' => 10]) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('id') !!}
{!! Form::close() !!}
