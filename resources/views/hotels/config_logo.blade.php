@if(isset($opcione))
    {!! Form::model($opcione, ['route' => ['config_logo'], 'files'=>true, 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['config_logo'], 'files'=>true]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Logo</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        @if(isset($opcione->descripcion))
        <div class="row">
            <div class="col-md-12">
                <img class="img-responsive pad" style="max-height: 120px;" src="{!! url('adjuntos/'.$opcione->descripcion) !!}" alt="Product Image">
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::file('archivo',['required','accept' => 'image/*']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('id') !!}
{!! Form::close() !!}
