@if(isset($opcione))
    {!! Form::model($opcione, ['route' => ['tiempo_cancelar_reg'], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['tiempo_cancelar_reg']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Tiempo de cancelacion de registro</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::number('descripcion', null, ['class' => 'form-control','placeholder' => 'Ingrese el tiempo en minutos','required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('id') !!}
{!! Form::close() !!}
