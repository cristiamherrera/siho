@if(isset($opcione))
    {!! Form::model($opcione, ['route' => ['config_hora_camb_fecha'], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['config_hora_camb_fecha']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Hora de cambio de fecha</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::select('descripcion',range(0,23), null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('id') !!}
{!! Form::close() !!}
