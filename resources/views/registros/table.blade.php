<div class="box">
    <div class="box-body table-responsive">
        <table class="table table-responsive table-bordered" id="registros-table">
            <thead>
            <th>Grupo</th>
            <th>Habitacione</th>
            <th>Estado</th>
            <th>Fecha Ingreso</th>
            <th>Fecha Salida</th>
            <th>Observacion</th>
            <th>Precio</th>
            <th>Monto Total</th>
            <th>Usuario</th>
            <th></th>
            </thead>
            <thead>
            <th>Grupo</th>
            <th>Habitacione</th>
            <th>Estado</th>
            <th>Fecha Ingreso</th>
            <th>Fecha Salida</th>
            <th>Observacion</th>
            <th>Precio</th>
            <th>Deuda</th>
            <th>Usuario</th>
            <th width="8%">Action</th>
            </thead>
            <tbody>


            </tbody>
        </table>
    </div>
</div>


@push('scriptsextras')
    <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script>

        var table = null;
        $(function () {
            table = $('#registros-table').DataTable({
                processing: true,
                serverSide: true,
                'order': [],
                "bSort": false,
                ajax: '{!! route('datatables.registros') !!}',
                columns: [
                    {data: 'nombre_grupo', name: 'grupos.nombre'},
                    {data: 'nombre_habitacion', name: "nombre_habitacion"},
                    {data: 'estado', name: 'registros.estado'},
                    {data: 'fecha_ingreso', name: 'fecha_ingreso'},
                    {data: 'fecha_salida', name: 'fecha_salida'},
                    {data: 'robservacion', name: 'robservacion'},
                    {data: 'precio', name: 'precio'},
                    {data: 'tdeudas', name: 'tdeudas'},
                    {data: 'nombre_usuario', name: 'users.name'},
                    {data: 'id', name: 'id'},


                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    if (parseFloat(aData['tdeudas']) > 0) {
                        $('td', nRow).addClass('info');
                    }

                    var ac_ver = '';
                    if (parseFloat(aData['estado']) != 'Desocupado') {
                        ac_ver = '<a class="btn btn-info btn-sm" href="{!! url('registrosgrupos') !!}/'+aData['grupo_id']+'"><i class="fa fa-eye"></i></a>&nbsp;';
                    }
                    var confirm_a = "if(confirm('Esta seguro de cancelar el registro??')){window.location.href = '{!! url('cancelaregistro') !!}/"+aData['id']+"';}";
                    var ac_eliminar = '<a href="javascript:" class="btn btn-danger btn-sm" style="color: white;" onclick="'+confirm_a+'" title="CANCELAR REGISTRO"><i class="fa fa-times"></i></a>';
                    $('td:eq(9)', nRow).html(ac_ver+ac_eliminar);


                },
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('#registros-table thead:eq( 0 ) th').each(function () {
                var title = $('#registros-table thead:eq( 0 ) th').eq($(this).index()).text();
                if (title != '') {
                    $(this).html('<input type="text" style="width: 100%;" placeholder="' + title + '" />');
                }
            });
            table.columns().eq(0).each(function (colIdx) {
                $('input', table.column(colIdx).header()).on('keyup change', function () {
                    table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
                });
            });
        });

    </script>
@endpush