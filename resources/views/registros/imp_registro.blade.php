<html>
<head>
    <title>REGISTRO DE HABITACION</title>
    <style>

        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 100%;
            border: 1px solid #000000;

            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;

            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;

            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;

            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .CSSTableGenerator table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
        }

        .CSSTableGenerator tr:last-child td:last-child {
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .CSSTableGenerator table tr:first-child td:first-child {
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .CSSTableGenerator table tr:first-child td:last-child {
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
        }

        .CSSTableGenerator tr:last-child td:first-child {
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .CSSTableGenerator tr:hover td {
            background-color: #ffffff;

        }

        .CSSTableGenerator td {
            vertical-align: middle;

            background-color: #ffffff;

            border: 1px solid #000000;
            border-width: 0px 1px 1px 0px;
            padding: 3px;
            font-size: 10px;
            font-family: Arial;
            color: #000000;
        }

        .CSSTableGenerator tr:last-child td {
            border-width: 0px 1px 0px 0px;
        }

        .CSSTableGenerator tr td:last-child {
            border-width: 0px 0px 1px 0px;
        }

        .CSSTableGenerator tr:last-child td:last-child {
            border-width: 0px 0px 0px 0px;
        }
    </style>
</head>
<body>
@if(isset($logo->descripcion))
<img class="img-responsive pad" style="max-height: 80px; position: fixed;opacity: 0.6;" src="{!! url('adjuntos/'.$logo->descripcion) !!}">
<br>
<br>
@endif
<h3 align="center">REGISTRO DE HABITACION </h3>

<table class="CSSTableGenerator">
    <tbody>
    <tr>
        <td><b>HABITACION: </b></td>
        <td>{!! $registro->habitacione->nombre.' - '.$registro->habitacione->rpiso->nombre.' - '.(isset($registro->habitacione->categoria->nombre) ? $registro->habitacione->categoria->nombre: 'S/C') !!}</td>
        <td><b>GRUPO: </b></td>
        <td>{!! $registro->grupo->nombre !!}</td>
    </tr>
    <tr>
        <td><b>FECHA INRGESO: </b></td>
        <td>{!! $registro->fecha_ingreso2 !!}</td>
        <td><b>PRECIO: </b></td>
        <td>{!! $registro->precio !!}</td>
    </tr>
    </tbody>
</table>
<h4 align="center">Hospedantes</h4>
@foreach($registro->hospedantes as $hospedante)
    <table class="CSSTableGenerator">
        <tbody>
        <tr>
            <td><b>NOMBRE: </b></td>
            <td>{!! $hospedante->cliente->nombre !!}</td>
            <td><b>NACIONALIDAD: </b></td>
            <td>{!! $hospedante->cliente->nacionalidad !!}</td>
        </tr>
        <tr>
            <td><b>EDAD: </b></td>
            <td>{!! $hospedante->cliente->edad2 !!}</td>
            <td><b>PROCEDENCIA: </b></td>
            <td>{!! $hospedante->cliente->procedencia !!}</td>
        </tr>
        <tr>
            <td><b>C.I.: </b></td>
            <td>{!! $hospedante->cliente->ci !!}</td>
            <td><b>PASAPORTE: </b></td>
            <td>{!! $hospedante->cliente->pasaporte !!}</td>
        </tr>
        <tr>
            <td><b>CELULAR: </b></td>
            <td>{!! $hospedante->cliente->celular !!}</td>
            <td><b>PROFESION: </b></td>
            <td>{!! $hospedante->cliente->profesion !!}</td>
        </tr>
        </tbody>
    </table>
    <p align="center">
        -----------------------------------------------------------------------
    </p>
@endforeach
@if(!empty($terminos->descripcion))
<h4 align="center">TERMINOS Y CONDICIONES</h4>
    <p>
        {!! $terminos->descripcion !!}
    </p>
@endif
<script>
    window.print();
</script>
</body>
</html>