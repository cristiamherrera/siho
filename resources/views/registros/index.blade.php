@extends('layouts.app')

@section('content')
	<h1 class="pull-left">Registros</h1>
	<div class="clearfix"></div>
	@include('flash::message')
	<div class="clearfix"></div>
	@include('registros.table')
@endsection
