<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $registro->id !!}</p>
</div>

<!-- Cliente Id Field -->
<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    <p>{!! $registro->cliente_id !!}</p>
</div>

<!-- Habitacione Id Field -->
<div class="form-group">
    {!! Form::label('habitacione_id', 'Habitacione Id:') !!}
    <p>{!! $registro->habitacione_id !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $registro->estado !!}</p>
</div>

<!-- Fecha Ingreso Field -->
<div class="form-group">
    {!! Form::label('fecha_ingreso', 'Fecha Ingreso:') !!}
    <p>{!! $registro->fecha_ingreso !!}</p>
</div>

<!-- Fecha Salida Field -->
<div class="form-group">
    {!! Form::label('fecha_salida', 'Fecha Salida:') !!}
    <p>{!! $registro->fecha_salida !!}</p>
</div>

<!-- Observacion Field -->
<div class="form-group">
    {!! Form::label('observacion', 'Observacion:') !!}
    <p>{!! $registro->observacion !!}</p>
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('precio', 'Precio:') !!}
    <p>{!! $registro->precio !!}</p>
</div>

<!-- Monto Total Field -->
<div class="form-group">
    {!! Form::label('monto_total', 'Monto Total:') !!}
    <p>{!! $registro->monto_total !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $registro->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $registro->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $registro->updated_at !!}</p>
</div>

