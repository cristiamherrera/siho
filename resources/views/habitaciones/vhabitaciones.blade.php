@extends('layouts.app')

@section('content')
    <style>
        .in-habit {
            height: 60px;
        }

        .small-box .icon{
            font-size: 60px;
        }

        .tit-habit{
            font-size: 30px !important;
        }
        .small-box:hover .icon{
            font-size: 68px;
        }

        .ddd-habit:hover{
            cursor: pointer;
        }

    </style>
    @include('flash::message')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Hotel: {!! $hotel->nombre  !!}</h3>
            <?php
$role = Auth::user()->rol;
?>
            @if($role != 'Operario')
                <div class="box-tools pull-right">
                    <a href="{!! url('nuevaHabitacion', $hotel->id) !!}" class="btn btn-success btn-box-tool"
                       style="color: white;"><b>NUEVA HABITACION</b></a>

                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @foreach($pisos as $piso)

                <h3 class="text-center text-primary"><b>{!! $piso->nombre !!}</b></h3>
                <div class="row">

                    @foreach($piso->habitaciones as $habitacion)

                        <?php
$color_h = 'aqua';
$esyado_h = 'Libre';
$color_h_letra = '';

if ($habitacion->estado == 'Deshabilitado') {
	$color_h = 'default';
	$esyado_h = 'Deshabilitado';
} elseif ($habitacion->estado == 'Limpieza') {
	$color_h = 'green';
	$esyado_h = 'Limpieza';
} elseif ($habitacion->estaocupado) {
	$color_h = 'red';
	$esyado_h = 'Ocupado';
}
if ($habitacion->estareservado) {
	$color_h_letra = 'style="color:#FFFF00"; ';

	//$color_h = 'yellow';
	//$esyado_h = 'Reservado';
}
?>

                        <div class="col-lg-1 col-xs-2">
                            <!-- small box -->
                            <div class="small-box bg-{!! $color_h !!} ddd-habit" dato-id="{{ $habitacion->id }}">
                                <div class="inner in-habit"  >
                                    <h3 class="tit-habit" {!! $color_h_letra !!}>{!! $habitacion->nombre !!}</h3>

                                    {{--<p>Habitacion <b>{!! $esyado_h !!}</b></p>
                                    <p>
                                        @if(isset($habitacion->categoria->nombre))
                                            {!! $habitacion->categoria->nombre !!}
                                        @endif
                                    </p>--}}
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pricetag"></i>
                                </div>
                                <a href="javascript:"
                                   {{--onclick="cargarmodal('{!! route('informacion_habitacion',[$habitacion->id]) !!}','info')"--}}
                                   class="small-box-footer">{!! $esyado_h !!}</a>
                            </div>
                        </div>
                    @endforeach

                </div>
            @endforeach
        </div>
    </div>



@endsection
@push('scriptsextras')
{{--    @include('layouts.partials.jsdatatable')--}}
    <script>
        $('.ddd-habit').on('click',function(){
//            alert("dsad");
            cargarmodal('{{ url('informacion_habitacion') }}/'+$(this).attr('dato-id'),'info');
        });
    </script>
@endpush