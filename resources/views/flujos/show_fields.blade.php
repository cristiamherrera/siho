<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $flujo->id !!}</p>
</div>

<!-- Ingreso Field -->
<div class="form-group">
    {!! Form::label('ingreso', 'Ingreso:') !!}
    <p>{!! $flujo->ingreso !!}</p>
</div>

<!-- Salida Field -->
<div class="form-group">
    {!! Form::label('salida', 'Salida:') !!}
    <p>{!! $flujo->salida !!}</p>
</div>

<!-- Detalle Field -->
<div class="form-group">
    {!! Form::label('detalle', 'Detalle:') !!}
    <p>{!! $flujo->detalle !!}</p>
</div>

<!-- Observacion Field -->
<div class="form-group">
    {!! Form::label('observacion', 'Observacion:') !!}
    <p>{!! $flujo->observacion !!}</p>
</div>

<!-- Flujo Id Field -->
<div class="form-group">
    {!! Form::label('flujo_id', 'Flujo Id:') !!}
    <p>{!! $flujo->flujo_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $flujo->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $flujo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $flujo->updated_at !!}</p>
</div>

