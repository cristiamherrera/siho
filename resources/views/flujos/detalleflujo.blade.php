<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">DETALLE DEL FLUJO</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><b>ID:</b> {!! $flujo->id !!}</td>
                        <td><b>Fecha</b> {!! $flujo->created_at !!}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {!! $flujo->detalle !!}
                        </td>
                    </tr>
                    <tr>
                        <td><b>Ingreso:</b> {!! $flujo->ingreso !!}</td>
                        <td><b>Egreso</b> {!! $flujo->salida !!}</td>
                    </tr>
                </table>
                @if(!empty($pagos_nor->all()))
                    <h4 class="text-center">DETALLE DE INGRESOS</h4>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Habitacion</th>
                            <th>Tipo</th>
                            <th>Piso</th>
                            <th>Grupo</th>
                            <th>Monto</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pagos_nor as $pago)
                            <tr>
                                <td>{!! $pago->fecha2 !!}</td>
                                <td>{!! $pago->registro->habitacione->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->categoria->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->rpiso->nombre !!}</td>
                                <td>{!! $pago->registro->grupo->nombre !!}</td>
                                <td>{!! $pago->monto_total !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
                @if(!empty($pagos_ret->all()))
                    <h4 class="text-center">DETALLE DE EGRESO</h4>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Habitacion</th>
                            <th>Tipo</th>
                            <th>Piso</th>
                            <th>Grupo</th>
                            <th>Monto</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pagos_ret as $pago)
                            <tr>
                                <td>{!! $pago->fecha2 !!}</td>
                                <td>{!! $pago->registro->habitacione->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->categoria->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->rpiso->nombre !!}</td>
                                <td>{!! $pago->registro->grupo->nombre !!}</td>
                                <td>{!! $pago->monto_total !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
</div>

