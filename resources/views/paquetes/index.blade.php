@extends('layouts.app')

@section('content')

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Listado de Paquetes</h3>
            <div class="box-tools pull-right no-imprimir">
                <button class="btn btn-box-tool btn-success" title="Nuevo Paquete"
                        onclick="cargarmodal('{!! route('paquete') !!}')" style="color: #ffffff;"><i
                            class="fa fa-plus"></i> <b>Nuevo Paquete</b></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-responsive table-bordered">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Nombre</th>
                    <th>Ingredientes</th>
                    <th>Estado</th>
                    <th>Descripcion</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($paquetes as $paquete)
                    <tr>
                        <td>{!! $paquete->id !!}</td>
                        <td>{!! $paquete->nombre !!}</td>
                        <td>
                            @foreach($paquete->ingredientes as $ingrediente)
                                <a onclick="cargarmodal('{!! route('ingrediente', [$paquete->id,$ingrediente->id]) !!}')" href="javascript:">{!! $ingrediente->cantidad.' '.$ingrediente->insumo->medida.' '.$ingrediente->insumo->nombre !!}</a><br>
                            @endforeach
                        </td>
                        <td>{!! $paquete->estado !!}</td>
                        <td>{!! $paquete->descripcion !!}</td>
                        <td>
                            <div class='btn-group'>
                                <a href="javascript:" onclick="cargarmodal('{!! route('paquete', [$paquete->id]) !!}')"
                                   class='btn btn-default btn-xs'><i
                                            class="glyphicon glyphicon-edit"></i></a>

                                <a href="javascript:"
                                   onclick="cargarmodal('{!! route('ingrediente', [$paquete->id]) !!}')"
                                   class='btn btn-info btn-xs'><i
                                            class="fa fa-coffee"></i></a>

                                <a href="javascript:"
                                   onclick="if(confirm('Esta seguro de eliminar el usuario??')){window.location.href = '{!! route('eliminar_paquete', [$paquete->id]) !!}';}"
                                   class='btn btn-danger btn-xs'><i
                                            class="fa fa-remove"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection


@push('scriptsextras')
@include('layouts.partials.jsdatatable')
@endpush