{!! Form::open(['route' => ['guarda_entrega_insumo']]) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Entrega de Paquete</h4>
</div>

<div class="modal-body">
    <div class="form-group">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Seleccione el Paquete:</label>
                    {!! Form::select('paquete_id', $paquetes,null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Observacion:</label>
                    {!! Form::textarea('observacion', null, ['class' => 'form-control','placeholder' => 'Ingrese una observacion de la entrega','rows' => 3]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
@foreach($clientes_l as $idCliente => $cliente)
    <input type="hidden" name="clientes[{!! $idCliente !!}][valor]">
@endforeach
{!! Form::close() !!}

