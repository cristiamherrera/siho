@if(isset($paquete))
    {!! Form::model($paquete, ['route' => ['guarda_paquete',$paquete->id], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['guarda_paquete']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Formulario de Paquete</h4>
</div>

<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nombre del Paquete:</label>
                    {!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Nombre del paquete','required']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Descripcion:</label>
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control','placeholder' => 'Ingrese la descripcion del paquete','rows' => 3]) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Estado:</label>
                    {!! Form::select('estado', ['Activo' => 'Activo','Inactivo' => 'Inactivo'],null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::close() !!}


