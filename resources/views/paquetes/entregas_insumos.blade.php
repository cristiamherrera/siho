@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Listado de clientes en fecha <input type="text" onchange="window.location.href = '{!! url('entregas_insumos') !!}/'+this.value;" class="calendario" size="9" value="{!! $fecha_d !!}">

            </h3>
            <div class="box-tools pull-right no-imprimir">

                <button class="btn btn-box-tool btn-success" title="REGISTRAR ENTREGA"
                        onclick="enviarformu()" style="color: #ffffff;"><i
                            class="fa fa-plus"></i> <b>REGISTRAR ENTREGA</b></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::open(['route' => ['registra_entregas_pa'],'id' => 'form-entregas']) !!}
            <table class="table table-responsive table-bordered">
                <thead>
                <tr>
                    <th>{!! Form::checkbox('todos', 'value',null,['id' => 'c-todos-p'])!!}</th>
                    <th>#ID</th>
                    <th>Nombre</th>
                    <th>Habitacion</th>
                    <th>Piso</th>
                    <th>Paquetes</th>
                </tr>
                </thead>
                <tbody>
                @foreach($hospedantes as $hospedante)
                    <tr>
                        <td>
                            {!! Form::checkbox("clientes[".$hospedante->cliente->id."][marcado]", null,null,['class' => 'c-todos-pt'])!!}
                        </td>
                        <td>{!! $hospedante->id !!}</td>
                        <td>{!! $hospedante->cliente->nombre !!}</td>
                        <td>{!! $hospedante->registro->habitacione->nombre !!}</td>
                        <td>{!! $hospedante->registro->habitacione->rpiso->nombre !!}</td>
                        <td>
                            @foreach($hospedante->cliente->getEntregas($idHotel,$fecha_d) as $entrega)
                                <a href="javascript:" title="ELIMINAR ENTREGA" onclick="if(confirm('Esta seguro de eliminar la entrega??')){window.location.href = '{!! route('eliminar_entrega_in', [$entrega->id]) !!}';}">{!! $entrega->paquete->nombre.' - '.$entrega->horacreado !!}</a><br>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! Form::close() !!}
        </div>
    </div>

@endsection


@push('scriptsextras')
@include('layouts.partials.jsdatatable')
<script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
    function enviarformu() {
        var postData = $("#form-entregas").serializeArray();
        var formURL = $("#form-entregas").attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,

                    beforeSend: function (XMLHttpRequest) {

                        $('#mimodal').attr('class', 'modal modal-primary');
                        $('#mimodal div.modal-dialog').addClass('modal-md');
                        $('#divmodal').hide();
                        $("#spin-cargando-mod").show(200);
                        $('#mimodal').modal('show', {backdrop: 'static'});
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        //alert('despues de enviar');
                    },
                    success: function (data, textStatus, jqXHR) {
                        $("#divmodal").html(data);
                        setTimeout(function () {
                            $("#spin-cargando-mod").hide();
                            $('#divmodal').show();
                        }, 1500);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                        alert("error");
                    }
                });
    }

    $('.calendario').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
</script>

<script>
    $('#c-todos-p').click(function () {
        $('.c-todos-pt').prop('checked', $('#c-todos-p').prop('checked'));
    });
</script>
@endpush