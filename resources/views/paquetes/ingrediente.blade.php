@if(isset($ingrediente))
    {!! Form::model($ingrediente, ['route' => ['guarda_ingrediente',$ingrediente->id], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['guarda_ingrediente']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Formulario de Ingrediente</h4>
</div>

<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Insumo:</label>
                    {!! Form::select('insumo_id', $insumos,null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Cantidad:</label>
                    {!! Form::text('cantidad', null, ['class' => 'form-control','placeholder' => 'Ingrese la cantidad del insumo','required']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Descripcion:</label>
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control','placeholder' => 'Ingrese la descripcion del ingrediente','rows' => 3]) !!}
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    @if(isset($ingrediente))
        <button type="button" class="btn btn-outline btn-danger pull-left"
                onclick="if(confirm('Esta seguro de eliminar el ingrediente??')){window.location.href = '{!! route('eliminar_ingrediente', [$ingrediente->id]) !!}';}">
            Eliminar
        </button>
    @endif
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('paquete_id',$idPaquete) !!}
{!! Form::close() !!}


