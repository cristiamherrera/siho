@extends('layouts.app')

@section('content')

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Listado de Parametros</h3>
            <div class="box-tools pull-right no-imprimir">
                <button class="btn btn-box-tool btn-success" title="Nuevo Parametro"
                        onclick="cargarmodal('{!! route('parametro') !!}')" style="color: #ffffff;"><i
                            class="fa fa-plus"></i> <b>Nueva Parametro</b></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-responsive table-bordered">
                <thead>
                <tr>
                    <th>Nit</th>
                    <th>Numero de Autorizacion</th>
                    <th>Llave</th>
                    <th>Fecha Limite</th>
                    <th>Numero Ref</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($parametros as $parametro)
                    <tr>
                        <td>{!! $parametro->nit !!}</td>
                        <td>{!! $parametro->numero_autorizacion !!}</td>
                        <td>{!! $parametro->llave !!}</td>
                        <td>{!! $parametro->fechalimite !!}</td>
                        <td>{!! $parametro->numero_ref !!}</td>
                        <td>
                            <div class='btn-group'>
                                <a href="javascript:" onclick="cargarmodal('{!! route('parametro', [$parametro->id]) !!}')" class='btn btn-default btn-xs'><i
                                            class="glyphicon glyphicon-edit"></i></a>

                                <a href="javascript:" onclick="if(confirm('Esta seguro de eliminar el usuario??')){window.location.href = '{!! route('eliminarparametro', [$parametro->id]) !!}';}" class='btn btn-danger btn-xs'><i
                                            class="fa fa-remove"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection


@push('scriptsextras')
    @include('layouts.partials.jsdatatable')
@endpush