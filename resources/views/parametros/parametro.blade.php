<link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
@if(isset($parametro))
    {!! Form::model($parametro, ['route' => ['guarda_parametro',$parametro->id], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['guarda_parametro']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Formulario de Parametro</h4>
</div>

<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nit:</label>
                    {!! Form::text('nit', null, ['class' => 'form-control','placeholder' => 'Nit de la empresa','required']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Numero Autorizacion:</label>
                    {!! Form::text('numero_autorizacion', null, ['class' => 'form-control','placeholder' => 'Numero Autorizacion','required']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Llave:</label>
                    {!! Form::text('llave', null, ['class' => 'form-control','placeholder' => 'Llave','required']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Fecha Limite:</label>
                    {!! Form::text('fechalimite', null, ['class' => 'form-control calendario','placeholder' => 'Fecha limite','required']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Numero Referencia:</label>
                    {!! Form::text('numero_ref', null, ['class' => 'form-control','placeholder' => 'Numero de referencia','required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::close() !!}

<script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$('.calendario').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
</script>

