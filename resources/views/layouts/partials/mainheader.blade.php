<!-- Main Header -->
<script type="text/javascript">
function spanishDate(d){
    var weekday=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
    var monthname=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    return weekday[d.getDay()]+" "+d.getDate()+" de "+monthname[d.getMonth()]+" de "+d.getFullYear()
}
</script>
<style>
    .estadosmenul {
        float: left;
        background-color: transparent;

        padding: 15px 15px;
        font-family: fontAwesome;
        color: #fff;
        font-size: 18px;
        font-weight: bold;
        font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
</style>
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SIHO</b>Administracion </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" id="campoestadoshabit" data-toggle="offcanvas" role="button">
            <a 
                class="btn btn-success" 
                style="margin-top: 10px" 
                href="javascript:" 
                onclick="cargarmodal('{!! route('cliente') !!}','primary','lg')"
            ><i class="fa fa-plus"></i> Cliente
            </a>
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                <!-- User Account Menu -->
                <script>                    
                    var d = new Date();
                </script>
                    <li><a href="javascript:" style="font-weight: bold; font-size: 22px;" id="reloj">00:00:00</a></li>
                    <li><a href="javascript:" style="font-weight: bold; font-size: 22px;" ><script>document.write(spanishDate(d));</script></a></li>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{asset('/img/user-ico.ico')}}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{asset('/img/user-ico.ico')}}" class="img-circle" alt="User Image"/>
                                <p>
                                    {{ Auth::user()->name }}
                                    <small> {{ Auth::user()->rol }}</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                 <a href="javascript:" onclick="cargarmodal('{!! route('usuario',[Auth::user()->id]) !!}');" class="btn btn-default btn-flat">EDITAR</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}"  class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>
                                </div>



                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>
<script>
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('reloj').innerHTML =
                h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
        ;  // add zero in front of numbers < 10
        return i;
    }
</script>