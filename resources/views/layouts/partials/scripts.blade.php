<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
@if(Auth::user()->hotel_id != 0)
    <script>
        $.ajax({
            url: '{!! route('getestadoshabit') !!}',
            type: 'GET',
            success: function (data) {
                canha_ocupa = '<a href="javascript:" class="estadosmenul" title"Ocupados">OC: <b style="color: #ffffcc;">' + data.ocupados + '</b></a>';
                canha_limpieza = '<a href="javascript:" class="estadosmenul" title"Limpieza">LI: <b style="color: #ffffcc;">' + data.limpieza + '</b></a>';
                canha_libres = '<a href="javascript:" class="estadosmenul" title"Libres">LB: <b style="color: #ffffcc;">' + data.libres + '</b></a>';
                canha_deshab = '<a href="javascript:" class="estadosmenul" title"Deshabilitados">DS: <b style="color: #ffffcc;">' + data.deshabilitados + '</b></a>';
                canha_reservas = '<a style="color: #101010;" href="javascript:" class="estadosmenul" title"Reservas">RE: <b style="color: #ffffcc;">' + data.reservas + '</b></a>';
                $('#campoestadoshabit').after(canha_ocupa + canha_limpieza + canha_libres +canha_deshab + canha_reservas)
            }

        });
    </script>
@endif