<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('/img/user-ico.ico')}}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->rol }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
    @endif

    <!-- search form (Optional) -->

        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li>{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('usuarios') }}"><i class='fa fa-users'></i> <span>Usuarios</span></a>
            </li>
            <li><a href="{{ url('hotels') }}"><i class='fa fa-building-o'></i> <span>Hoteles</span></a>
            </li>
            <li><a href="{{ url('clientes') }}"><i class='fa fa-building-o'></i>
                    <span>Clientes</span></a></li>


            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>REPORTES</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ url('pasajeros_reporte') }}"><i class="fa fa-circle-o"></i> <span>Reporte de Pasajeros</span></a>
                    </li>
                    <li class="active"><a href="{{ url('reporte_pagos') }}"><i class='fa fa-circle-o'></i> <span>Reporte de Cajas</span></a>
                    </li>
                    <li class="active"><a href="{{ url('repo_pago_retirados') }}"><i class='fa fa-circle-o'></i> <span>Reporte de Pagos Retirados</span></a>
                    </li>
                    <li class="active"><a href="{{ url('repo_pago_regis') }}"><i class='fa fa-circle-o'></i> <span>Reporte de Pagos</span></a>
                    </li>
                    <li class="active"><a href="{{ url('reporte_registros') }}"><i class='fa fa-circle-o'></i> <span>Reporte de Registros</span></a>
                    </li>
                </ul>
            </li>
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>CONFIGURACIONES</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li ><a href="javascript:" onclick="cargarmodal('{!! route('terminos_condiciones') !!}')"><i class="fa fa-circle-o"></i> <span>Terminos y condiciones</span></a>
                    </li>
                    <li ><a href="javascript:" onclick="cargarmodal('{!! route('tiempo_cancelar_reg') !!}')"><i class="fa fa-circle-o"></i> <span>Tiempo de cancel registro</span></a>
                    </li>
                    <li ><a href="javascript:" onclick="cargarmodal('{!! route('config_logo') !!}')"><i class="fa fa-circle-o"></i> <span>Logo</span></a>
                    </li>
                    <li ><a href="javascript:" onclick="cargarmodal('{!! route('config_hora_lim') !!}')"><i class="fa fa-circle-o"></i> <span>Hora Limite de salida</span></a>
                    </li>
                    <li ><a href="javascript:" onclick="cargarmodal('{!! route('config_hora_camb_fecha') !!}')"><i class="fa fa-circle-o"></i> <span>Hora de cambio de fecha</span></a>
                    </li>
                </ul>
            </li>
            <li class=""><a href="{{ url('insumos') }}"><i class='fa fa-archive'></i> <span>Inventario</span></a></li>
            <li class=""><a href="{{ url('parametros') }}"><i class='fa fa-archive'></i> <span>Parametros Factura</span></a></li>
            <li class=""><a href="{{ url('tutoriales') }}"><i class='fa fa-archive'></i> <span>Tutoriales</span></a></li> 
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
