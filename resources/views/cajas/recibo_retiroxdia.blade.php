<html>
<head>
    <title>INFORME DE PAGOS DE GRUPO</title>
    <style>

        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 100%;
            border: 1px solid #000000;

            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;

            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;

            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;

            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .CSSTableGenerator table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
        }

        .CSSTableGenerator tr:last-child td:last-child {
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .CSSTableGenerator table tr:first-child td:first-child {
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .CSSTableGenerator table tr:first-child td:last-child {
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
        }

        .CSSTableGenerator tr:last-child td:first-child {
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .CSSTableGenerator tr:hover td {
            background-color: #ffffff;

        }

        .CSSTableGenerator td {
            vertical-align: middle;

            background-color: #ffffff;

            border: 1px solid #000000;
            border-width: 0px 1px 1px 0px;
            padding: 3px;
            font-size: 10px;
            font-family: Arial;
            color: #000000;
        }

        .CSSTableGenerator tr:last-child td {
            border-width: 0px 1px 0px 0px;
        }

        .CSSTableGenerator tr td:last-child {
            border-width: 0px 0px 1px 0px;
        }

        .CSSTableGenerator tr:last-child td:last-child {
            border-width: 0px 0px 0px 0px;
        }
    </style>
</head>
<body>
<h3 style="text-align: center;">NOTA DE ENTREGA #{!! $flujo->id !!}</h3>


@if(!empty($flujo->retiros))
    <p>
        POR CONCEPTO DE <?= strtoupper($flujo->detalle) ?> CON EL SIGUIENTE DETALLE POR FECHA:

    </p>

    <table class="CSSTableGenerator">
        <thead>
        <tr>
            <th>Fecha</th>

            <th>Monto</th>
        </tr>
        </thead>
        <tbody>
        <?php $total_det = 0; ?>
        @foreach($flujos_det as $fludet)
            <?php $total_det += $fludet->monto_total ?>
            <tr>
                <td>{!! $fludet->lafecha !!}</td>

                <td>{!! $fludet->monto_total !!}</td>
            </tr>
        @endforeach

        <tr>

            <td>TOTAL:</td>
            <td><b>{!! $total_det !!}</b></td>
        </tr>
        </tbody>
    </table>
@else
    <p>
        No se encontro el detalle de este flujo!!!
    </p>
@endif
<br><br><br><br><br>
@if(isset($flujo->user) && isset($flujo->ruser))
<table style="width: 100%">
    <tr>
        <td style="border-top: 1px black solid; text-align: center">
            RECIBI CONFORME<br>
            {!! strtoupper($flujo->user->name) !!}
        </td>
        <td>
            &nbsp;&nbsp;
        </td>
        <td style="border-top: 1px black solid; text-align: center">
            ENTREGUE CONFORME<br>
            {!! strtoupper($flujo->ruser->name) !!}
        </td>
    </tr>
</table>
@endif

<script>
    window.print();
</script>
</body>
</html>