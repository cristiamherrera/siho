{!! Form::open(['url' => 'caja/eliminar_flujo/'.$idFlujo]) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Eliminar flujo</h4>
</div>

@if($flujo->detalle != 'DEUDA DE ENTREGA' && $flujo->detalle != 'DEUDA DE TRASPASO')
    <div class="modal-body">
        <div class="form-group">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(!empty($ultima_entrega))
                            <h2>NO PUEDE ELIMINAR ESTE FLUJO DEBIDO A QUE YA FUE ENTREGADO</h2>
                        @else
                            @if($flujo->detalle == 'ENTREGA DE CAJA CHICA' || $flujo->detalle == 'TRASPASO DE CAJA CHICA')
                                <h4>NOTA: AL ELIMINAR ESTE FLUJO SE ELIMINA LA ENTREGA COMPLETA (INGERSOS,SALIDAS Y
                                    DEUDAS)</h4>
                            @elseif($flujo->detalle == 'DEUDA CANCELADA')
                                <h4>NOTA: AL ELIMINAR LA DEUDA CANCELADA REGRESA LA DEUDA</h4>
                            @endif

                            <label>Motivo por el cual desea eliminar el flujo??</label>
                            {!! Form::textarea('observacion', null, ['class' => 'form-control','placeholder' => 'Observacion...','rows' => 3,'required']) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
        @if(empty($ultima_entrega))
            {!! Form::submit('Eliminar Flujo', ['class' => 'btn btn-outline pull-left']) !!}
        @endif
    </div>
    {!! Form::close() !!}

@else
    <?php
    $role = Auth::user()->rol;
    ?>
    <div class="modal-body">
        <div class="form-group">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if($role == 'Operario' )
                            <h4>NOTA: SE CANCELARA LA DEUDA Y SE GENERARA UN INGRESO PARA COMPSENSARLO</h4>
                            {!! Form::hidden('opcion','Cancelar deuda') !!}
                        @else
                            <h4>NOTA: AL CANCELAR LA DEUDA SE GENERA UN INGRESO PARA COMPSENSARLO Y EL ELIMINAR NO
                                ELIMINA LA ENTREGA COMPLETA</h4>
                            <label for="">Seleccione la opcion</label>
                            {!! Form::select('opcion',['Cancelar deuda' => 'Cancelar deuda','Eliminar deuda' => 'Eliminar deuda'],null,['class' => 'form-control']) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
        {!! Form::submit('Enviar', ['class' => 'btn btn-outline pull-left']) !!}
    </div>
    {!! Form::close() !!}
@endif