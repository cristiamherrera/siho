@extends('layouts.app')

@section('content')
    <h1 class="pull-left">FLUJOS DE {!! strtoupper($caja->nombre) !!}</h1>

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                {!! Form::open(['route' => ['retirarpagosdia'],'id' => 'form-habit','onsubmit' => "return confirm('Esta seguro de registrar los pagos como retirados?')"]) !!}
                <div class="box-header">
                    <h3 class="box-title">movimientos no retirados x dia <b>Total: <span id="total_mar">0</span> Bs</b>
                    </h3>

                    <div class="box-tools pull-right">

                        <a href="{!! route('flujos',[$caja->id]) !!}" class="btn btn-primary btn-box-tool"
                           style="color: yellow;"><b>IR A CAJA</b></a>
                        <a href="{!! route('ultimosretiros',[$caja->id]) !!}" class="btn btn-info btn-box-tool"
                           style="color: yellow;"><b>ULTIMOS RETIROS</b></a>

{{--                        {!! Form::button('<b>RETIRAR</b>', ['type' => 'submit', 'class' => 'btn btn-success btn-box-tool', 'style' => 'color: white;','onclick' => "return confirm('Esta seguro de registrar los pagos como retirados?')"]) !!}--}}
                    </div>
                </div>
                <style>
                    th.centro {
                        text-align: center;
                    !important;
                    }
                </style>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            {{--<th class="centro" align="center">
                                <input type="checkbox" name="todos"
                                       onclick="$('.marcado').prop('checked',$(this).prop('checked'));calculatotal();">
                            </th>--}}
                            <th>Fecha</th>
                            <th>Monto</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_t = 0 ?>
                        @foreach($pagos_sr as $pago)

                            <tr>

                                <td>{!! $pago->lafecha !!}</td>
                                <td>
                                    {!! $pago->monto_total !!}
                                    <input type="hidden" name="pagos[{!! $pago->lafecha !!}][valor]" class="marcado"
                                           value="{!! $pago->monto_total !!}">
                                </td>
                            </tr>
                            <?php $total_t += $pago->monto_total?>
                        @endforeach
                        </tbody>
                    </table>
                    <br>
                    <div class="row">
                        <div class="form-group col-sm-2">
                            Monto a entregado
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::number('salida_real', $total_t, ['step' => 'any', 'class' => 'form-control','required', 'min' => 0,'max' => $total_t,'id' => 'idsalreal']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::select('ruser_id',$usuarios,null,array('class' => 'form-control','placeholder' => 'Seleccione el que entrega','required')) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-2">
                            Deuda
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::number('salida_deuda', 0, ['type' => 'number','step' => 'any', 'class' => 'form-control','required', 'min' => 0,'id' => 'idsaldeuda','readonly']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::button('<b>RETIRAR</b>', ['type' => 'submit', 'class' => 'btn btn-success btn-block', 'style' => 'color: white;']) !!}
                        </div>
                    </div>
                </div>
            {!! Form::hidden('caja_id',$caja->id,array('id' => 'idcaja')) !!}
            {!! Form::hidden('salida',$total_t,array('id' => 'idsalida')) !!}
            {!! Form::close() !!}
            <!-- /.box-body -->
                <br>
                <h4 class="text-center">Detalle de los movimientos de caja</h4>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>Monto</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total_det = 0; ?>
                    @foreach($flujos_det as $fludet)
                        <?php $total_det += $fludet->monto_total ?>
                        <tr>
                            <td>{!! $fludet->lafecha !!}</td>
                            <td>{!! $fludet->usuario !!}</td>
                            <td>{!! $fludet->monto_total !!}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td>TOTAL:</td>
                        <td><b>{!! $total_det !!}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scriptsextras')
    <script>
        var suma_to = {!! $total_t !!};
        $('#total_mar').html(suma_to);

        $('#idsalreal').on('keyup', function () {
            restacal();
        });

        function restacal() {
            $('#idsaldeuda').val(Math.round((suma_to - $('#idsalreal').val()) * 100) / 100);
        }
    </script>
@endpush