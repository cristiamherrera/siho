@extends('layouts.app')

@section('content')
    <h1 class="pull-left">PAGOS DE {!! strtoupper($caja->nombre) !!}</h1>

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                {!! Form::open(['route' => ['retirarpagos'],'id' => 'form-habit']) !!}
                <div class="box-header">
                    <h3 class="box-title">Ingresos no retirados a detalle <b>Marcados total: <span id="total_mar">0</span> Bs</b>
                    </h3>

                    <div class="box-tools pull-right">
                        <a href="{!! route('soloingresosdia',[$caja->id]) !!}" class="btn btn-info btn-box-tool"
                           style="color: white;"><b>RETIRAR X DIA</b></a>
                        <a href="{!! route('flujos',[$caja->id]) !!}" class="btn btn-primary btn-box-tool"
                        style="color: yellow;"><b>IR A CAJA</b></a>
                        <a href="{!! route('ultimosretiros',[$caja->id]) !!}" class="btn btn-info btn-box-tool"
                           style="color: yellow;"><b>ULTIMOS RETIROS</b></a>

                        {!! Form::button('<b>RETIRAR</b>', ['type' => 'submit', 'class' => 'btn btn-success btn-box-tool', 'style' => 'color: white;','onclick' => "return confirm('Esta seguro de registrar los pagos como retirados?')"]) !!}
                    </div>
                </div>
                <!-- /.box-header -->
                <style>
                    th.centro{
                        text-align: center; !important;
                    }
                </style>
                <div class="box-body no-padding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="centro" align="center">
                                <input type="checkbox" name="todos"
                                       onclick="$('.marcado').prop('checked',$(this).prop('checked'));calculatotal();">
                            </th>
                            <th>Fecha</th>
                            <th>Habitacion</th>
                            <th>Tipo</th>
                            <th>Piso</th>
                            <th>Grupo</th>
                            <th>Monto</th>
                        </tr>
                        </thead>
                        <tbody>                        
                        @foreach($pagos_sr as $pago)
                            <tr>
                                <td align="center">
                                    <input type="checkbox" name="pagos[{!! $pago->id !!}][valor]" class="marcado"
                                           value="{!! $pago->monto_total !!}">
                                </td>
                                <td>{!! $pago->fecha2 !!}</td>
                                <td>{!! $pago->registro->habitacione->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->categoria->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->rpiso->nombre !!}</td>
                                <td>{!! $pago->registro->grupo->nombre !!}</td>
                                <td>{!! $pago->monto_total !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            {!! Form::select('ruser_id',$usuarios,null,array('class' => 'form-control','placeholder' => 'Seleccione el que entrega','required')) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::button('<b>RETIRAR</b>', ['type' => 'submit', 'class' => 'btn btn-success btn-block', 'style' => 'color: white;','onclick' => "return confirm('Esta seguro de registrar los pagos como retirados?')"]) !!}
                        </div>
                    </div>
                </div>
            {!! Form::hidden('caja_id',$caja->id,array('id' => 'idcaja')) !!}
            {!! Form::hidden('salida',null,array('id' => 'idsalida')) !!}
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@push('scriptsextras')
<script>
    $('.marcado').click(function () {
        calculatotal();
    });

    function calculatotal() {
        var suma_to = 0.00;
        $('.marcado').each(function (es, elem) {
            if ($(elem).prop('checked')) {
                suma_to = suma_to + parseFloat($(elem).val());
            }
        });
        $('#total_mar').html(suma_to);
        $('#idsalida').val(suma_to);
    }
</script>
@endpush