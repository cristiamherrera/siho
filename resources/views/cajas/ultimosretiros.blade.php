@extends('layouts.app')

@section('content')
    <h1 class="pull-left">PAGOS DE {!! strtoupper($caja->nombre) !!}</h1>

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">ULTIMOS RETIROS</h3>

                    <div class="box-tools pull-right">
                        <a href="{!! route('flujos',[$caja->id]) !!}" class="btn btn-primary btn-box-tool" style="color: yellow;"><b>IR A CAJA</b></a>
                        <a href="{!! route('soloingresos',[$caja->id]) !!}" class="btn btn-success btn-box-tool" style="color: yellow;"><b>IR A RETIRAR</b></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-bordered" id="tabla">
                        <thead>
                        <tr>
                            <th>ID-EGRESO</th>
                            <th>Fecha Retiro</th>
                            <th>Fecha Pago</th>
                            <th>Habitacion</th>
                            <th>Tipo</th>
                            <th>Piso</th>
                            <th>Grupo</th>
                            <th>Monto</th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th>ID-EGRESO</th>
                            <th>Fecha Retiro</th>
                            <th>Fecha Pago</th>
                            <th>Habitacion</th>
                            <th>Tipo</th>
                            <th>Piso</th>
                            <th>Grupo</th>
                            <th>Monto</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pagos_cr as $pago)
                            <tr>
                                <td>{!! $pago->retiro->id !!}</td>
                                <td>{!! $pago->retiro->created_at !!}</td>
                                <td>{!! $pago->fecha2 !!}</td>
                                <td>{!! $pago->registro->habitacione->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->categoria->nombre !!}</td>
                                <td>{!! $pago->registro->habitacione->rpiso->nombre !!}</td>
                                <td>{!! $pago->registro->grupo->nombre !!}</td>
                                <td>{!! $pago->monto_total !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@push('scriptsextras')
@include('layouts.partials.jsdatatable')
@endpush