@extends('layouts.app')

@section('content')
    <h1 class="pull-left">{!! $caja->nombre !!} <span style="font-size: 17px; font-weight: bold;" class="text-primary">(SALDO: {!! $caja->total !!}
            ) Bs.</span></h1>

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <?php
    $role = Auth::user()->rol;
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Flujos de caja</h3>

                    <div class="box-tools pull-right">
{{--                        @if($role != 'Operario' )--}}
                            <a href="{!! url('soloingresosdia',[$caja->id]) !!}" class="btn btn-info btn-box-tool"
                               style="color: white;"><b>ENTREGA</b></a>
                        {{--@endif--}}

                        {{--<a href="{!! route('ultimosretiros',[$caja->id]) !!}" class="btn btn-info btn-box-tool"
                           style="color: yellow;"><b>ULTIMOS RETIROS</b></a>--}}
                        <a href="javascript:" class="btn btn-success btn-box-tool" style="color: yellow;"
                           onclick="cargarmodal('{!!url('caja/ingreso',[$caja->id])!!}','success')"><b>NUEVO
                                INGRESO</b></a>
                        <a href="javascript:" class="btn btn-primary btn-box-tool" style="color: yellow;"
                           onclick="cargarmodal('{!!url('caja/egreso',[$caja->id])!!}','primary')"><b>NUEVO
                                EGRESO</b></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-bordered" id="flujos-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha</th>
                            <th>Usuario</th>
                            <th>Detalle</th>
                            <th>Ingreso</th>
                            <th>Salida</th>
                            <th></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha</th>
                            <th>Usuario</th>
                            <th>Detalle</th>
                            <th>Ingreso</th>
                            <th>Salida</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection
@push('scriptsextras')
    <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script>

        var table = null;
        $(function () {
            table = $('#flujos-table').DataTable({
                processing: true,
                serverSide: true,
                'order': [],
                "bSort": false,
                ajax: '{!! route('datatables.flujos',$caja->id) !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'created_at', name: "created_at"},
                    {data: 'usuario', name: 'usuario'},
                    {data: 'detalle', name: 'detalle'},
                    {data: 'ingreso', name: 'ingreso'},
                    {data: 'salida', name: 'salida'},
                    {data: 'acciones', name: 'acciones',searchable: false}

                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    if (parseFloat(aData['tdeudas']) > 0) {
                        $('td', nRow).addClass('info');
                    }

                    var ac_ver = '';
                    if (parseFloat(aData['estado']) != 'Desocupado') {
                        ac_ver = '<a class="btn btn-info btn-sm" href="{!! url('registrosgrupos') !!}/'+aData['grupo_id']+'"><i class="fa fa-eye"></i></a>&nbsp;';
                    }
                    var confirm_a = "if(confirm('Esta seguro de cancelar el registro??')){window.location.href = '{!! url('cancelaregistro') !!}/"+aData['id']+"';}";
                    var ac_eliminar = '<a href="javascript:" class="btn btn-danger btn-sm" style="color: white;" onclick="'+confirm_a+'" title="CANCELAR REGISTRO"><i class="fa fa-times"></i></a>';
                    $('td:eq(9)', nRow).html(ac_ver+ac_eliminar);


                },
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
            $('#flujos-table thead:eq( 0 ) th').each(function () {
                var title = $('#flujos-table thead:eq( 0 ) th').eq($(this).index()).text();
                if (title != '') {
                    $(this).html('<input type="text" style="width: 100%;" placeholder="' + title + '" />');
                }
            });
            table.columns().eq(0).each(function (colIdx) {
                $('input', table.column(colIdx).header()).on('keyup change', function () {
                    table
                        .column(colIdx)
                        .search(this.value)
                        .draw();
                });
            });
        });

        function ondetalle(idFlujo) {
            cargarmodal('{!! url('detalleflujo') !!}/'+idFlujo,'primary');
        }

        function onfacturar(idFlujo){
            cargarmodal('{!! url('facturar') !!}/'+idFlujo)
        }

        function onverfactura(idFactura) {
            window.location.href = '{!! url('factura') !!}/'+idFactura;
        }

        function onrecibo(idFlujo) {
            window.location.href = '{!! url('recibo_retiro') !!}/'+idFlujo;
        }
        function onreciboxdia(idFlujo) {
            window.location.href = '{!! url('recibo_retiroxdia') !!}/'+idFlujo;
        }

        function oneliminar(idFlujo) {
            cargarmodal('{!! url('caja/eliminaflujo') !!}/'+idFlujo,'danger');
        }
    </script>
@endpush
