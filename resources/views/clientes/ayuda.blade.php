<a 
	href="#" 
	class="btn btn-info btn-sm"
><i class="fa fa-pencil"></i> Editar Cliente</a>

<a 
	href="#" 
	title="Registrar habitacion" 
	class="btn btn-success btn-sm"
><i class="fa fa-hotel"></i> Asignar Habitacion</a>

<a 
	href="#" 
	title="Registros de Clientes" 
	class="btn btn-primary btn-sm"
><i class="fa fa-list"></i> Historico de Clientes</a>

<button 
	class="btn btn-danger btn-sm" 
><i class="glyphicon glyphicon-trash"></i> Eliminar Cliente</button>

<a 
	href="#" 
	class="btn btn-warning btn-sm" 
	title="VER ADJUNTOS"
><i class="fa fa-clipboard"></i> Ver Documentos</a>