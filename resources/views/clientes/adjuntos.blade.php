<?php
function escoge($adjun)
{
    $array_f = explode('.', $adjun);
    $extension = end($array_f);
    $extension = strtoupper($extension);
    if((in_array($extension, array('JPEG','JPG','PNG')))){
        return $adjun;
    }elseif ($extension == 'PDF'){
        return "../img/logopdf.png";
    }else{
        return "../img/logofile.png";
    }
}
?>
{!! Form::model($cliente, ['route' => ['guarda_s_adjunto',$cliente->id], 'method' => 'post', 'files'=>true,'enctype' => 'multipart/form-data','id' => 'form-cliente']) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span>
    </button>
    <h4 class="modal-title">Archivos adjuntos del cliente</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><b>CLIENTE:</b></td>
                        <td>{!! $cliente->nombre !!}</td>
                    </tr>
                    <tr>
                        <td><b>CI/PASAPORTE:</b></td>
                        <td>
                            @if(!empty($cliente->ci))
                                {!! $cliente->ci !!}
                            @elseif(!empty($cliente->pasaporte))
                                {!! $cliente->pasaporte !!}
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="form-group" >
        <div class="row">
            <div class="col-md-8">
                <div class="row" id="adjunto-1">
                    <div class="col-md-4 text-right">
                        <b>ARCHIVO ADJUNTO 1</b>
                    </div>
                    <div class="col-md-8">
                        {!! Form::file('archivos[1][datos]') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-success btn-sm" onclick="adicionar_a();"><i class="fa fa-plus"></i>
                    Adicionar
                </button>
                <button type="button" class="btn btn-danger btn-sm" onclick="quitar_a();"><i class="fa fa-minus"></i>
                    Quitar
                </button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                @if(!empty($cliente->adjuntos->all()))
                    <table class="table table-bordered">
                        @foreach($cliente->adjuntos as $adjunto)
                            @if(empty($adjunto->deleted_at))
                            <tr>
                                <td align="center" style="padding: 0px;">
                                    <?php $nombre_ar = escoge($adjunto->nombre);?>
                                            <img class="img-responsive pad" style="max-height: 80px;" src="{!! url('adjuntos/'.$nombre_ar) !!}" alt="Product Image">


                                </td>
                                <td style="vertical-align: middle;">{!! $adjunto->nombre_original !!}</td>

                                <td style="vertical-align: middle;">

                                    <a href="{!! url('adjuntos/'.$adjunto->nombre) !!}" target="_blank"
                                       class="btn btn-success btn-sm" title="VER ARCHIVO"><i
                                                class="fa fa-eye"></i></a>
                                    <a href="javascript:"
                                       onclick="if(confirm('Esta seguro de eliminar el archivo adjunto??')){eliminar_adj({!! $adjunto->id !!});}"
                                       class="btn btn-danger btn-sm" title="Eliminar adjunto"><i
                                                class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
                @else
                    <h4>NO TIENE ARCHIVOS ADJUNTOS!!!</h4>
                @endif
            </div>
        </div>
    </div>
</div>
{!! Form::hidden('ruser_id',Auth::user()->id) !!}
<div class="modal-footer">
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
</div>
{!! Form::close() !!}

<script>
    var n_adj = 1;
    function adicionar_a() {
        n_adj++;
        var cont_adj = $('#adjunto-1').html();
        $('#adjunto-' + (n_adj - 1)).after('<div class="row" id="adjunto-' + n_adj + '">' + cont_adj + '</div>');
        $('#adjunto-' + n_adj + ' div.text-right b').html('ARCHIVO ADJUNTO ' + n_adj);
        $('#adjunto-' + n_adj + ' input').attr('name', 'archivos[' + n_adj + '][datos]');
    }
    function quitar_a() {
        if (n_adj != 1) {
            $('#adjunto-' + n_adj).remove();
            n_adj--;
        }
    }
    $("#form-cliente").submit(function (e) {
        var postData = new FormData($(this)[0]);
        //console.log($(this));


        //var postData = $(this).serializeArray();
        //console.log(postData);
        //console.log(postData2);
        //e.preventDefault();

        var formURL = $(this).attr("action");
        $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    async: false,
                    processData: false,
                    cache: false,
                    contentType: false,

                    beforeSend: function (XMLHttpRequest) {
                        //alert("antes de enviar");
                        $('#divmodal').hide();
                        jQuery("#spin-cargando-mod").show(200);
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        //alert('despues de enviar');
                    },
                    success: function (data, textStatus, jqXHR) {
                        //console.log(data);
                        setTimeout(function () {
                            jQuery("#spin-cargando-mod").hide();
                            $('#divmodal').show();
                            if (data['m_error'] == '') {
                                $('#mimodal').attr('class', 'modal modal-success');
                                $('#divmodal h4.modal-title').html(data['m_bueno']);
                                $('#divmodal div.modal-body').hide();
                                $('#divmodal div.modal-footer').hide();
                                setTimeout(function () {
                                    cargarmodal('{!! route('adjuntos',[$cliente->id]) !!}', 'info', 'lg');
                                    $('#mimodal').attr('class', 'modal modal-info');
                                    $('#divmodal div.modal-body').show();
                                    $('#divmodal div.modal-footer').show();
                                }, 1900);
                            } else {
                                $('#mimodal').attr('class', 'modal modal-danger');
                                $('#divmodal h4.modal-title').html(data['m_error']);
                                $('#divmodal div.modal-body').hide();
                                $('#divmodal div.modal-footer').hide();
                                setTimeout(function () {
                                    //$('#divmodal h4.modal-title').html('Formulario de Cliente');
                                    $('#divmodal div.modal-body').show();
                                    $('#divmodal div.modal-footer').show();
                                    //$('#mimodal').modal('hide');
                                    //recargatabla();
                                }, 1900);
                            }


                        }, 1000);

                        //data: return data from server
                        //$("#parte").html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                        alert("error");
                    }
                });
        e.preventDefault(); //STOP default action
        //e.unbind(); //unbind. to stop multiple form submit.
    });
    @if(isset($cliente->id))
    function eliminar_adj(idAdjunto) {
        $.ajax(
                {
                    url: '{!! url('elimina_adjunto') !!}/' + idAdjunto,
                    type: "GET",
                    beforeSend: function (XMLHttpRequest) {
                        //alert("antes de enviar");
                        $('#divmodal').hide();
                        jQuery("#spin-cargando-mod").show(200);
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                        //alert('despues de enviar');
                    },
                    success: function (data, textStatus, jqXHR) {
                        //console.log(data);
                        setTimeout(function () {
                            jQuery("#spin-cargando-mod").hide();
                            $('#divmodal').show();
                            if (data['m_error'] == '') {
                                $('#mimodal').attr('class', 'modal modal-success');
                                $('#divmodal h4.modal-title').html(data['m_bueno']);
                                $('#divmodal div.modal-body').hide();
                                $('#divmodal div.modal-footer').hide();
                                setTimeout(function () {
                                    cargarmodal('{!! route('adjuntos',[$cliente->id]) !!}', 'info', 'lg');
                                    $('#mimodal').attr('class', 'modal modal-info');
                                    $('#divmodal div.modal-body').show();
                                    $('#divmodal div.modal-footer').show();
                                }, 1900);
                            } else {
                                $('#mimodal').attr('class', 'modal modal-danger');
                                $('#divmodal h4.modal-title').html(data['m_error']);
                                $('#divmodal div.modal-body').hide();
                                $('#divmodal div.modal-footer').hide();
                                setTimeout(function () {
                                    //$('#divmodal h4.modal-title').html('Formulario de Cliente');
                                    $('#divmodal div.modal-body').show();
                                    $('#divmodal div.modal-footer').show();
                                    //$('#mimodal').modal('hide');
                                    //recargatabla();
                                }, 1900);
                            }


                        }, 1000);

                        //data: return data from server
                        //$("#parte").html(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                        alert("error");
                    }
                });
    }
    @endif
</script>