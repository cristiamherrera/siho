<html>
<head>
    <title>INFORME DE PAGOS DE GRUPO</title>
    <style>

        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 100%;
            border: 1px solid #000000;

            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;

            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;

            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;

            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .CSSTableGenerator table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
        }

        .CSSTableGenerator tr:last-child td:last-child {
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .CSSTableGenerator table tr:first-child td:first-child {
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

        .CSSTableGenerator table tr:first-child td:last-child {
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
        }

        .CSSTableGenerator tr:last-child td:first-child {
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .CSSTableGenerator tr:hover td {
            background-color: #ffffff;

        }

        .CSSTableGenerator td {
            vertical-align: middle;

            background-color: #ffffff;

            border: 1px solid #000000;
            border-width: 0px 1px 1px 0px;
            padding: 3px;
            font-size: 10px;
            font-family: Arial;
            color: #000000;
        }

        .CSSTableGenerator tr:last-child td {
            border-width: 0px 1px 0px 0px;
        }

        .CSSTableGenerator tr td:last-child {
            border-width: 0px 0px 1px 0px;
        }

        .CSSTableGenerator tr:last-child td:last-child {
            border-width: 0px 0px 0px 0px;
        }
    </style>
</head>
<body
        @if(isset($logo->descripcion))
        style="background: url('{!! url('adjuntos/'.$logo->descripcion) !!}') no-repeat;background-size: auto 70px;background-attachment: fixed;background-position: top right; "
        @endif
>
<br>
<br>
<h3>INFORME DE PAGOS DEL GRUPO {!! strtoupper($grupo->nombre) !!}</h3>

@if(!empty($pagos_recibidos->all()))
    <p>
        En la siguiente tabla se presenta el listado de pagos recibidos por parte del grupo:
    </p>
    <table class="CSSTableGenerator">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Habitacion</th>
            <th>Tipo</th>
            <th>Piso</th>
            <th>Descripcion</th>
            <th>Monto Bs</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $total_p = 0.00;
        ?>
        @foreach($pagos_recibidos as $pago)
            <?php
            $total_p = $total_p + $pago->monto_total;
            ?>
            <tr>
                <td>{!! $pago->fecha2 !!}</td>
                <td>{!! $pago->registro->habitacione->nombre !!}</td>
                <td>{!! $pago->registro->habitacione->categoria->nombre !!}</td>
                <td>{!! $pago->registro->habitacione->rpiso->nombre !!}</td>
                <td>{!! $pago->observacion !!}</td>
                <td>{!! $pago->monto_total !!}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>TOTAL</b></td>
            <td><b>{!! $total_p !!}</b></td>
        </tr>
        </tbody>
    </table>
@else
    <p>
        SE INFORMA QUE NO SE HA RECIBIDO NINGUN PAGO POR PARTE DEL GRUPO.
    </p>
@endif

@if(!empty($pagos_pendientes->all()))
    <p>
        En la siguiente tabla se presenta el listado de pagos pendientes que tiene el grupo:
    </p>
    <table class="CSSTableGenerator">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Habitacion</th>
            <th>Tipo</th>
            <th>Piso</th>
            <th>Descripcion</th>
            <th>Monto Bs</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $total_p = 0.00;
        ?>
        @foreach($pagos_pendientes as $pago)
            <?php
            $total_p = $total_p + $pago->monto_total;
            ?>
            <tr>
                <td>{!! $pago->fecha2 !!}</td>
                <td>{!! $pago->registro->habitacione->nombre !!}</td>
                <td>{!! $pago->registro->habitacione->categoria->nombre !!}</td>
                <td>{!! $pago->registro->habitacione->rpiso->nombre !!}</td>
                <td>{!! $pago->observacion !!}</td>
                <td>{!! $pago->monto_total !!}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>TOTAL</b></td>
            <td><b>{!! $total_p !!}</b></td>
        </tr>
        </tbody>
    </table>
@else
    <p>
        SE INFORMA QUE EL GRUPO NO TIENE NINGUN PAGO NI DEUDA PENDIENTE.
    </p>
@endif

<script>
    //window.print();
</script>
</body>
</html>