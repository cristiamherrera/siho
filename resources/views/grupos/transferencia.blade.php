<link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
<style>
    .datepicker {
        z-index: 9999 !important;
    }
</style>
{!! Form::open(['route' => ['guarda_transferencia']]) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">TRANSFERENCIA
        - {!! $registro->habitacione->nombre.'-'.$registro->habitacione->rpiso->nombre !!}</h4>
</div>

<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Seleccione la habitacion a transferir</label>

                    <select name="habitacione_id" class="form-control" id="idhabitacions">
                        <option value="">Seleccione la habitacion</option>
                        @foreach($habitaciones as $habitacion)
                            @if(!$habitacion->estaocupado && $habitacion['estado'] == 'Habilitado')
                                <option value="{!! $habitacion->id !!}">{!! $habitacion->nombre.' - '.$habitacion->rpiso->nombre.' - '.$habitacion->categoria->nombre !!}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group" id="divselect-precio">
                    <label>Seleccione el precio</label>
                    {!! Form::select('precio', [],null, ['class' => 'form-control','required']) !!}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal-footer">
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
</div>
{!! Form::hidden('registro_id',$registro->id) !!}
{!! Form::close() !!}

<script>
    $('#idhabitacions').change(function () {
        $('#divselect-precio').load('{!! url('ajaxpreciosr') !!}/' + $(this).val());
    });
</script>
