@extends('layouts.app')

@section('content')
    <h1 class="pull-left">
        Grupo: {!! $grupo->nombre !!}        
    </h1>
    <a 
        class="btn btn-info pull-right" 
        style="margin-top: 25px"
        href="{!! route('informe_pago_grupo', [$grupo->id]) !!}" 
        target="_blank">
        <i class="fa fa-print"></i> Informe de Pagos
    </a>
    <a class="btn btn-primary pull-right" style="margin-top: 25px"
       href="{!! route('asignahabitacion2', ['Grupo',$grupo->id]) !!}"><i class="fa fa-plus"></i> Registro</a>
    <a class="btn btn-success pull-right" style="margin-top: 25px"
       onclick="cargarmodal('{!! route('grupo',[$grupo->id]) !!}');"><i class="fa fa-edit"></i> Editar</a>
    <div class="div-pagos">
        <a 
            class="btn btn-warning pull-right" 
            style="margin-top: 25px; font-weight: bold;"
            onclick="mostrarclientes();"><i class="fa fa-user"></i> CAMBIAR RESPONSABLE</a>
    </div>
    <div class="div-clientes" style="display: none;">
        <a class="btn btn-default pull-right" style="margin-top: 25px;font-weight: bold;"
           onclick="mostrarpagos();"> MOSTRAR PAGOS</a>
    </div>

    <div class="clearfix"></div>

    <h2>RESPONSABLE:
        @if(isset($grupo->responsable->nombre))
            {!! $grupo->responsable->nombre !!}
            @if(!empty($grupo->responsable->ci))
                CI: {!! $grupo->responsable->ci !!}
            @else
                PASAPORTE: {!! $grupo->responsable->pasaporte !!}
            @endif
        @else
            <span style="color: red;">SIN RESPONSABLE! </span>
        @endif
    </h2>
        <a href="#" class="btn bg-navy btn-flat btn-sm" title="IMPRIMIR"><i class="fa fa-print"></i> Imprimir Ocupantes</a>
        <a href="#" style="color: white;" title="ADICIONAR PAGO" class="btn btn-success btn-sm"><i class="fa fa-money"></i> Adicionar Deuda</a>
        <a href="#" title="EDITAR" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> Editar Datos</a>
        <a href="#" title="MARCAR SALIDA" class="btn btn-primary btn-sm" style="color: white;"><i class="fa fa-sign-out"></i> Generar Salida</a>
        <a href="#" title="TRANSFERIR" class="btn btn-warning btn-sm" style="color: white;"><i class="fa  fa-exchange"></i> Tranferir Habitacion</a>

    @include('flash::message')

    <div class="row">
        <div class="col-md-8">
            @foreach($registros as $registro)
                <?php
                $color = 'primary';
                $color2 = 'primary';
                if ($registro->estado == 'Reservado') {
                    $color = 'warning';
                    $color2 = 'warning';
                } elseif ($registro->estado == 'Salida') {
                    $color = 'success';
                    $color2 = 'success';
                } elseif ($registro->estado == 'Cancelado') {
                    $color = 'danger';
                    $color2 = 'danger';
                }

                ?>
                <div class="box box-{!! $color2 !!}">
                    <div class="box-header">
                        <h3 class="box-title">
                            <button type="button" class="btn btn-block btn-{!! $color !!}">
                                <b style="font-size: 16pt;">
                                    {!! $registro->habitacione->nombre.' - '.$registro->habitacione->rpiso->nombre !!} => 
                                    {!! $registro->estado !!}
                                </b>    
                            </button>
                        </h3>
                        <div class="box-tools pull-right">
                            @if($registro->estado != 'Salida' && $registro->estado != 'Cancelado')
                                <a href="{!!route('imp_registro',[$registro->id])!!}" target="_blank"
                                   class="btn bg-navy btn-flat btn-lg" title="IMPRIMIR"><i
                                            class="fa fa-print"></i></a>
                                <a href="javascript:" style="color: white;"
                                   onclick="cargarmodal('{!! route('addpagoextra',[$registro->id]) !!}')"
                                   title="ADICIONAR PAGO"
                                   class="btn btn-success btn-lg"><i
                                            class="fa fa-money"></i></a>
                                <a href="{!!route('nuevoregistro',['Grupo',$grupo->id,$registro->habitacione->id,$registro->id])!!}"
                                   title="EDITAR"
                                   class="btn btn-default btn-lg"><i class="fa fa-edit"></i></a>

                                <a href="javascript:"
                                   onclick="if(confirm('Esta seguro que desea marcar la salida de este registro??')){window.location.href = '{!! route('marcasalida',[$registro->id]) !!}';}"
                                   title="MARCAR SALIDA"
                                   class="btn btn-primary btn-lg" style="color: white;"><i
                                            class="fa fa-sign-out"></i></a>
                                <a href="javascript:"
                                   onclick="if(confirm('Esta seguro que desea transferir de habitacion??')){cargarmodal('{!! route('transferencia',[$registro->id]) !!}');}"
                                   title="TRANSFERIR"
                                   class="btn btn-warning btn-lg" style="color: white;"><i
                                            class="fa  fa-exchange"></i></a>
                                <?php
                                $role = Auth::user()->rol;
                                ?>
                                @if($role != 'Operario')
                                    <a href="javascript:" class="btn btn-danger btn-lg" style="color: white;"
                                       onclick="if(confirm('Esta seguro de cancelar el registro??')){window.location.href = '{!! route('cancelaregistro',[$registro->id]) !!}';}"
                                       title="CANCELAR REGISTRO"><i class="fa fa-times"></i></a>
                                @else
                                    <?php
                                    $selectedTime = $registro->created_at;
                                    $tiempo_act = strtotime("+$tiempo_cancelar minutes", strtotime($selectedTime));
                                    $tiempo_act = date('Y-m-d H:i:s', $tiempo_act);
                                    ?>
                                    @if($tiempo_act >= date('Y-m-d H:i:s'))
                                        <a href="javascript:" class="btn btn-danger btn-lg" style="color: white;"
                                           onclick="if(confirm('Esta seguro de cancelar el registro??')){window.location.href = '{!! route('cancelaregistro',[$registro->id]) !!}';}"
                                           title="CANCELAR REGISTRO"><i class="fa fa-times"></i></a>
                                    @elseif($registro->estado == 'Reservado')
                                        <a href="javascript:" class="btn btn-danger btn-lg" style="color: white;"
                                           onclick="if(confirm('Esta seguro de cancelar el registro??')){window.location.href = '{!! route('cancelaregistro',[$registro->id]) !!}';}"
                                           title="CANCELAR REGISTRO"><i class="fa fa-times"></i></a>
                                    @endif
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-{!! $color2 !!} text-bold">Fecha Ingreso</td>
                                <td>{!! $registro->fecha_ingreso2 !!}</td>
                                <td class="text-{!! $color2 !!} text-bold">Fecha Salida</td>
                                <td>{!! $registro->fecha_salida !!}</td>
                            </tr>
                            <tr>
                                <td class="text-{!! $color2 !!} text-bold">Fecha inicial Reserva</td>
                                <td>{!! $registro->fech_ini_reserva2 !!}</td>
                                <td class="text-{!! $color2 !!} text-bold">Fecha final Reserva</td>
                                <td>{!! $registro->fech_fin_reserva2 !!}</td>
                            </tr>
                            <tr>
                                <td class="text-{!! $color2 !!} text-bold">Equipaje</td>
                                <td>{!! $registro->equipaje !!}</td>
                                <td class="text-{!! $color2 !!} text-bold">Precio</td>
                                <td>{!! $registro->precio.' Bs.' !!}</td>
                            </tr>
                        </table>
                        <h4 class="text-center">Listado de Huespedes</h4>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Pasaporte</th>
                                <th>C.I.</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($registro->hospedantes as $hospedante)
                                <tr>
                                    <td>{!! $hospedante->cliente->nombre !!}</td>
                                    <td>{!! $hospedante->cliente->pasaporte !!}</td>
                                    <td>{!! $hospedante->cliente->ci !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-4">
            <div class="div-pagos">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Pagos Pendientes</h3>
                        <div class="box-tools pull-right">
                            <a href="{!!route('generaadelantosgrupos',[$grupo->id])!!}" title="Generar pagos de Reserva"
                               class="btn btn-default btn-box-tool"><b>
                                    <i class="fa fa-refresh"></i></b></a>
                        </div>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => ['registrapagosg']]) !!}
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    {!! Form::checkbox('todos', 'value',null,['id' => 'c-todos-p'])!!}
                                </th>
                                <th>Habitacion</th>
                                <th>Fecha</th>
                                <th>Monto</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_p_p = 0.00;
                            ?>
                            @foreach($pagos_pendientes as $pago)
                                <?php $total_p_p = $total_p_p + $pago->monto_total;?>
                                @if($pago->estado == 'Deuda Extra')
                                    <tr class="info">
                                @else
                                    <tr>
                                        @endif

                                        <td>
                                            {!! Form::checkbox("pagos[".$pago->id."][marcado]", $pago->estado,null,['class' => 'c-todos-pt'])!!}

                                        </td>
                                        <td>
                                            <b style="font-size: 12pt;">{!! $pago->registro->habitacione->nombre !!}</b>
                                            @if($pago->estado == 'Deuda Extra')
                                                {{ ($pago->observacion) }}
                                            @else
                                            @endif
                                        </td>
                                        <td>{!! $pago->fecha !!}</td>
                                        <td>{!! $pago->monto_total !!}</td>
                                        <td>
                                            <?php
                                            $role = Auth::user()->rol;
                                            ?>
                                            @if($role != 'Operario')
                                                <a href="javascript:"
                                                   onclick="cargarmodal('{!! route('eliminapago',[$pago->id]) !!}','danger')"
                                                   class='btn btn-danger btn-xs' title="Eliminar Pago"><i
                                                            class="fa fa-remove"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><b>TOTAL</b></td>
                                        <td><b>{!! $total_p_p.' Bs.' !!}</b></td>
                                        <td></td>
                                    </tr>
                            </tbody>
                        </table>
                        @push('scriptsextras')
                        <script>
                            $('#c-todos-p').click(function () {
                                $('.c-todos-pt').prop('checked', $('#c-todos-p').prop('checked'));
                            });
                        </script>
                        @endpush
                        <br>

                        <div class="form-group col-sm-12">
                            {!! Form::select('caja_id', $cajas,null, ['class' => 'form-control','required']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::button('<i class="fa fa-save"></i> Registrar', ['type' => 'submit', 'class' => 'btn btn-primary', 'onclick' => "return confirm('Esta seguro de realizar el pago?')"]) !!}
                        </div>
                        {!! Form::hidden('grupo_id',$grupo->id) !!}
                        {!! Form::hidden('user_id',Auth::user()->id) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Pagos Recibidos</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Modificado</th>
                                <th>Habitacion</th>
                                <th>Fecha</th>
                                <th>Monto</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_p_r = 0.00;
                            ?>
                            @foreach($pagos_recibidos as $pago)
                                <?php $total_p_r = $total_p_r + $pago->monto_total;?>
                                @if($pago->estado == 'Deuda Extra')
                                    <tr class="info">
                                @else
                                    <tr>
                                        @endif
                                        <td>{!! $pago->modificado !!}</td>
                                        <td>{!! $pago->registro->habitacione->nombre !!}</td>
                                        <td>{!! $pago->fecha2 !!}</td>
                                        <td>{!! $pago->monto_total !!}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><b>TOTAL</b></td>
                                        <td><b>{!! $total_p_r.' Bs.' !!}</b></td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="div-clientes" style="display: none;">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Seleccion el Responsable</h3>

                        <div class="box-tools pull-right">
                            <a href="javascript:" class="btn btn-primary btn-box-tool" style="color: yellow;"
                               onclick="cargarmodal('{!!route('cliente')!!}','primary','lg')"><b>NUEVO
                                    CLIENTE</b></a>
                        </div>
                    </div>
                    <div class="box-body">
                        @include('clientes.tabla2')
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection


@push('scriptsextras')
<script>
    function mostrarclientes() {
        $('.div-clientes').show(400);
        $('.div-pagos').hide(400);
    }
    function mostrarpagos() {
        $('.div-pagos').show(400);
        $('.div-clientes').hide(400);
    }
</script>
@endpush
