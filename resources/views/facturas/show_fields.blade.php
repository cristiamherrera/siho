<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $factura->id !!}</p>
</div>

<!-- Codigo Control Field -->
<div class="form-group">
    {!! Form::label('codigo_control', 'Codigo Control:') !!}
    <p>{!! $factura->codigo_control !!}</p>
</div>

<!-- Cliente Field -->
<div class="form-group">
    {!! Form::label('cliente', 'Cliente:') !!}
    <p>{!! $factura->cliente !!}</p>
</div>

<!-- Nit Field -->
<div class="form-group">
    {!! Form::label('nit', 'Nit:') !!}
    <p>{!! $factura->nit !!}</p>
</div>

<!-- Nit P Field -->
<div class="form-group">
    {!! Form::label('nit_p', 'Nit P:') !!}
    <p>{!! $factura->nit_p !!}</p>
</div>

<!-- Importetotal Field -->
<div class="form-group">
    {!! Form::label('importetotal', 'Importetotal:') !!}
    <p>{!! $factura->importetotal !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $factura->fecha !!}</p>
</div>

<!-- Fecha Limite Field -->
<div class="form-group">
    {!! Form::label('fecha_limite', 'Fecha Limite:') !!}
    <p>{!! $factura->fecha_limite !!}</p>
</div>

<!-- Numero Field -->
<div class="form-group">
    {!! Form::label('numero', 'Numero:') !!}
    <p>{!! $factura->numero !!}</p>
</div>

<!-- Autorizacion Field -->
<div class="form-group">
    {!! Form::label('autorizacion', 'Autorizacion:') !!}
    <p>{!! $factura->autorizacion !!}</p>
</div>

<!-- Qr Field -->
<div class="form-group">
    {!! Form::label('qr', 'Qr:') !!}
    <p>{!! $factura->qr !!}</p>
</div>

<!-- Montoliteral Field -->
<div class="form-group">
    {!! Form::label('montoliteral', 'Montoliteral:') !!}
    <p>{!! $factura->montoliteral !!}</p>
</div>

<!-- Created Field -->
<div class="form-group">
    {!! Form::label('created', 'Created:') !!}
    <p>{!! $factura->created !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $factura->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $factura->updated_at !!}</p>
</div>

