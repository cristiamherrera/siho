<style>
    table.mitablar {
        width: 100%;
    }
</style>
{!! Form::open(['route' => 'generar_factura']) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Facturar</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::text('nit', $nit_ci, ['class' => 'form-control','placeholder' => 'NIT/CI','required','id' => 'cnit']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::text('cliente', $cliente, ['class' => 'form-control','placeholder' => 'Cliente','required','id' => 'ccliente']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if(empty($flujo->descripcion))
                    <table class="table table-cordered">
                        <thead>
                        <tr>
                            <th>Detalle</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {!! $flujo->detalle !!}
                            </td>
                            <td>
                                {!! $flujo->ingreso !!}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>{!! $flujo->ingreso !!}</td>
                        </tr>
                        </tbody>

                    </table>
                @else
                    {!! $flujo->descripcion !!}
                @endif
            </div>

        </div>

    </div>
</div>

{!! Form::hidden('flujo_id',$flujo->id) !!}
{!! Form::hidden('user_id',Auth::user()->id) !!}
<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Generar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::close() !!}


<script>
    $('#cnit').focusout(function () {

        $.ajax(
                {
                    url: '{!! url('get_cliente_fac') !!}/' + $('#cnit').val(),
                    type: "GET",
                    success: function (data, textStatus, jqXHR) {
                        $('#ccliente').val(data['cliente']);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                        alert("error");
                    }
                });
    });

</script>