{!! Form::open(['route' => ['guarda_salida_insumo']]) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Salida de "{!! $insumo->nombre !!}" ({{ $total->cantidad }})</h4>
</div>

<div class="modal-body">
    <div class="form-group">

        @if(!empty($idHotel))
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tipo de Salida:</label>
                        {!! Form::select('tipo', ['Salida' => 'Salida','Salida a Central' => 'Salida a Central'],null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

        @else
            {!! Form::hidden('tipo','Salida') !!}
        @endif
        {!! Form::hidden('hotel_id',$idHotel) !!}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Cantidad:</label>
                    {!! Form::number('cantidad', null, ['class' => 'form-control','placeholder' => 'Ej: 15','step' => 'any','required']) !!}
                </div>
            </div>
            @if (!empty($operarios))
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Asignado:</label>
                        {!! Form::select('operarios', $operarios, null, ['placeholder'=>'Seleccione uno', 'class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Pisos:</label>
                        {!! Form::select('pisos', $pisos, null, ['placeholder'=>'Seleccione uno', 'class' => 'form-control']) !!}
                    </div>
                </div>
            @else

            @endif
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('insumo_id',$insumo->id) !!}
{{--{!! Form::hidden('medida',$insumo->medida) !!}--}}
{!! Form::close() !!}


<script>
    $('#sel-medida').change(function () {
        if ($(this).val() == 'Ingreso a Hotel') {
            $('#camp-hoteles').show(400);
        } else {
            $('#camp-hoteles').hide(400);
        }
    });
</script>
