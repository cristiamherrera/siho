@extends('layouts.app')

@section('content')

    <div class="clearfix"></div>

    @include('flash::message')
    <h1 class="pull-left">
        <div class="btn">
            <a href="#" class="btn btn-success btn-sm" title="Ingresar">
                <i class="fa fa-arrow-down"></i> / 
                <i class="fa fa-arrow-right"><b style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif"> Ingresar o Transferir Productos</b></i>
            </a>
            <a href="#" class="btn btn-info btn-sm" title="Sacar">
                <i class="fa fa-arrows"><b style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif"> Sacar o Asignar Producto</b></i>
            </a>
            <a href="#" class="btn btn-warning btn-sm" title="Movimientos">
                <i class="fa fa-navicon"><b style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif"> Movimientos de un Producto</b></i>
            </a>
           <a href="#" class="btn btn-default btn-sm">
                <i class="glyphicon glyphicon-edit"><b style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif"> Editar Producto</b></i>
            </a>
            <a href="#" class="btn btn-danger btn-sm">
                <i class="fa fa-remove"><b style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif"> Eliminar Producto</b></i>
            </a>
        </div>
    </h1>
    <div class="clearfix"></div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Listado de Insumos
                <?php
                if (isset($hotel->nombre)) {
                    echo ' - <b>' . $hotel->nombre . '</b>';
                }else{
                    echo ' - <b>ALMACEN CENTRAL</b>';
                }
                ?>
            </h3>
            <?php
            $mihotel_id = Auth::user()->hotel_id;
            $mi_rol = Auth::user()->rol;
            ?>
            @if($mihotel_id == null && $mi_rol == 'Super Administrador' || $mi_rol == 'Administrador')
                <div class="box-tools pull-right no-imprimir">
                    <button class="btn btn-box-tool btn-primary"
                            onclick="window.location.href = '{!! route('insumos') !!}';"
                            style="color: #ffffff;" title="CENTRAL">
                        <b>ALMACEN CENTRAL</b>
                    </button>
                    @if ($mi_rol == 'Super Administrador')
                        @foreach($hoteles as $numHot => $hotel)
                            <button class="btn btn-box-tool btn-primary"
                                    onclick="window.location.href = '{!! route('insumos',[$hotel->id]) !!}';"
                                    style="color: #ffffff;" title="{!! $hotel->nombre !!}">
                                <b> {!! $hotel->nombre !!}</b>
                            </button>
                        @endforeach
                    @endif
                    &nbsp;&nbsp;
                    <button class="btn btn-box-tool btn-success" title="Nuevo Usuario"
                            onclick="cargarmodal('{!! route('insumo') !!}')" style="color: #ffffff;">
                            <i class="fa fa-plus-square"></i>
                            <b>NUEVO PRODUCTO</b>
                    </button>
                </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-responsive table-bordered" id="tabla">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Nombre</th>
                    <th>Medida</th>
                    <th>Categoria</th>
                    <th>Stok Almacen</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($insumos as $insumo)
                    <tr>
                        <td>{!! $insumo->id !!}</td>
                        <td>{!! $insumo->nombre !!}</td>
                        <td>{!! $insumo->medida !!}</td>
                        <td>{!! $insumo->categoria !!}</td>
                        <td>
                            <?php
                            echo $total_g = $insumo->getTotal($idHotel);
                            //echo $total_g . ' (' . ($total_g / $insumo->cantidad) . ')';
                            ?>
                            {{--@if(isset($insumo->total->cantidad))
                                {!! $insumo->total->cantidad !!}
                            @else
                                0
                            @endif--}}
                        </td>
                        <td>
                            <div class='btn'>
                                <a href="javascript:"
                                   onclick="cargarmodal('{!! route('ingreso_insumo', [$insumo->id,$idHotel]) !!}')"
                                   class='btn btn-success btn-sm' title="Ingresar">
                                   <i class="fa fa-arrow-down"></i> / 
                                   <i class="fa fa-arrow-right"></i>
                                </a>
                                <a href="javascript:"
                                   onclick="cargarmodal('{!! route('salida_insumo', [$insumo->id,$idHotel]) !!}')"
                                   class='btn btn-info btn-sm' title="Sacar"><i
                                            class="fa fa-arrows"></i></a>
                                <a href="{!! route('movimientos_in', [$insumo->id,$idHotel]) !!}"
                                   class='btn btn-warning btn-sm' title="Movimientos"><i
                                            class="fa fa-navicon"></i></a>
                                &nbsp;&nbsp;
                                <a href="javascript:" onclick="cargarmodal('{!! route('insumo', [$insumo->id]) !!}')"
                                   class='btn btn-default btn-sm'><i
                                            class="glyphicon glyphicon-edit"></i></a>
                                &nbsp;&nbsp;
                                <a href="javascript:"
                                   onclick="if(confirm('Esta seguro de eliminar el usuario??')){window.location.href = '{!! route('eliminar_insumo', [$insumo->id]) !!}';}"
                                   class='btn btn-danger btn-sm'><i
                                            class="fa fa-remove"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scriptsextras')
@include('layouts.partials.jsdatatable')
@endpush
