@if(isset($insumo))
    {!! Form::model($insumo, ['route' => ['guarda_insumo',$insumo->id], 'method' => 'post']) !!}
@else
    {!! Form::open(['route' => ['guarda_insumo']]) !!}
@endif
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Formulario de Insumo</h4>
</div>

<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nombre del Insumo</label>
                    {!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Ej: Controles TV', 'required']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Categoria:</label>
                    @if (isset($insumo))
                        <input list="browsers" placeholder="Seleccione categoria" value="{{ $insumo->categoria }}" class="form-control" name="categoria" required>
                    @else
                        <input list="browsers" placeholder="Seleccione categoria" class="form-control" name="categoria" required>
                    @endif
                    <datalist id="browsers">
                        @foreach ($categorias as $key => $categoria)
                            <option value="{{ $categoria }}">
                        @endforeach
                    </datalist>
                </div>
            </div>
        </div>

        <div class="row">
            <!--<div class="col-md-6">
                <div class="form-group">
                    <label>Medida:</label>
                    {!! Form::select('medida', ['Unidades' => 'Unidades','Litros' => 'Litros','Kilos' => 'Kilos'],null, ['class' => 'form-control','placeholder' => 'Seleccione la medida','required']) !!}
                </div>
            </div>-->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Descripcion:</label>
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control','placeholder' => 'Ingrese la descripcion del insumo','rows' => 3]) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::close() !!}
