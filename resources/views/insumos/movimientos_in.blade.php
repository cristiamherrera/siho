@extends('layouts.app')

@section('content')

    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Movimientos de {!! $insumo->nombre !!}
                <?php
                $idHotel = null;
                if (isset($hotel->nombre)) {
                    echo ' - <b>' . $hotel->nombre . '</b>';
                    $idHotel = $hotel->id;
                }
                ?>
                <?php
                $total_g = $insumo->getTotal($idHotel);
                echo $total_g . ' (' . ($total_g / $insumo->cantidad) . ')';

                ?>
            </h3>
            <div class="box-tools pull-right no-imprimir">
                {{-- <button class="btn btn-box-tool btn-primary"
                        onclick="window.location.href = '{!! route('insumos') !!}';"
                        style="color: #ffffff;" title="CENTRAL">
                    <b>0</b>
                </button> --}}
                <?php
                    $mi_hotel = Auth::user()->hotel_id;
                    $mi_rol = Auth::user()->rol;
                ?>
                
                {{-- @foreach($hoteles as $numHot => $hotel)
                    <button class="btn btn-box-tool btn-primary"
                            onclick="window.location.href = '{!! route('insumos',[$hotel->id]) !!}';"
                            style="color: #ffffff;" title="{!! $hotel->nombre !!}">
                        <b>{!! $numHot+1 !!}</b>
                    </button>
                @endforeach --}}
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-responsive table-bordered" id="tabla">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Tipo</th>
                    <th>Hotel</th>
                    <th>Medida</th>
                    <th>Cantidad</th>
                    <th>Piso</th>
                    <th>Asignado</th>
                    <th>Fecha</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($movimientos as $movimiento)
                    <?php
                    $color_tr = "";
                    if (!empty($movimiento->deleted_at)) {
                        $color_tr = 'danger';
                    }
                    ?>

                    <tr class="{!! $color_tr !!}">
                        <td>{!! $movimiento->id !!}</td>
                        <td>{!! $movimiento->tipo !!}</td>
                        <td>
                            <?php
                            if (!empty($movimiento->hotel->nombre)) {
                                echo $movimiento->hotel->nombre;
                            } else {
                                echo "Central";
                            }
                            ?>
                        </td>
                        <td>{!! $movimiento->medida !!}</td>
                        <td>{!! $movimiento->cantidad !!}</td>
                        <td>
                            @if (!empty($movimiento->piso->nombre))
                                {!! $movimiento->piso->nombre !!}
                            @else
                                HOTEL
                            @endif
                        </td>
                        <td>
                            @if (!empty($movimiento->asignado->name))
                                {!! $movimiento->asignado->name !!}
                            @else
                                HOTEL
                            @endif
                        </td>
                        <td>{!! $movimiento->created_at !!}</td>
                        <td>
                            <div class='btn-group'>
                                @if (empty($movimiento->deleted_at))
                                    <a href="javascript:"
                                       onclick="if(confirm('Esta seguro de eliminar el usuario??')){window.location.href = '{!! route('eliminar_movimiento', [$movimiento->id]) !!}';}"
                                       class='btn btn-danger btn-xs'><i
                                                class="fa fa-remove"></i></a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('scriptsextras')
@include('layouts.partials.jsdatatable')
@endpush
