{!! Form::open(['route' => ['guarda_ingre_insumo']]) !!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <?php
        $total_g = $insumo->getTotal($idHotel);
    ?>
    <h4 class="modal-title">Ingreso de "{!! $insumo->nombre !!}" / <?php echo $total_g . ' (' . ($total_g / $insumo->cantidad) . ')'; ?></h4>
</div>
<div class="modal-body">
    <div class="form-group">
        @if(empty($idHotel))
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tipo de Ingreso:</label>
                        {!! Form::select('tipo', ['Ingreso' => 'Ingreso - Almacen Central', 'Ingreso a Hotel' => 'Ingreso a Hotel'],null, ['class' => 'form-control','id' => 'sel-medida']) !!}
                    </div>
                </div>
            </div>
            <div class="row" id="camp-hoteles" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Hotel:</label>
                        {!! Form::select('hotel_id', $hoteles,null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        @else
            {!! Form::hidden('tipo','Ingreso') !!}
            {!! Form::hidden('hotel_id',$idHotel) !!}
            {!! Form::hidden('aux_hotel_id',$idHotel) !!}
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Cantidad:</label>
                    {!! Form::number('cantidad', null, ['class' => 'form-control','placeholder' => 'Ej: 5','step' => 'any','required']) !!}
                </div>
            </div>

            {{-- <div class="col-md-4">
                <div class="form-group">
                    <label>Medida:</label>
                    {!! Form::select('medida', $medidas,null, ['class' => 'form-control','required']) !!}
                </div>
            </div> --}}

            {{-- <div class="col-md-8">
                <div class="form-group">
                    <label>Personal:</label>
                    {!! Form::select('asignado_id', $operarios, null, ['Seleccione uno', 'class' => 'form-control','required']) !!}
                </div>
            </div> --}}
        </div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
    {!! Form::submit('Guardar', ['class' => 'btn btn-outline pull-left']) !!}
</div>
{!! Form::hidden('insumo_id',$insumo->id) !!}
{{--{!! Form::hidden('medida',$insumo->medida) !!}--}}
{!! Form::close() !!}


<script>
    $('#sel-medida').change(function () {
        if ($(this).val() == 'Ingreso a Hotel') {
            $('#camp-hoteles').show(400);
        } else {
            $('#camp-hoteles').hide(400);
        }
    });
</script>
