@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('/plugins/fullcalendar/fullcalendar.min.css') }}">
<h1 class="pull-left">
    Calendario de Reservas
</h1>
<div class="clearfix">
</div>
<div class="clearfix">
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body no-padding">
                <div id="calendar">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scriptsextras')
<script src="{{ asset('/plugins/datepicker/moment.min.js') }}"></script>
<script src="{{ asset('/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

<script type="text/javascript">
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'hoy',
        month: 'mes',
        week : 'semana',
        day  : 'dia'
      },
      //Random default events
      events    : '{!! route("ajaxreservas") !!}',
      editable  : false,
      droppable : false, // this allows things to be dropped onto the calendar !!!
    })
</script>
@endpush