@extends('layouts.app')

@section('content')
<h1 class="pull-left">Tutoriales</h1>
<br><br><br><br>
<div class="box">
	<div class="box-body">
		<div class="clearfix">
			
			<div class="row">
				<div class="col-sm-6">
					<h4>Registro Cliente y Designar Habitacion</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/qgdDwRu_FWQ?ecver=1"  allowfullscreen></iframe>
				</div>

				<div class="col-sm-6">
					<h4>Registrar Salida de huesped</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/nF5Gz0vTqz0?ecver=1"  allowfullscreen></iframe>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h4>Transferir un cliente a otra Habitacion</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/YO37YW34jT8?ecver=1"  allowfullscreen></iframe>
				</div>
				<div class="col-sm-6">
					<h4>Realizar pagos extras</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/1EB2BO7UOnA?ecver=1"  allowfullscreen></iframe>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h4>Revisar el registro y caja chica</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/kMzLXqgz4Do?ecver=1"  allowfullscreen></iframe>
				</div>
				<div class="col-sm-6">
					<h4>Registro de Cliente</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/DBimHsZwias?ecver=1"  allowfullscreen></iframe>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h4>Generacion de pagos adelantados</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/6yFhuEixhbY?ecver=1"  allowfullscreen></iframe>
				</div>

				<div class="col-sm-6">
					<h4>Adicion de huesped en habitacion</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/fI4gB1ePfG0?ecver=1"  allowfullscreen></iframe>
				</div>
			</div>
			
		</div>
	</div>
</div>



@endsection