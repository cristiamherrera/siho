@extends('layouts.app')

@section('content')
<h1 class="pull-left">Tutoriales</h1>
<br><br><br><br>
<div class="box">
	<div class="box-body">
		<div class="clearfix">
			<div class="row">
				<div class="col-sm-6">
					<h4>1  Registrar Hotel, Habitaciones y categorias</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/NqpciUbQvFg?ecver=1"  allowfullscreen></iframe>
				</div>
				<div class="col-sm-6">
					<h4>2 Crear Precios por habitacion</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/M_kWP0WA5Rw?ecver=1"  allowfullscreen></iframe>
				</div>

			</div>
			<div class="row">
				<div class="col-sm-6">
					<h4>3 Caja Chica</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/9wDT9t99dB0?ecver=1"  allowfullscreen></iframe>
				</div>
				<div class="col-sm-6">
					<h4>4  Registrar Cliente</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/hc2pLG7-KJ0?ecver=1"  allowfullscreen></iframe>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h4>5  Crear Usuario</h4>
					<iframe width="100%" height="264" src="https://www.youtube.com/embed/hc2pLG7-KJ0?ecver=1"  allowfullscreen></iframe>
				</div>
				
			</div>


		</div>
	</div>
</div>



@endsection