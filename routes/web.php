<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*Route::get('/', function () {
return view('welcome');
});*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::get('/', 'UserController@direcciona');

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
	Route::group(['prefix' => 'v1'], function () {
		require config('infyom.laravel_generator.path.api_routes');
	});
});

Route::group(['middleware' => 'auth'], function () {

	Route::resource('hotels', 'HotelController');

	Route::resource('posts', 'PostController');

	Route::resource('habitaciones', 'HabitacionesController');

	Route::get('muestraHabitaciones/{idHotel}', 'HotelController@muestraHabitaciones');
//Route::get('muestraPisos/{idHotel}', 'PisosController@muestraPisos');
	Route::get('pisosHotel/{idHotel}', 'PisosController@pisosHotel')->name('pisosHotel');
	Route::get('piso/{idHotel}/{idPiso?}', 'PisosController@piso')->name('piso');
	Route::get('pisos/{idHotel}', 'PisosController@pisos')->name('pisos');
	Route::post('guarda_piso/{idPiso?}', 'PisosController@guarda_piso')->name('guarda_piso');
	Route::get('nuevaHabitacion/{idHotel}', 'HabitacionesController@nuevahabitacion');
	Route::get('ingresaPrecio/{idHabitacion}', 'PrecioshabitacionesController@ingresaPrecio');
	Route::get('asignahabitacion', 'ClientesController@asignahabitacion');
	Route::get('asignahabitacion2/{tipo}/{idCliente}/{num_reg?}', 'ClientesController@asignahabitacion2')->name('asignahabitacion2');
	/*Route::controller('datatables', 'PisosController', [
		        'anyData'  => 'datatables.data',
		        'muestraPisos' => 'datatables',
	*/

	Route::resource('clientes', 'ClientesController');

	Route::get('datatables/anyData', 'ClientesController@anyData')->name('datatables.data');
//    Route::get('datatables/getIndex', 'ClientesController@getIndex')->name('datatables');

	Route::resource('pisos', 'PisosController');

	Route::resource('precioshabitaciones', 'PrecioshabitacionesController');

	Route::resource('estudiantes', 'EstudiantesController');

//`Route::auth();

	Route::resource('cajas', 'CajaController');

	Route::resource('flujos', 'FlujoController');

	Route::get('caja/flujos/{idCaja}', 'CajaController@flujos')->name('flujos');

//Route::get('cajas2/{idHotel?}', 'CajaController@index')->name('cajas.index');

	Route::get('caja/ingreso/{idCaja}', 'CajaController@ingreso');
	Route::post('caja/guarda_ingreso', 'CajaController@guarda_ingreso');
	Route::post('retirarpagos', 'CajaController@retirarpagos')->name('retirarpagos');
	Route::post('retirarpagosdia', 'CajaController@retirarpagosdia')->name('retirarpagosdia');

	Route::get('caja/egreso/{idCaja}', 'CajaController@egreso');

	Route::get('soloingresos/{idCaja}', 'CajaController@soloingresos')->name('soloingresos');
	Route::get('soloingresosdia/{idCaja}', 'CajaController@soloingresosdia')->name('soloingresosdia');
	Route::get('ultimosretiros/{idCaja}', 'CajaController@ultimosretiros')->name('ultimosretiros');

	Route::post('caja/guarda_egreso', 'CajaController@guarda_egreso');
	Route::post('caja/eliminar_flujo/{idFlujo}', 'CajaController@eliminar_flujo');

	Route::get('caja/eliminaflujo/{idFlujo}', 'CajaController@eliminaflujo')->name('eliminaflujo');

	Route::get('detalleflujo/{idFlujo}', 'FlujoController@detalleflujo')->name('detalleflujo');

	Route::resource('registros', 'RegistroController');
	Route::get('registros', 'RegistroController@index')->name('registros');
	Route::get('registros/nuevo/{tipo}/{idCliGru}/{idHabitacion}/{idRegistro?}', 'RegistroController@nuevo')->name('nuevoregistro');
	Route::get('registros_calendario', 'RegistroController@calendario')->name('calendario');
	Route::post('registros/guarda_registro/{idRegistro?}', 'RegistroController@guarda_registro')->name('guarda_registro');
//Route::post('registros/nuevos/{idCliente}/{num_reg?}', 'RegistroController@nuevos')->name('nuevos');
	Route::post('registros/guarda_registros/{num_reg?}', 'RegistroController@guarda_registros')->name('guarda_registros');
	Route::get('registros_cliente/{idCliente}', 'RegistroController@registros_cliente')->name('registros_cliente');
	Route::post('registrar_pago', 'RegistroController@registrar_pago')->name('registrar_pago');

	Route::match(['get', 'post'], 'registros/nuevos/{idCliente}/{num_reg?}', 'RegistroController@nuevos')->name('nuevos');

	Route::get('get_num_reg', 'RegistroController@get_num_reg');

	Route::get('direcciona', 'UserController@direcciona')->name('direcciona');

	Route::get('usuarios/', 'UserController@index')->name('usuarios');
	Route::get('usuarios/usuario/{idUsuario?}', 'UserController@usuario')->name('usuario');
	Route::post('usuarios/guarda_usuario/{idUsuario?}', 'UserController@guarda_usuario')->name('guarda_usuario');
	Route::get('usuarios/eliminar/{idUsuario}', 'UserController@eliminar')->name('eliminar');

	Route::get('cambiarhotel/{idHotel}', 'UserController@cambiarhotel')->name('cambiarhotel');

	Route::get('vhabitaciones', 'HabitacionesController@vhabitaciones')->name('vhabitaciones');
	Route::get('informacion_habitacion/{idHabitacion}', 'HabitacionesController@informacion_habitacion')->name('informacion_habitacion');

	Route::resource('categorias', 'CategoriaController');

	Route::resource('facturas', 'FacturaController');
	Route::get('tutoriales', 'TutorialesController@index')->name('tutoriales');
	Route::get('tutoriales/operariotutoriales', 'TutorialesController@operariotutoriales')->name('opetutoriales');

	Route::get('facturar/{idFlujo}', 'FacturaController@facturar')->name('facturar');
	Route::post('generar_factura', 'FacturaController@generar_factura')->name('generar_factura');
	Route::get('factura/{idFactura}', 'FacturaController@factura')->name('factura');

//Route::get('reporte_pagos', 'ReporteController@reporte_pagos')->name('reporte_pagos');

	Route::match(['get', 'post'], 'reporte_pagos', 'ReporteController@reporte_pagos')->name('reporte_pagos');
	Route::match(['get', 'post'], 'repo_pago_regis', 'ReporteController@repo_pago_regis')->name('repo_pago_regis');
	Route::match(['get', 'post'], 'repo_pago_retirados', 'ReporteController@repo_pago_retirados')->name('repo_pago_retirados');

	Route::get('cliente/{idCliente?}', 'ClientesController@cliente')->name('cliente');
	Route::get('adjuntos/{idCliente}', 'ClientesController@adjuntos')->name('adjuntos');

	Route::post('guarda_cliente/{idCliente?}', 'ClientesController@guarda_cliente')->name('guarda_cliente');

	Route::post('guarda_s_adjunto/{idCliente}', 'ClientesController@guarda_s_adjunto')->name('guarda_s_adjunto');

	Route::get('elimina_adjunto/{idAdjunto}', 'ClientesController@elimina_adjunto')->name('elimina_adjunto');

	Route::get('quitarhuesped/{idHospedante}', 'RegistroController@quitarhuesped')->name('quitarhuesped');

	Route::get('msalidahuesped/{idHospedante}', 'RegistroController@msalidahuesped')->name('msalidahuesped');

	Route::get('grupos', 'GrupoController@index')->name('grupos');
	Route::get('registrosgrupos/{idGrupo}', 'GrupoController@registrosgrupos')->name('registrosgrupos');
	Route::get('grupo/{idGrupo?}', 'GrupoController@grupo')->name('grupo');
	Route::get('eliminargrupo/{idGrupo}', 'GrupoController@eliminargrupo')->name('eliminargrupo');
	Route::post('registrapagosg', 'GrupoController@registrapagosg')->name('registrapagosg');

	Route::get('informe_pago_grupo/{idGrupo}', 'GrupoController@informe_pago_grupo')->name('informe_pago_grupo');

	Route::get('marcasalida/{idRegistro}', 'GrupoController@marcasalida')->name('marcasalida');
	Route::get('cancelaregistro/{idRegistro}', 'GrupoController@cancelaregistro')->name('cancelaregistro');
	Route::get('generadeudasgrupos/{idGrupo}', 'GrupoController@generadeudasgrupos')->name('generadeudasgrupos');
	Route::get('generaadelantosgrupos/{idGrupo}', 'GrupoController@generaadelantosgrupos')->name('generaadelantosgrupos');

	Route::get('grupo/{idGrupo}', 'GrupoController@grupo')->name('grupo');
	Route::post('guarda_grupo/{idGrupo}', 'GrupoController@guarda_grupo')->name('guarda_grupo');
	Route::get('addpagoextra/{idRegistro}', 'GrupoController@addpagoextra')->name('addpagoextra');
	Route::post('guarda_pagoextra', 'GrupoController@guarda_pagoextra')->name('guarda_pagoextra');

	Route::match(['get', 'post'], 'pasajeros_reporte', 'ReporteController@pasajeros_reporte')->name('pasajeros_reporte');
	Route::match(['get', 'post'], 'reporte_registros', 'ReporteController@reporte_registros')->name('reporte_registros');

	Route::resource('actividads', 'ActividadController');
	Route::get('actividad/{idCliente}/{idActividad?}', 'ActividadController@actividad')->name('actividad');
	Route::post('guarda_actividad/{idActividad?}', 'ActividadController@guarda_actividad')->name('guarda_actividad');
	Route::get('grupos/eliminapago/{idPago}', 'GrupoController@eliminapago')->name('eliminapago');
	Route::post('grupos/eliminar_pago/{idPago}', 'GrupoController@eliminar_pago');
	Route::get('crono_genera_pagos', 'GrupoController@crono_genera_pagos');

	Route::post('opcioneshab', 'PisosController@opcioneshab')->name('opcioneshab');
	Route::post('guarda_precio_h', 'PisosController@guarda_precio_h')->name('guarda_precio_h');
	Route::post('elimina_precio_h', 'PisosController@elimina_precio_h')->name('elimina_precio_h');

	Route::get('imp_registro/{idRegistro}', 'RegistroController@imp_registro')->name('imp_registro');

	Route::match(['get', 'post'], 'terminos_condiciones', 'HotelController@terminos_condiciones')->name('terminos_condiciones');

	Route::match(['get', 'post'], 'tiempo_cancelar_reg', 'HotelController@tiempo_cancelar_reg')->name('tiempo_cancelar_reg');

	Route::match(['get', 'post'], 'config_logo', 'HotelController@config_logo')->name('config_logo');
	Route::match(['get', 'post'], 'config_hora_lim', 'HotelController@config_hora_lim')->name('config_hora_lim');
	Route::match(['get', 'post'], 'config_hora_camb_fecha', 'HotelController@config_hora_camb_fecha')->name('config_hora_camb_fecha');

	Route::get('transferencia/{idRegistro}', 'GrupoController@transferencia')->name('transferencia');
	Route::post('guarda_transferencia', 'RegistroController@guarda_transferencia')->name('guarda_transferencia');

	Route::get('ajaxpreciosr/{idHabitacion}', 'GrupoController@ajaxpreciosr')->name('ajaxpreciosr');

	Route::get('recibo_retiro/{idFlujo}', 'CajaController@recibo_retiro')->name('recibo_retiro');
	Route::get('recibo_retiroxdia/{idFlujo}', 'CajaController@recibo_retiroxdia')->name('recibo_retiroxdia');

	Route::get('get_cliente_fac/{nit}', 'FacturaController@get_cliente_fac')->name('get_cliente_fac');

	Route::get('registra_responable/{idGrupo}/{idCliente}', 'GrupoController@registra_responable')->name('registra_responable');

	Route::get('insumos/{idHotel?}', 'InsumoController@index')->name('insumos');
	Route::get('insumo/{idInsumo?}', 'InsumoController@insumo')->name('insumo');
	Route::post('guarda_insumo/{idInsumo?}', 'InsumoController@guarda_insumo')->name('guarda_insumo');

	Route::get('eliminar_insumo/{idInsumo}', 'InsumoController@eliminar_insumo')->name('eliminar_insumo');

	Route::get('paquetes', 'PaqueteController@index')->name('paquetes');
	Route::get('paquete/{idPaquete?}', 'PaqueteController@paquete')->name('paquete');
	Route::post('guarda_paquete/{idPaquete?}', 'PaqueteController@guarda_paquete')->name('guarda_paquete');
	Route::get('eliminar_paquete/{idPaquete}', 'PaqueteController@eliminar_paquete')->name('eliminar_paquete');
	Route::get('paquetes', 'PaqueteController@index')->name('paquetes');

	Route::get('ingrediente/{idPaquete}/{idIngrediente?}', 'PaqueteController@ingrediente')->name('ingrediente');
	Route::post('guarda_ingrediente/{idIngrediente?}', 'PaqueteController@guarda_ingrediente')->name('guarda_ingrediente');
	Route::get('eliminar_ingrediente/{idIngrediente}', 'PaqueteController@eliminar_ingrediente')->name('eliminar_ingrediente');

	Route::get('ingreso_insumo/{idInsumo}/{idHotel?}', 'InsumoController@ingreso_insumo')->name('ingreso_insumo');
	Route::post('guarda_ingre_insumo', 'InsumoController@guarda_ingre_insumo')->name('guarda_ingre_insumo');

	Route::get('movimientos_in/{idInsumo}/{idHotel?}', 'InsumoController@movimientos_in')->name('movimientos_in');
	Route::get('eliminar_movimiento/{idMovimiento}', 'InsumoController@eliminar_movimiento')->name('eliminar_movimiento');

	Route::get('salida_insumo/{idInsumo}/{idHotel?}', 'InsumoController@salida_insumo')->name('salida_insumo');
	Route::post('guarda_salida_insumo', 'InsumoController@guarda_salida_insumo')->name('guarda_salida_insumo');

	Route::get('entregas_insumos/{fecha?}', 'PaqueteController@entregas_insumos')->name('entregas_insumos');
	Route::get('eliminar_entrega_in/{idEntrega}', 'PaqueteController@eliminar_entrega_in')->name('eliminar_entrega_in');
	Route::post('registra_entregas_pa', 'PaqueteController@registra_entregas_pa')->name('registra_entregas_pa');
	Route::post('guarda_entrega_insumo', 'PaqueteController@guarda_entrega_insumo')->name('guarda_entrega_insumo');
	Route::get('user/ayuda', 'UserController@ayuda')->name('ayuda');

	Route::get('datatables/registros', 'RegistroController@datatableregistros')->name('datatables.registros');
	Route::get('datatables/grupos', 'GrupoController@datatablegrupos')->name('datatables.grupos');
	Route::get('datatables/flujos/{idCaja}', 'CajaController@datatableflujos')->name('datatables.flujos');

	Route::match(['get', 'post'], 'panelcontrol', 'UserController@panelcontrol')->name('panelcontrol');

	Route::post('guarda_habitacion_es/{idPiso?}', 'HabitacionesController@guarda_habitacion_es')->name('guarda_habitacion_es');

	Route::get('parametros/', 'ParametroController@index')->name('parametros');
	Route::get('parametros/parametro/{idParametro?}', 'ParametroController@parametro')->name('parametro');
	Route::post('parametros/guarda_parametro/{idParametro?}', 'ParametroController@guarda_parametro')->name('guarda_parametro');
	Route::get('parametros/eliminarparametro/{idParametro}', 'ParametroController@eliminarparametro')->name('eliminarparametro');

	Route::get('getestadoshabit', function () {
		$idHotel = Auth::user()->hotel_id;
		$c_ocupados = DB::table('registros')
			->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
			->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
			->where('pisos.hotel_id', $idHotel)
			->where('registros.estado', 'Ocupando')
			->whereNull('registros.deleted_at')
			->count();

		$c_reservas = DB::table('registros')
//            ->select(DB::raw('COUNT(*) as n_ocupados'))
			->join('habitaciones', 'habitaciones.id', '=', 'registros.habitacione_id')
			->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
			->where('pisos.hotel_id', $idHotel)
			->where('registros.estado', 'Reservado')
			->whereNull('registros.deleted_at')
//            ->groupBy('habitaciones.id')
			->count();

		$c_limpieza = DB::table('habitaciones')
			->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
			->where('pisos.hotel_id', $idHotel)
			->where('habitaciones.estado', 'Limpieza')
			->count();

		$c_habitotal = DB::table('habitaciones')
			->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
			->where('pisos.hotel_id', $idHotel)
			->where('habitaciones.estado', 'Habilitado')
			->whereNull(DB::raw("(SELECT id FROM registros WHERE habitacione_id = habitaciones.id AND ISNULL(registros.deleted_at) AND registros.estado='Ocupando'  LIMIT 1)"))
			->count();

		$c_deshabil = DB::table('habitaciones')
			->join('pisos', 'pisos.id', '=', 'habitaciones.piso_id')
			->where('pisos.hotel_id', $idHotel)
			->where('habitaciones.estado', 'Deshabilitado')
			->count();

		return response()->json([
			'ocupados' => $c_ocupados,
			'limpieza' => $c_limpieza,
			'reservas' => $c_reservas,
			'libres' => $c_habitotal,
			'deshabilitados' => $c_deshabil,

		]);
	})->name('getestadoshabit');

	Route::get('reportedia', 'ReporteController@reportedia')->name('reportedia');
	Route::get('reservas', 'ReporteController@reservas')->name('reservas');
	Route::get('ajaxreservas', 'ReporteController@ajaxreservas')->name('ajaxreservas');
});
